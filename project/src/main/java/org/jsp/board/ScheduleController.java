package org.jsp.board;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.jsp.board.service.CheckBoxDAO;
import org.jsp.board.service.MemberDAO;
import org.jsp.board.service.PjmemlistDAO;
import org.jsp.board.service.PlanDAO;
import org.jsp.board.service.ProjectDAO;
import org.jsp.board.vo.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Handles requests for the application home page.
 */
@Controller
public class ScheduleController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Inject
	PlanDAO planDAO;
	
	@Inject
	ProjectDAO pjDAO;
	
	@Inject
	PjmemlistDAO pjmDAO;
	
	@Inject
	MemberDAO mDAO;
	
	@Inject
	CheckBoxDAO checkboxDAO;


	//Controller 정렬 : 제일 첫번째 조회(입장 페이지). 그리고 그 이하 CRUD 순서대로 정렬했습니다.
	//C : create. insert 기능
	//R : read. select 기능
	//U : update. update 기능
	//D : delete. delete 기능

	//CRUD의 C.
	@ResponseBody
	@RequestMapping(value = "/insertJ", method = RequestMethod.GET)
	public List<Plan> reloadJ(Locale locale, Model model, String jsonString) {
		/*
		 * JSON으로 Parsing해야 하고.
		 * Parsing 받은 것 중 id 부분은 ArrayList기 때문에
		 * 이를 저장하기 위해 BLOB? 의 긴 String을 저장하는 방법을 알아야 한다.
		 * 
		 */
		System.out.println("======================");
		System.out.println(jsonString);
		System.out.println("======================");
		//JSONlistQ가 스트링으로 받는게 맞는 형식인가?
		//JSONlistQ 이거를 어떻게 변환해야 안에 있는 Value들을 받을 수 있는지
		//Value 가지고 
		//new Plan(pNO, planName, pStartDate, pEndDate, pJNo, id, priority, progress, color)
		//이거 만들어서
		//DB에 저장
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse( jsonString );
		JsonObject jsonObj = (JsonObject) obj;

		Gson gson = new Gson();
		
		String id = gson.toJson(jsonObj.get("id"));
		
		/*ArrayList<String> idList = new ArrayList<>();
		
		for (int i = 0; i < jsonObj.get("id").getAsJsonArray().size(); i++) {
			idList.add(i, String.valueOf(jsonObj.get("id").getAsJsonArray().get(i)));
		}*/ //이거 나중에 Select할 때 써먹을 코드임.
		
		int pNO = Integer.parseInt(String.valueOf(jsonObj.get("PNO")));
		String planName = String.valueOf(jsonObj.get("planName"));
		String pStartDate = String.valueOf(jsonObj.get("pStartDate"));
		String pEndDate = String.valueOf(jsonObj.get("pEndDate"));

		System.out.println(pStartDate);
		System.out.println(pEndDate);
		
	        SimpleDateFormat format = new SimpleDateFormat("\"yyyy-MM-dd-HH-mm-ss\"");
	        SimpleDateFormat sqlformat = new SimpleDateFormat("yyyy/MM/dd, HH:mm");
	        Date parsed = null;
	        String theDateString = null;
			try {
				parsed = format.parse(pStartDate);
				theDateString = sqlformat.format(parsed);
				pStartDate = theDateString;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
	        System.out.println("sql에 맞춰 변환 된 값 : "+pStartDate);
	        
	        try {
	        	parsed = format.parse(pEndDate);
				theDateString = sqlformat.format(parsed);
				pEndDate = theDateString;
	        } catch (ParseException e) {
	        	e.printStackTrace();
	        }
	        System.out.println("sql에 맞춰 변환 된 값 : "+pEndDate);
	        
		int pJNo = Integer.parseInt(String.valueOf(jsonObj.get("PJNo")));
		int priority = Integer.parseInt(String.valueOf(jsonObj.get("Priority")).replaceAll("\"", ""));
		double progress = Double.parseDouble(String.valueOf(jsonObj.get("Progress")));
		String color = String.valueOf(jsonObj.get("color"));
				
		Plan newPlanIns = new Plan(pNO, planName, pStartDate, pEndDate, 
				pJNo, id, priority, progress, color);
		
		System.out.println(planDAO.insertPlan(newPlanIns));
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		
		int SelectProject = pJNo;
		
		planList = planDAO.selectPlanList(SelectProject);

		System.out.println(planList);

		model.addAttribute("planList",planList);
		
		return planList;
	}
	
	//현재 schedule_seunghee의 코드를 schedule_jooyoung의 코드에 차츰 얹고 있습니다.
	@RequestMapping(value = "/goplan", method = RequestMethod.GET)
	public String goJ(Locale locale, Model model, HttpSession session, int pjno) {

		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		ArrayList<Pjmemlist> pjmList = new ArrayList<>();
		ArrayList<Member> memberList = new ArrayList<>();
		
		//pno에 해당하는 plan정보를 디비에서 가져온다
		//pno에 해당하는 checkbox정보를 디비에서 가져온다 
		pjmList = pjmDAO.pjmemlist(pjno);
		
		for (Pjmemlist pjmember : pjmList) {
			Member memberIns = mDAO.selectMember(pjmember.getId());
			memberList.add(memberIns);
		}
		
		//int SelectProject = pjno;
		
		planList = planDAO.selectPlanList(pjno);
		
		System.out.println(planList);
		
		model.addAttribute("pjno", pjno);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		model.addAttribute("planList",planList);
		model.addAttribute("memberList",memberList);

		return "schedule";
	}

	//Calendar형
	@RequestMapping(value = "/goplanCalendar", method = RequestMethod.GET)
	public String goplanCalendar(Locale locale, Model model, int pjno) {
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		
		int SelectProject = pjno;
		
		planList = planDAO.selectPlanList(SelectProject);
		
		System.out.println(planList);
		
		model.addAttribute("pjNO", SelectProject);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_calendar";
	}
	

	//Gant형 날짜순 기본형
	@RequestMapping(value = "/goplanGant", method = RequestMethod.GET)
	public String goplanGant(Locale locale, Model model, int pjno) {
	
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		ArrayList<Pjmemlist> pjmList = new ArrayList<>();
		ArrayList<Member> memberList = new ArrayList<>();
		
		//pno에 해당하는 plan정보를 디비에서 가져온다
		//pno에 해당하는 checkbox정보를 디비에서 가져온다 
		pjmList = pjmDAO.pjmemlist(pjno);
		
		for (Pjmemlist pjmember : pjmList) {
			Member memberIns = mDAO.selectMember(pjmember.getId());
			memberList.add(memberIns);
		}
		
		int SelectProject = pjno;
		
		planList = planDAO.selectPlanList(SelectProject);
		
		System.out.println(planList);
		
		model.addAttribute("pjNO", SelectProject);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_gant";
	}
	
	//Gant형 내 일정
	@RequestMapping(value = "/goplanGantById", method = RequestMethod.GET)
	public String goplanGantById(Locale locale, Model model, int pjno, String loginname) {
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		ArrayList<Pjmemlist> pjmList = new ArrayList<>();
		ArrayList<Member> memberList = new ArrayList<>();
		
		//pno에 해당하는 plan정보를 디비에서 가져온다
		//pno에 해당하는 checkbox정보를 디비에서 가져온다 
		pjmList = pjmDAO.pjmemlist(pjno);
		
		for (Pjmemlist pjmember : pjmList) {
			Member memberIns = mDAO.selectMember(pjmember.getId());
			memberList.add(memberIns);
		}
		
		int SelectProject = pjno;
		
		Plan planIns = new Plan(0, null, null, null, pjno, loginname, 0, 0, null);
		
		planList = planDAO.selectPlanListById(planIns);
		
		System.out.println(planList);
		
		model.addAttribute("pjNO", SelectProject);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_gant";
	}
	
	//Gant형 중요도순
	@RequestMapping(value = "/goplanGantByPriority", method = RequestMethod.GET)
	public String goplanGantByPriority(Locale locale, Model model, int pjno) {
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		ArrayList<Pjmemlist> pjmList = new ArrayList<>();
		ArrayList<Member> memberList = new ArrayList<>();
		
		//pno에 해당하는 plan정보를 디비에서 가져온다
		//pno에 해당하는 checkbox정보를 디비에서 가져온다 
		pjmList = pjmDAO.pjmemlist(pjno);
		
		for (Pjmemlist pjmember : pjmList) {
			Member memberIns = mDAO.selectMember(pjmember.getId());
			memberList.add(memberIns);
		}
		
		int SelectProject = pjno;
		
		planList = planDAO.selectPlanListByPriority(SelectProject);
		
		System.out.println(planList);
		
		model.addAttribute("pjNO", SelectProject);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_gant";
	}
	
	//개인형
	@RequestMapping(value = "/myScheduleToday", method = RequestMethod.GET)
	public String schedule_myScheduleToday(Locale locale, Model model,int pjno) {
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		
		int SelectProject = pjno;
		
		planList = planDAO.selectPlanList(SelectProject);
		
		System.out.println(planList);
		
		model.addAttribute("pjNO", SelectProject);
		model.addAttribute("pj", pjDAO.selectAProject(pjno));
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_myScheduleToday";
	}

	@RequestMapping(value = "/goplanS", method = RequestMethod.GET)
	public String goS(Locale locale, Model model) {

		//여기서부터
		String idListString = null;
		
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		
		int SelectProject = 21;
		
		planList = planDAO.selectPlanList(SelectProject);

		System.out.println(planList);

		/*Gson gson = new Gson();
		
		List<Object> planListGson = new ArrayList<Object>();
		
		for (Plan plan : planList) {
			Object gsonIns = gson.toJson(plan);
			planListGson.add(gsonIns);
		}*/
		
		
		//model.addAttribute("planList",planListGson);
		model.addAttribute("planList",planList);
		
		return "schedule_seunghee";
	}	

	
	@RequestMapping(value = "/reloadS", method = RequestMethod.GET)
	public String reloadS(Locale locale, Model model, String jsonString) {
		System.out.println("======================");
		System.out.println(jsonString);
		System.out.println("======================");
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse( jsonString );
		JsonObject jsonObj = (JsonObject) obj;

		Gson gson = new Gson();
		
		String id = gson.toJson(jsonObj.get("id"));
		
		int pNO = Integer.parseInt(String.valueOf(jsonObj.get("PNO")));
		String planName = String.valueOf(jsonObj.get("planName"));
		String pStartDate = String.valueOf(jsonObj.get("pStartDate"));
		String pEndDate = String.valueOf(jsonObj.get("pEndDate"));

		System.out.println(pStartDate);
		System.out.println(pEndDate);
		
	        SimpleDateFormat format = new SimpleDateFormat("\"yyyy-MM-dd-HH-mm-ss\"");
	        Date parsed = null;
			try {
				parsed = format.parse(pStartDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
	        java.sql.Date sql = new java.sql.Date(parsed.getTime());
	        pStartDate = sql.toString();
	        
	        try {
	        	parsed = format.parse(pEndDate);
	        } catch (ParseException e) {
	        	e.printStackTrace();
	        }
	        sql = new java.sql.Date(parsed.getTime());
	        pEndDate = sql.toString();
	        
		int pJNo = Integer.parseInt(String.valueOf(jsonObj.get("PJNo")).replaceAll("\"", ""));
		int priority = Integer.parseInt(String.valueOf(jsonObj.get("priority")).replace("\"", ""));
		double progress = Double.parseDouble(String.valueOf(jsonObj.get("Progress")).replace("\"", ""));

		String color = String.valueOf(jsonObj.get("color"));
				
		Plan newPlanIns = new Plan(pNO, planName, pStartDate, pEndDate, 
				pJNo, id, priority, progress, color);
		
		planDAO.insertPlan(newPlanIns);
		//System.out.println(planDAO.insertPlan(newPlanIns));
		
		return "schedule_seunghee";
	}

	@SuppressWarnings("deprecation")
	@ResponseBody
	@RequestMapping(value = "/updateJ", method = RequestMethod.GET)
	public List<Plan> updateJ(Locale locale, Model model, String jsonString) {
		/*
		var planInsJSON = { "PNO":3, "planName":planName,
				"pStartDate":pStartDate, "pEndDate":pEndDate,
				"PJNo":0, "id":id, "Priority":5, "Progress":0.4, "color":color };
		//위와 같은 형식으로 보낸 것을 String으로 받기 때문에,
		 * JSON으로 Parsing해야 하고.
		 * Parsing 받은 것 중 id 부분은 ArrayList기 때문에
		 * 이를 저장하기 위해 BLOB? 의 긴 String을 저장하는 방법을 알아야 한다.
		 * 
		 */
		System.out.println("======================");
		System.out.println(jsonString);
		System.out.println("======================");
		//JSONlistQ가 스트링으로 받는게 맞는 형식인가?
		//JSONlistQ 이거를 어떻게 변환해야 안에 있는 Value들을 받을 수 있는지
		//Value 가지고 
		//new Plan(pNO, planName, pStartDate, pEndDate, pJNo, id, priority, progress, color)
		//이거 만들어서
		//DB에 저장
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse( jsonString );
		JsonObject jsonObj = (JsonObject) obj;

		Gson gson = new Gson();
		
		System.out.println("jsonObj.get(id):"+jsonObj.get("id"));
		System.out.println("jsonObj.get(id).toString():"+jsonObj.get("id").toString());
		System.out.println("gson.toJson(jsonObj.get(id)):"+gson.toJson(jsonObj.get("id")));
		System.out.println("jsonObj.get(planName):"+jsonObj.get("planName"));
		System.out.println("jsonObj.get(planName).toString():"+jsonObj.get("planName").toString());
		
		String id = gson.toJson(jsonObj.get("id")).toString();
		String planName = jsonObj.get("planName").toString();
		//스트링 타입인 JSON은 replace 등으로 건드리지 않는다.
		
		/*ArrayList<String> idList = new ArrayList<>();
		
		for (int i = 0; i < jsonObj.get("id").getAsJsonArray().size(); i++) {
			idList.add(i, String.valueOf(jsonObj.get("id").getAsJsonArray().get(i)));
		}*/ //이거 나중에 Select할 때 써먹을 코드임.
		
		int pNO = Integer.parseInt(String.valueOf(jsonObj.get("PNO")).replaceAll("\"",""));
		int pJNo = Integer.parseInt(String.valueOf(jsonObj.get("PJNo")).replaceAll("\"",""));
		//int 값이므로 JSON에서 보낼 때 양 옆에 큰 따옴표 \" 추가 되는 것 제거
		
		String pStartDate = String.valueOf(jsonObj.get("pStartDate")).replaceAll("\"","");
		String pEndDate = String.valueOf(jsonObj.get("pEndDate")).replaceAll("\"","");

		System.out.println(pStartDate);
		System.out.println(pEndDate);
		
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        SimpleDateFormat sqlformat = new SimpleDateFormat("yyyy/MM/dd, HH:mm");
        Date parsed = null;
        //java.sql.Date parsed = null;
        String theDateString = null;
		try {
			parsed = format.parse(pStartDate);
			System.out.println("::::::::"+parsed);
			theDateString = sqlformat.format(parsed);
			pStartDate = theDateString;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
        System.out.println("sql에 맞춰 변환 된 값 : "+pStartDate);
        
        try {
        	parsed = format.parse(pEndDate);
			theDateString = sqlformat.format(parsed);
			pEndDate = theDateString;
        } catch (ParseException e) {
        	e.printStackTrace();
        }
        System.out.println("sql에 맞춰 변환 된 값 : "+pEndDate);
	        
		int priority = Integer.parseInt(String.valueOf(jsonObj.get("Priority")).replaceAll("\"",""));
		double progress = Double.parseDouble(String.valueOf(jsonObj.get("Progress")).replaceAll("\"",""));
		String color = String.valueOf(jsonObj.get("color"));
				
		Plan newPlanIns = new Plan(pNO, planName, pStartDate, pEndDate, 
				pJNo, id, priority, progress, color);
		
		System.out.println(planDAO.updatePlan(newPlanIns));

		//이하 새로 업데이트 된 리스트 받아오기
		ArrayList<Plan> planList = new ArrayList<Plan>();
		//Plan으로 구성된 ArrayList를 상정했을 때.
		//여기까지
		
		int SelectProject = pJNo;
		
		planList = planDAO.selectPlanList(SelectProject);
		model.addAttribute("planList",planList);
		
		return planList;
	}
	
	
	//일정막대그래프를 클릭하면 그 일정에 해당하는 내용을 디비에서 가져와서 창을 띄워준다
	@RequestMapping(value = "/getThePlan", method = RequestMethod.GET)
	public String getThePlan(Model model, String pno) {
		
		Plan plan = new Plan();
		ArrayList<CheckBox> checkboxList = new ArrayList<>();
		//디비에서 checkbox 텍스트 불러와서 "" 이거 제거해주고 null값을 " "이렇게 
		//jsp단에서 뿌려주고 싶어서 만들었습니다
		ArrayList<String> checkboxListText = new ArrayList<>(); 
		ArrayList<Pjmemlist> pjmList = new ArrayList<>();
		ArrayList<Member> memberList = new ArrayList<>();
		
		plan = planDAO.selectPlan(Integer.parseInt(pno)); //디비에서 일정 받아옴

		int pjNO = plan.getPjno();
		
		//pno에 해당하는 plan정보를 디비에서 가져온다
		//pno에 해당하는 checkbox정보를 디비에서 가져온다 
		checkboxList = checkboxDAO.selectCheckBoxList(Integer.parseInt(pno));  
		pjmList = pjmDAO.pjmemlist(pjNO);
		
		String text="";

		for(int i=0; i<checkboxList.size(); i++){

			if(checkboxList.get(i).getText().equals("null")){

				text=" ";
				checkboxListText.add(text); // null이면 ""로 설정

			}else{

				text=checkboxList.get(i).getText().replaceAll("\"","");
				checkboxListText.add(text);
			}

		}
		
		for (Pjmemlist pjmember : pjmList) {
			Member memberIns = mDAO.selectMember(pjmember.getId());
			memberList.add(memberIns);
		}
		
System.out.println("=====================================");
System.out.println("===================="+memberList);
System.out.println("=====================================");
		
		//디비에서 ""큰따옴표 들어간 데이터가 jsp에 안뿌려져서 replace로 빼야될거 같다
		String planname = plan.getPlanname().replace("\"", "");
		String color = plan.getColor().replace("\"", "");
		
		//jsp 화면에서 데이터를 뿌려주기위해 수정한 데이터 포함해서 다시 넣어줌
		plan = new Plan(plan.getPno(),planname,plan.getPstartdate(),plan.getPenddate(),
				plan.getPjno(),plan.getId(),plan.getPriority(),plan.getProgress(),color);
		//jsp 화면에서 checkbox를 뿌려주기위한
	
		model.addAttribute("checkboxListText",checkboxListText);
		model.addAttribute("checkboxList",checkboxList);
		model.addAttribute("memberList",memberList);
		model.addAttribute("plan",plan);
		
		System.out.println("plan : " + plan);
		System.out.println("checkboxList : " + checkboxList);
		System.out.println("checkboxListText: " + checkboxListText);
		
		return "getThePlan";
		
	}
	
	//plan 업데이트
	@ResponseBody
	@RequestMapping(value = "/updatePlan", method = RequestMethod.GET)
	public String updatePlan(Locale locale, Model model, String jsonString) {
		
		System.out.println("======업데이트테스트======");
		System.out.println(jsonString);
		System.out.println("=======================");
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse( jsonString );
		JsonObject jsonObj = (JsonObject) obj;

		Gson gson = new Gson();
		
		String id = gson.toJson(jsonObj.get("id")).replaceAll("\"", "");
		int pno = Integer.parseInt(String.valueOf(jsonObj.get("pno")).replace("\"", ""));
		String planname = String.valueOf(jsonObj.get("planname"));
		String pStartDate = String.valueOf(jsonObj.get("pstartdate"));
		String pEndDate = String.valueOf(jsonObj.get("penddate"));

		System.out.println(pStartDate);
		System.out.println(pEndDate);
		
	        SimpleDateFormat format = new SimpleDateFormat("\"yyyy-MM-dd-HH-mm-ss\"");
	        SimpleDateFormat sqlformat = new SimpleDateFormat("yyyy/MM/dd, HH:mm");
	        Date parsed = null;
	        String theDateString = null;
			try {
				parsed = format.parse(pStartDate);
				theDateString = sqlformat.format(parsed);
				pStartDate = theDateString;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
	        System.out.println("sql에 맞춰 변환 된 값 : "+pStartDate);
	        
	        try {
	        	parsed = format.parse(pEndDate);
				theDateString = sqlformat.format(parsed);
				pEndDate = theDateString;
	        } catch (ParseException e) {
	        	e.printStackTrace();
	        }
	        System.out.println("sql에 맞춰 변환 된 값 : "+pEndDate);
	            
		int pjno = Integer.parseInt(String.valueOf(jsonObj.get("pjno")).replaceAll("\"", ""));
		int priority = Integer.parseInt(String.valueOf(jsonObj.get("priority")).replace("\"", ""));
		double progress = Double.parseDouble(String.valueOf(jsonObj.get("progress")).replaceAll("\"", ""));
		String color = String.valueOf(jsonObj.get("color"));
				
		Plan newPlanIns = new Plan(pno, planname, pStartDate, pEndDate, pjno, id, priority, progress, color);
		
		System.out.println(newPlanIns);
		System.out.println("=============chlwndud 테스트=======");
		System.out.println(planDAO.updatePlan(newPlanIns));
		
		return "goplan";
	}

	//plan 삭제
	@ResponseBody
	@RequestMapping(value = "/deletePlan", method = RequestMethod.GET)
	public String deletePlan(Locale locale, Model model, String pno) {
		
		System.out.println("pno : " + pno);
		
		planDAO.deletePlan(Integer.parseInt(String.valueOf(pno).replace("\"", "")));
		
		return "goplan";
	}	
	
	//체크박스 디비에 insert
	@ResponseBody
	@RequestMapping(value = "/insertCheckBox", method = RequestMethod.GET)
	public String insertCheckBox(Locale locale, Model model, String jsonString) {
		
		/*
		var planInsJSON = { "PNO":3, "planName":planName,
				"pStartDate":pStartDate, "pEndDate":pEndDate,
				"PJNo":0, "id":id, "Priority":5, "Progress":0.4, "color":color };
		//위와 같은 형식으로 보낸 것을 String으로 받기 때문에,
		 * JSON으로 Parsing해야 하고.
		 * Parsing 받은 것 중 id 부분은 ArrayList기 때문에
		 * 이를 저장하기 위해 BLOB? 의 긴 String을 저장하는 방법을 알아야 한다.
		 * 
		 */
		
		System.out.println("=insertCheckBox=====================");
		System.out.println(jsonString);
		System.out.println("============insertCheckBox==========");  
		//JSONlistQ가 스트링으로 받는게 맞는 형식인가?
		//JSONlistQ 이거를 어떻게 변환해야 안에 있는 Value들을 받을 수 있는지
		//Value 가지고 
		//new Plan(pNO, planName, pStartDate, pEndDate, pJNo, id, priority, progress, color)
		//이거 만들어서
		//DB에 저장
		
		JsonParser parser = new JsonParser();
		Object obj = parser.parse(jsonString);
		JsonArray jsonArr = (JsonArray) obj;
		
		//Gson gson = new Gson();
		
		int cno=7777; int pno=0; int pjno=0; String text=""; int checked=0;
		
		CheckBox cb = null;
		
		//cno, pno, pjno, text, checked
		for(int i=0; i<jsonArr.size(); i++) {
			
			try {
				
				JsonObject jsonObject = (JsonObject)jsonArr.get(i);
				
				cno = Integer.parseInt(String.valueOf(jsonObject.get("cno")).replaceAll("\"", ""));
				pno = Integer.parseInt(String.valueOf(jsonObject.get("pno")).replaceAll("\"", ""));
				pjno = Integer.parseInt(String.valueOf(jsonObject.get("pjno")).replaceAll("\"", ""));
				text= String.valueOf(jsonObject.get("text")).replaceAll("\"", "");
				checked = Integer.parseInt(String.valueOf(jsonObject.get("checked")).replaceAll("\"", ""));
				
				System.out.println("cno : " + cno);
				
			} catch (Exception e) {
				
				System.out.println(e+"안되는데?ㅠㅠㅠㅠㅠ");
			}
				
			cb = new CheckBox(cno,pno,pjno,text,checked);
			
			if (checkboxDAO.selectCheckBox(cno) == null) {
				if (cb.getCno() == 0) {
					System.out.println("checkbox to insert : " + cb);
					checkboxDAO.insertCheckBox(cb);
				}
			} else if (cb.getText().equals("") || cb.getText() == null) {
				System.out.println("checkbox to delete : " + cb);
				checkboxDAO.deleteCheckBox(cno);
			} else {
				System.out.println("checkbox to update : " + cb);
				checkboxDAO.updateCheckBox(cb);
			}
			
			if (i == jsonArr.size()-1) {
				return null;				
			}
		}
		
		//return "goplan?pjno="+pjno;
		return null;
	}

}
