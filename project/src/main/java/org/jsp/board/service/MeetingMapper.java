package org.jsp.board.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsp.board.vo.Meeting;
import org.jsp.board.vo.Member;

public interface MeetingMapper {
	
	public int insertMeeting(Meeting mt);
	
	public int updateMeetingtext(Meeting mt);
	
	public Meeting selectOneMeeting(Meeting mt);
	
	public Meeting selectMeetingTitle(Meeting mt);
	
	public ArrayList<Meeting> selectPjMeetings(int pjno);
}