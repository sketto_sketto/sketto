package org.jsp.board.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsp.board.vo.Chatlibrary;
import org.jsp.board.vo.Meeting;
import org.jsp.board.vo.Member;

public interface ChatlibraryMapper {
	
	//파일 등록
	public int insertFile (Chatlibrary ch);
	
	//한 파일 만 가져옴
	public Chatlibrary selectOneFile(Chatlibrary ch);
	
	//한 회의에 속하는 파일을 모두 가져옴
	public ArrayList<Chatlibrary> selectAllFiles(Chatlibrary ch);
}