package org.jsp.board.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.jsp.board.vo.Orderproduct;
import org.jsp.board.vo.Pjmemlist;
import org.jsp.board.vo.Project;
import org.jsp.board.vo.Shoppinglist;
import org.springframework.stereotype.Repository;

@Repository
public class ProjectDAO implements ProjectMapper {

	@Inject
	SqlSession session;
	
	@Override
	public int insertProject(Project pj) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).insertProject(pj);
	}

	@Override
	public Project selectAProject(int pjno) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).selectAProject(pjno);
	}

	@Override
	public ArrayList<Project> selectPjlistLeader(String id) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).selectPjlistLeader(id);
	}

	@Override
	public ArrayList<Project> selectPjlistJoined(String id) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).selectPjlistJoined(id);
	}

	@Override
	public ArrayList<Project> selectPjlist(String id) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).selectPjlist(id);
	}
	
	@Override
	public int updateProject(Project pj) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).updateProject(pj);
	}

	@Override
	public int deleteProject(Project pj) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).deleteProject(pj);
	}
	
	
	
	
	
	
/*	
	@Override
	public ArrayList<Project> readlist() {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).readlist();
	}
*/	
	/*
	@Override
	public List<Project> getProjectStockInfo(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).getProjectStockInfo(map);
	}
	
	@Override
	public List<Project> getSCDInfo() {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).getSCDInfo();
	}
	
	@Override
	public List<Project> getBInfo() {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).getBInfo();
	}
	
	@Override
	public List<Project> getLInfo() {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).getLInfo();
	}
	
	@Override
	public List<Shoppinglist> getShoppingList(String custid) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).getShoppingList(custid);
	}
	
	@Override
	public int insertOrderProject(Orderproduct op) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).insertOrderProject(op);
	}
	
	//String productserialnumber, int amount
	@Override
	public int updateStock(HashMap<String, String> map) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).updateStock(map);
	}
	
	@Override
	public int insertShoppinglist(Shoppinglist sl) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).insertShoppinglist(sl);
	}
	
	@Override
	public int deleteShoppinglist(String productserialnumber) {
		// TODO Auto-generated method stub
		return session.getMapper(ProjectMapper.class).deleteShoppinglist(productserialnumber);
	}*/
}
