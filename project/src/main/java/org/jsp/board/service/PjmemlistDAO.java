package org.jsp.board.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.jsp.board.vo.Pjmemlist;
import org.springframework.stereotype.Repository;

@Repository
public class PjmemlistDAO implements PjmemlistMapper {

	@Inject
	SqlSession session;
	
	@Override
	public int insertPjmem(Pjmemlist pjmem) {
		// TODO Auto-generated method stub
		return session.getMapper(PjmemlistMapper.class).insertPjmem(pjmem);
	}

	@Override
	public ArrayList<Pjmemlist> pjmemlist(int pjno) {
		// TODO Auto-generated method stub
		return session.getMapper(PjmemlistMapper.class).pjmemlist(pjno);
	}

	@Override
	public int deletePjmem(Pjmemlist pjmem) {
		// TODO Auto-generated method stub
		return session.getMapper(PjmemlistMapper.class).deletePjmem(pjmem);
	}

	
	

}
