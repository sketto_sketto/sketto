package org.jsp.board.service;

import java.util.ArrayList;

import org.jsp.board.vo.Pjinvitedlist;

public interface PjinvitedlistMapper {

	public int inviteToPj(Pjinvitedlist ivtlist);
	
	public ArrayList<Pjinvitedlist> invitedList(int pjno);
	
	//초대 리스트와 Sketto멤버 리스트의 교집합(조인)
	public ArrayList<Pjinvitedlist> checkIfJoined(int pjno);
	
	public int deleteFromInvited(Pjinvitedlist ivt);
	
	public ArrayList<Pjinvitedlist> pjnolistById(String id);


}
