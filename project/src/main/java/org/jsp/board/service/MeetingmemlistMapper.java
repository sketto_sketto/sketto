package org.jsp.board.service;

import java.util.ArrayList;

import org.jsp.board.vo.Meetingmemlist;

public interface MeetingmemlistMapper {
	//회의에 참여시킴
	public int insertMml(Meetingmemlist mml);
	
	//한 개 회의에 참여하는 한 사용자를 찾는 메소드
	public Meetingmemlist selectOneMml(Meetingmemlist mml);
	
	//참여자 목록에 이름들을 올려주기 위한 메소드
	public ArrayList<String> selectNames(int meetingno);
	
	//회의에서 나가면 해당 사용자를 회의 참여자 목록에서 없애는 메소드
	public int deleteMml(Meetingmemlist mml);
}
