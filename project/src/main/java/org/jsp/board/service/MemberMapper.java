package org.jsp.board.service;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsp.board.vo.Member;

public interface MemberMapper {
	//1.회원가입
	public int joinMember(Member sc);
	//2.회원정보수정
	public int updateMember(Member sc);
	//3.회원탈퇴
	public int deleteMember(HashMap<String, String> map);
	//4.로그인
	public Member login(Member sc);
	//5.아이디 중복확인
	public String checkId(String id);
	//6. 멤버 한 명 검색
	public Member selectMember(String id);
	//7.project 생성시 invite.jsp에서 검색하는 리스트
	public ArrayList<Member> MemberList(String searchText);
	//8. invitedList랑 비교할 리스트
	public ArrayList<Member> AllMember();
}