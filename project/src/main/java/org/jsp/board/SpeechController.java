package org.jsp.board;

import java.awt.List;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.jsp.board.service.ChatlibraryDAO;
import org.jsp.board.service.MeetingDAO;
import org.jsp.board.service.MeetingmemlistDAO;
import org.jsp.board.service.MemberDAO;
import org.jsp.board.service.PjmemlistDAO;
import org.jsp.board.service.ProjectDAO;
import org.jsp.board.util.FileService;
import org.jsp.board.vo.Chatlibrary;
import org.jsp.board.vo.Meeting;
import org.jsp.board.vo.Meetingmemlist;
import org.jsp.board.vo.Member;
import org.jsp.board.vo.Pjmemlist;
import org.jsp.board.vo.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

@Controller
public class SpeechController {
	
	@Autowired
	private JavaMailSenderImpl javaMailSenderImpl;
	
	@Autowired
	ProjectDAO pjdao;
	
	@Autowired
	MeetingmemlistDAO mmldao;
	
	@Autowired
	PjmemlistDAO memlistdao;
	
	@Autowired
	MeetingDAO mtdao;
	
	@Autowired
	ChatlibraryDAO chdao;

	@RequestMapping(value = "speechDemo", method = RequestMethod.GET)
	public String speech() {
		return "/speech";
	}
	//회의 개설 및 선택 폼으로 이동 - 리더 한정
	@RequestMapping(value = "meetingForm", method = RequestMethod.GET)
	public String meetingForm(Model model, HttpSession session) {
		session.removeAttribute("plist");
		session.removeAttribute("mt");
		ArrayList<Meeting> pjMeetingList = new ArrayList<>();
		//session.setAttribute("loginid", "TESTID");	//회원 관리 기능 완성 되면 수정하거나 삭제해야 함  
		int pjno = (int) session.getAttribute("pjno");
		Project pj = pjdao.selectAProject(pjno);
		session.setAttribute("pj", pj);	//pjno는 어느 순간부터 세션에 고정해서 들어갈지를 정해야 함.
		pjMeetingList = mtdao.selectPjMeetings(pjno);
		model.addAttribute("pjmlist", pjMeetingList);
		
		return "/meetingForm";	//회의 생성은 ajax로 바꿀 것.	//?pjno="+pjno; 나중에 추가하기
	}
	
	//회의 개설 기능에 관한 메소드 - 리더 전용
	//@ResponseBody
	@RequestMapping(value = "makemeeting", method = RequestMethod.POST)
	public String makeMeeting(Meeting mt, HttpSession session, Model model){
		File originalFile, savedFile, folder;
		FileOutputStream fos = null;
		session.removeAttribute("errormsg");
		if(mtdao.selectMeetingTitle(mt) == null){
			mtdao.insertMeeting(mt);	//<selectKey> 태그를 사용하여 meetingno를 자동으로 가져올 수 있게 됨!
			//System.out.println("처음엔 meetingno와 meetingdate가 안 보일 것이다: "+mt);
			mt = mtdao.selectOneMeeting(mt);	//meetingno와 meetingdate가 없는 상태라 의미가 없음
			System.out.println("회의 생성: "+mt);	
			
			//회의록 파일 생성 및 DB에 회의록 파일 이름(경로 포함) 저장
			try {
				folder = new File("C:\\SkettoMeeting");
				if (!folder.exists()) {
					folder.mkdirs();
					System.out.println("폴더 생성");
				}
				originalFile  = new File("C:/SkettoMeeting/SkettoMeetingText"+mt.getPjno()+"_"+mt.getMeetingno()+"_"+mt.getMeetingdate()+".txt");
				fos = new FileOutputStream(originalFile);
				
				if (!originalFile.exists()) {
					originalFile.createNewFile();
					System.out.println("파일 생성");
				}
				
				if (mt.getMeetingtext() == null) {
					mt.setMeetingtext(originalFile.toString());
					mtdao.updateMeetingtext(mt);
				}
				
//				//byte[] meetingTextInBytes = meetingText.toString().getBytes("UTF-8");
//				
//				fos.write(meetingTextInBytes);
//				fos.flush();
//				fos.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}else {
			String errormsg = "중복되지 않는 새로운 회의명을 입력해 주세요.";
			session.setAttribute("errormsg", errormsg);
		}
		
		return "redirect:meetingForm";	//?pjno="+pjno; 나중에 추가하기, 위의 메소드로 가는 게 아니라면 어떡할까...
	}
	
	@ResponseBody
	@RequestMapping(value="alarm30101", method=RequestMethod.GET, produces = "application/text; charset=utf8")
	public String alarm30(HttpSession session){
		String alarmMsg = null;
		
		Calendar calCurrent = Calendar.getInstance();
        java.util.Date dateCurrent = calCurrent.getTime();
        String today = (new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss").format(dateCurrent));
        System.out.println(today);
        int pjno = (int) session.getAttribute("pjno");
        ArrayList<Meeting> pjmtlist = mtdao.selectPjMeetings(pjno);
        ArrayList<Pjmemlist> pjmemlist = memlistdao.pjmemlist(pjno);
        
        for (Meeting m : pjmtlist) {
        	SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        	Date toDate = new Date();
			try {
				toDate = transFormat.parse(m.getMeetingdate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
        	
//        	Calendar calMeetingdate = Calendar.getInstance();
//        	
//        	calCurrent.setTime(dateCurrent);
//        	calMeetingdate.setTime(toDate);
//        	calMeetingdate.add(Calendar.MINUTE, -30);
        	System.out.println(dateCurrent+"  "+toDate);
        	long diff = toDate.getTime() - dateCurrent.getTime();
        	long diffToMinute = diff / 60000;
        	if (diffToMinute == 30) {
				alarmMsg = m.getMeetingtitle()+" 회의가 약 30분 남았습니다.";
				System.out.println(alarmMsg);
				
				for (Pjmemlist pjmem : pjmemlist) {
					SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
					simpleMailMessage.setFrom("skettomanager@gmail.com");
					simpleMailMessage.setTo(pjmem.getId());
					simpleMailMessage.setSubject("Sketto 회의 참여 알람: "+m.getMeetingtitle());
					simpleMailMessage.setText("Sketto에서 회의 참여 알람이 도착했습니다. 로그인하여 회의에 참여해 주세요. \n http://10.10.8.186:8888/board/");
					javaMailSenderImpl.send(simpleMailMessage);
				}
				
			}
        	
        	if (diffToMinute == 10) {
				alarmMsg = m.getMeetingtitle()+" 회의가 약 10분 남았습니다. 준비해주세요.";
				System.out.println(alarmMsg);
				
				for (Pjmemlist pjmem : pjmemlist) {
					SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
					simpleMailMessage.setFrom("skettomanager@gmail.com");
					simpleMailMessage.setTo(pjmem.getId());
					simpleMailMessage.setSubject("Sketto 회의 참여 알람: "+m.getMeetingtitle());
					simpleMailMessage.setText("Sketto에서 회의 참여 알람이 도착했습니다. 로그인하여 회의에 참여해 주세요. \n http://10.10.8.186:8888/board/");
					javaMailSenderImpl.send(simpleMailMessage);
				}
			}
        	
        	if (diffToMinute == 1) {
				alarmMsg = m.getMeetingtitle()+" 회의가 약 1분 남았습니다. 서둘러서 접속해주세요.";
				System.out.println(alarmMsg);
				
				for (Pjmemlist pjmem : pjmemlist) {
					SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
					simpleMailMessage.setFrom("skettomanager@gmail.com");
					simpleMailMessage.setTo(pjmem.getId());
					simpleMailMessage.setSubject("Sketto 회의 참여 알람: "+m.getMeetingtitle());
					simpleMailMessage.setText("Sketto에서 회의 참여 알람이 도착했습니다. 로그인하여 회의에 참여해 주세요. \n http://10.10.8.186:8888/board/");
					javaMailSenderImpl.send(simpleMailMessage);
				}
			}
		}
		
		return alarmMsg;
	}
	
	
	
	
	//선택한 회의 폼으로 이동 - 리더 한정
	@RequestMapping(value = "speech", method = RequestMethod.GET)
	public String startSpeech(Meeting mt, Model model, HttpSession session) {
		mt = mtdao.selectOneMeeting(mt);
		System.out.println("회의 입장: "+mt);
		session.setAttribute("mt", mt);
		
		Meetingmemlist mml = new Meetingmemlist(mt.getMeetingno(), 
												(String)session.getAttribute("loginid"), 
												(String)session.getAttribute("loginname"));
		mmldao.deleteMml(mml);
		mmldao.insertMml(mml);
		ArrayList<String> plistFormal = mmldao.selectNames(mt.getMeetingno());
		
		ArrayList<String> plist = new ArrayList<>();
		for (int i = 0; i < plistFormal.size(); i++) {
			if (!plist.contains(plistFormal.get(i))) {
				plist.add(plistFormal.get(i));
			}
		}
		System.out.println(plist);
		session.setAttribute("plist", plist);
	
		return "speech";	//?meetingno="+mt.getMeetingno();	//가 가능하도록 해야 함
	}

	@RequestMapping(value = "broadcast", method = RequestMethod.GET)
	public String broadcast() {
		return "/broadcast";
	}
	
	//회의록 파일 작성
	//StringBuffer meetingText = new StringBuffer();
	
	@ResponseBody
	@RequestMapping(value = "voice", method = RequestMethod.GET)
	public String voice(Meeting mt, String voice, Model model, HttpSession session) {
		File originalFile, savedFile, folder;
		
		FileOutputStream fos = null;
		//FileInputStream fis = null;
		
		if (voice.isEmpty()) {
			voice += ">>텍스트 재현 불안";
		}
		
		System.out.println(voice);
//		if (!meetingText.toString().contains(voice)) {
//			meetingText.append(voice);
//		}else{
//			return null;
//		}
		
		mt = (Meeting) session.getAttribute("mt");
		//System.out.println(mt);
		
		try {
			folder = new File("C:\\SkettoMeeting");
			if (!folder.exists()) {
				folder.mkdirs();
				System.out.println("폴더 생성");
			}
			originalFile  = new File("C:/SkettoMeeting/SkettoMeetingText"+mt.getPjno()+"_"+mt.getMeetingno()+"_"+mt.getMeetingdate()+".txt");
			fos = new FileOutputStream(originalFile, true);
			
			if (!originalFile.exists()) {
				originalFile.createNewFile();
				System.out.println("파일 생성");
			}
			
			if (mt.getMeetingtext() == null) {
				mt.setMeetingtext(originalFile.toString());
				mtdao.updateMeetingtext(mt);
			}
			
//			byte[] meetingTextInBytes = meetingText.toString().getBytes("UTF-8");
			byte[] meetingTextInBytes = voice.getBytes("UTF-8");
			
			fos.write(meetingTextInBytes);
			fos.flush();
			fos.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("voice텍스트 입력 후: "+meetingText.toString());
		return voice;
	}
	
	/*남은 작업들 리스트*/
	//DB와 연동
	//파일 올리기(FileService) & 받기
	//회의 종료-정리 작업
	//파일 리스트-계정 화면에서 회의 히스토리 참조할 때
	final String uploadPath = "C:\\SkettoMeeting\\filesUploaded";
	ArrayList<Chatlibrary> downloadsList = new ArrayList<>();
	@ResponseBody
	@RequestMapping(value = "/uploadFile")
	public Chatlibrary uploadFile(Chatlibrary ch, MultipartFile upload, HttpSession session, Model model){
		//세션에서 로그인한 사용자의 아이디를 읽어서 Chatlibrary 클래스의 객체  ch의 작성자 정보에 세팅
	    //String id = (String) session.getAttribute("loginid");
	    //ch.setId(id);	
		//System.out.println("파일 넣기 전: "+ch);	//id, meetingno자동으로 붙는 것 확인
	    //첨부파일이 있는 경우 지정된 경로에 저장하고, 원본 파일명과 저장된 파일명을 ch객체에 세팅
		if (!upload.isEmpty()) {
			String savedfile = FileService.saveFile(upload, uploadPath);
			ch.setOriginalfile(upload.getOriginalFilename());
			ch.setSavedfile(savedfile);
		}else{
			System.out.println("파일 전송 실패");
		}
		chdao.insertFile(ch);
		ch = chdao.selectOneFile(ch);
		System.out.println("파일 업로드: "+ch);
		
		return ch;
	}
	
	
	//다운로드 버튼 클릭시 새창 열기
	@RequestMapping(value = "download", method = RequestMethod.GET)
	public String download(Model model, HttpSession session) {
		Meeting mt = (Meeting) session.getAttribute("mt");
		Chatlibrary ch = new Chatlibrary();
		
		ch.setMeetingno(mt.getMeetingno());
		downloadsList = chdao.selectAllFiles(ch);
		
		model.addAttribute("dlist", downloadsList);
		return "/download";
	}
	
	//@ResponseBody
	//@Param filenum
	@RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
	public Chatlibrary downloadFile(Chatlibrary filenumOnly, int filenum, Model model, HttpServletResponse response){
		filenumOnly.setFilenum(filenum);
		Chatlibrary ch = chdao.selectOneFile(filenumOnly);
		
		//원래 파일명 불러오기
		String originalfile = new String(ch.getOriginalfile());
		try {
			response.setHeader("Content-Disposition", " attachment;filename="+ URLEncoder.encode(originalfile, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		//저장된 파일 경로
		String fullpath = uploadPath +"\\"+ ch.getSavedfile();
		
		//서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
	    FileInputStream filein = null;
	    ServletOutputStream fileout = null;

	    try {
	        filein = new FileInputStream(fullpath);
	        fileout = response.getOutputStream();

	        //Spring의 파일 관련 유틸
	        FileCopyUtils.copy(filein, fileout);

	        filein.close();
	        fileout.close();
	        
	        System.out.println("파일 다운로드: "+ch);
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
		return null;
	}
	
	@RequestMapping(value = "/hitherto", method = RequestMethod.GET)
	public Meeting downloadHitherto(Meeting mt, Model model, HttpSession session, HttpServletResponse response){
		mt = (Meeting) session.getAttribute("mt");
		
		//원래 파일명 불러오기
		//String originalfile = new String(mt.getMeetingtext().substring(17, mt.getMeetingtext().length()));	
		String originalfile = new String(mt.getMeetingtext().replaceAll("\\\\", "//").split("//")[2]);
		System.out.println("기존 회의록 다운로드: "+originalfile);
		try {
			response.setHeader("Content-Disposition", " attachment;filename="+ URLEncoder.encode(originalfile, "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		//저장된 파일 경로
		String fullpath = mt.getMeetingtext();
		
		//서버의 파일을 읽을 입력 스트림과 클라이언트에게 전달할 출력스트림
	    FileInputStream filein = null;
	    ServletOutputStream fileout = null;

	    try {
	        filein = new FileInputStream(fullpath);
	        fileout = response.getOutputStream();

	        //Spring의 파일 관련 유틸
	        FileCopyUtils.copy(filein, fileout);

	        filein.close();
	        fileout.close();
	        
	        //System.out.println("파일 다운로드: "+ mt.getMeetingtext());
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
		return null;
	}
	
	@RequestMapping(value="saveMeetingText", method = RequestMethod.GET)
	public String saveMeetingText(Meeting mt, HttpSession session){
		Meeting mtt = (Meeting)session.getAttribute("mt");
		
		Meetingmemlist mml = new Meetingmemlist(mtt.getMeetingno(), (String)session.getAttribute("loginid"), null);
		
		mmldao.deleteMml(mml);
		
		session.removeAttribute("plist");
		session.removeAttribute("mt");
		
		return "redirect:/meetingForm";
	}
	
}
