--------------------------------------------------------
--  파일이 생성됨 - 월요일-4월-30-2018   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table CHATLIBRARY
--------------------------------------------------------

  CREATE TABLE "SKETTO"."CHATLIBRARY" 
   (	"ID" VARCHAR2(50 BYTE), 
	"MEETINGNO" NUMBER, 
	"INPUTDATE" DATE DEFAULT SYSDATE, 
	"ORIGINALFILE" VARCHAR2(100 BYTE), 
	"SAVEDFILE" VARCHAR2(100 BYTE), 
	"FILENUM" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table CHECKBOX
--------------------------------------------------------

  CREATE TABLE "SKETTO"."CHECKBOX" 
   (	"CNO" NUMBER, 
	"PJNO" NUMBER, 
	"PNO" NUMBER, 
	"TEXT" VARCHAR2(1000 BYTE), 
	"CHECKED" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MEETING
--------------------------------------------------------

  CREATE TABLE "SKETTO"."MEETING" 
   (	"MEETINGNO" NUMBER, 
	"ID" VARCHAR2(50 BYTE), 
	"MEETINGDATE" DATE DEFAULT SYSDATE, 
	"PJNO" NUMBER, 
	"MEETINGTEXT" VARCHAR2(500 BYTE), 
	"MEETINGTITLE" VARCHAR2(500 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MEETINGMEMLIST
--------------------------------------------------------

  CREATE TABLE "SKETTO"."MEETINGMEMLIST" 
   (	"MEETINGNO" NUMBER, 
	"ID" VARCHAR2(50 BYTE), 
	"NAME" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table MEMBER
--------------------------------------------------------

  CREATE TABLE "SKETTO"."MEMBER" 
   (	"ID" VARCHAR2(50 BYTE), 
	"PW" VARCHAR2(50 BYTE), 
	"NAME" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PJINVITEDLIST
--------------------------------------------------------

  CREATE TABLE "SKETTO"."PJINVITEDLIST" 
   (	"PJNO" NUMBER, 
	"ID" VARCHAR2(50 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PJMEMLIST
--------------------------------------------------------

  CREATE TABLE "SKETTO"."PJMEMLIST" 
   (	"ID" VARCHAR2(50 BYTE), 
	"PJNO" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PLAN
--------------------------------------------------------

  CREATE TABLE "SKETTO"."PLAN" 
   (	"PNO" NUMBER, 
	"PLANNAME" VARCHAR2(100 BYTE), 
	"PSTARTDATE" DATE DEFAULT SYSDATE, 
	"PENDDATE" DATE DEFAULT SYSDATE+7, 
	"PJNO" NUMBER, 
	"ID" VARCHAR2(1000 BYTE), 
	"PRIORITY" NUMBER DEFAULT 9, 
	"PROGRESS" NUMBER DEFAULT 0, 
	"COLOR" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Table PROJECT
--------------------------------------------------------

  CREATE TABLE "SKETTO"."PROJECT" 
   (	"PJNO" NUMBER, 
	"PJTITLE" VARCHAR2(100 BYTE) DEFAULT '새 프로젝트 이름', 
	"ID" VARCHAR2(50 BYTE), 
	"STARTDATE" DATE DEFAULT SYSDATE, 
	"ENDDATE" DATE DEFAULT SYSDATE+30
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  DDL for Sequence BOARD3_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."BOARD3_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CHATROOMLIST_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."CHATROOMLIST_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence CNO_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."CNO_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 121 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence FILENUM_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."FILENUM_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 521 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MEETINGNO_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."MEETINGNO_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 901 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MEMBER_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."MEMBER_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PJNO_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."PJNO_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 361 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PNO_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."PNO_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 281 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REPLY3_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "SKETTO"."REPLY3_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 21 CACHE 20 NOORDER  NOCYCLE ;
REM INSERTING into SKETTO.CHATLIBRARY
SET DEFINE OFF;
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',878,to_date('18/04/29','RR/MM/DD'),'07_Layout_3step.html','20180429.html',481);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',876,to_date('18/04/29','RR/MM/DD'),'007.png','20180429.png',482);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'08_게시판 글목록.PNG','201804291524992149545.PNG',483);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'08_게시판 글목록.PNG','201804291524992149545.PNG',484);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'11_게시판 글수정.PNG','201804291524992207692.PNG',485);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'11_게시판 글수정.PNG','201804291524992207686.PNG',486);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'08_게시판 글목록.PNG','201804291524992261041.PNG',487);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'01_메인화면 (로그인 하지 않은 경우).PNG','201804291524992415369.PNG',488);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',877,to_date('18/04/29','RR/MM/DD'),'01_메인화면 (로그인 하지 않은 경우).PNG','201804291524992415372.PNG',489);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',875,to_date('18/04/29','RR/MM/DD'),'005.png','20180429.png',490);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('thggbtmb@gmail.com',884,to_date('18/04/29','RR/MM/DD'),'001.png','201804291525003015300.png',491);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('thggbtmb@gmail.com',884,to_date('18/04/29','RR/MM/DD'),'009.png','201804291525003076541.png',492);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('thggbtmb@gmail.com',884,to_date('18/04/29','RR/MM/DD'),'009.png','201804291525003091022.png',493);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@gmail.com',883,to_date('18/04/29','RR/MM/DD'),'008.png','201804291525004919971.png',494);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',886,to_date('18/04/30','RR/MM/DD'),'08_게시판 글목록.PNG','20180430.PNG',495);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',886,to_date('18/04/30','RR/MM/DD'),'01_메인화면 (로그인 하지 않은 경우).PNG','201804301525063837320.PNG',496);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',886,to_date('18/04/30','RR/MM/DD'),'04_아이디 중복확인 (사용중인 아이디).PNG','201804301525065777453.PNG',497);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('thggbtmb@gmail.com',886,to_date('18/04/30','RR/MM/DD'),'015.png','201804301525065878585.png',498);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('qtw0626@gmail.com',888,to_date('18/04/30','RR/MM/DD'),'giphy.gif','20180430.gif',499);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('1113@1113.com',889,to_date('18/04/30','RR/MM/DD'),'005.png','20180430.png',500);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',890,to_date('18/04/30','RR/MM/DD'),'mic.gif','20180430.gif',501);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('kimys1986@naver.com',872,to_date('18/04/28','RR/MM/DD'),'SkettoMeetingText231_871_2018-04-28-13-37-00 (6).txt','201804281524890859130.txt',465);
Insert into SKETTO.CHATLIBRARY (ID,MEETINGNO,INPUTDATE,ORIGINALFILE,SAVEDFILE,FILENUM) values ('hello',871,to_date('18/04/28','RR/MM/DD'),'007.png','201804281524890914389.png',466);
REM INSERTING into SKETTO.CHECKBOX
SET DEFINE OFF;
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (57,44,172,'"가"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (58,44,172,'"나"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (59,44,172,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (60,44,171,'"ㄱ"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (61,44,171,'"ㄴ"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (62,44,171,'"ㄷ"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (63,44,178,'"1"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (64,44,178,'"2"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (65,44,178,'"3"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (66,44,178,'"4"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (67,44,167,'"하"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (68,44,167,'"이"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (55,44,172,'"들"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (56,44,172,'"어"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (69,44,167,'"루"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (70,44,167,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (71,44,170,'"나"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (72,44,170,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (73,44,170,'"와"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (74,44,170,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (75,44,170,'"라"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (76,44,170,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (77,44,222,'"집에 가기"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (78,44,222,'"어벤져스 복습하기"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (81,299,245,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (82,299,245,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (83,299,245,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (115,231,229,'"나나"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (116,231,229,'null',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (96,242,238,'"ㅁㄴㅇㅁㄴ"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (113,231,229,'"가가"',0);
Insert into SKETTO.CHECKBOX (CNO,PJNO,PNO,TEXT,CHECKED) values (114,231,229,'null',0);
REM INSERTING into SKETTO.MEETING
SET DEFINE OFF;
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (872,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_872_2018-04-28-13-37-58.txt','테스트02');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (873,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_873_2018-04-28-13-38-08.txt','테스트03');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (874,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_874_2018-04-28-14-43-54.txt','테스트04');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (875,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_875_2018-04-28-14-46-43.txt','테스트05');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (876,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_876_2018-04-28-14-48-02.txt','테스트06');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (877,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_877_2018-04-28-14-49-08.txt','테스트07');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (878,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_878_2018-04-28-15-05-56.txt','테스트08');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (879,'TESTID',to_date('18/04/28','RR/MM/DD'),228,'C:\SkettoMeeting\SkettoMeetingText228_879_2018-04-28-16-21-00.txt','TESTID''s meeting');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (880,'TESTID',to_date('18/04/28','RR/MM/DD'),228,'C:\SkettoMeeting\SkettoMeetingText228_880_2018-04-28-16-23-00.txt','TESTID''s meeting2');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (881,'hello',to_date('18/05/01','RR/MM/DD'),232,'C:\SkettoMeeting\SkettoMeetingText232_881_2018-05-01-11-00-00.txt','hellopj1stMeeting');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (882,'kimys1986@naver.com',to_date('18/05/04','RR/MM/DD'),229,'C:\SkettoMeeting\SkettoMeetingText229_882_2018-05-04-15-03-00.txt','TESTs meeting');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (883,'kimys1986@naver.com',to_date('18/04/29','RR/MM/DD'),229,'C:\SkettoMeeting\SkettoMeetingText229_883_2018-04-29-15-05-23.txt','Test02');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (884,'thggbtmb@gmail.com',to_date('18/05/09','RR/MM/DD'),241,'C:\SkettoMeeting\SkettoMeetingText241_884_2018-05-09-10-56-00.txt','okokdd');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (885,'jyncho@naver.com',to_date('18/05/04','RR/MM/DD'),242,'C:\SkettoMeeting\SkettoMeetingText242_885_2018-05-04-21-01-00.txt','Why is it has to be over 5 letters');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (886,'kimys1986@naver.com',to_date('18/04/29','RR/MM/DD'),299,'C:\SkettoMeeting\SkettoMeetingText299_886_2018-04-29-22-05-26.txt','인터뷰 준비');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (887,'kimys1986@naver.com',to_date('18/04/30','RR/MM/DD'),305,'C:\SkettoMeeting\SkettoMeetingText305_887_2018-04-30-00-41-49.txt','테스트00');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (888,'qtw0626@gmail.com',to_date('18/04/30','RR/MM/DD'),226,'C:\SkettoMeeting\SkettoMeetingText226_888_2018-04-30-15-03-00.txt','ㅎㅏ이5555555');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (889,'1113@1113.com',to_date('18/04/30','RR/MM/DD'),243,'C:\SkettoMeeting\SkettoMeetingText243_889_2018-04-30-15-07-03.txt','kitatata');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (890,'kimys1986@naver.com',to_date('18/04/30','RR/MM/DD'),299,'C:\SkettoMeeting\SkettoMeetingText299_890_2018-04-30-18-57-23.txt','프리젠테이션 준비');
Insert into SKETTO.MEETING (MEETINGNO,ID,MEETINGDATE,PJNO,MEETINGTEXT,MEETINGTITLE) values (871,'kimys1986@naver.com',to_date('18/04/28','RR/MM/DD'),231,'C:\SkettoMeeting\SkettoMeetingText231_871_2018-04-28-13-37-00.txt','테스트01');
REM INSERTING into SKETTO.MEETINGMEMLIST
SET DEFINE OFF;
Insert into SKETTO.MEETINGMEMLIST (MEETINGNO,ID,NAME) values (889,'1113@1113.com','1113ㅇ');
Insert into SKETTO.MEETINGMEMLIST (MEETINGNO,ID,NAME) values (882,'kimys1986@gmail.com','김영성');
Insert into SKETTO.MEETINGMEMLIST (MEETINGNO,ID,NAME) values (890,'kimys1986@naver.com','1111');
Insert into SKETTO.MEETINGMEMLIST (MEETINGNO,ID,NAME) values (886,'thggbtmb@gmail.com','拓哉');
REM INSERTING into SKETTO.MEMBER
SET DEFINE OFF;
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('TESTID','TESTPW','TESTNAME');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('123','123','123');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testest','testest','testest');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('TESTID1','TESTPW1','TESTNAME1');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('David','dbdbdip','kim');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('bbb','bbb','bbb');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('TEST_choi_joo_young','TESTPW_pwpwpw','TESTNAME_jooyoung');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('123456','123456','123456');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('onemorething','root','steve');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('789456','789456','789456');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('tttt','tttt','tttt');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('하이','하이','하이');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('hello','hello','hellojooyoung');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('qtw0626@gmail.com','111111','박소정');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('kimys1986@naver.com','1111','1111');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('ddddd','12345','yys');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('jyncho@naver.com','sketto','블레스유');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('000nectar@naver.com','1','박소정');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('kimys1986@gmail.com','1234','김영성');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('ㅑㅓㅑㅓㅑㅓ','ㅐㅐ','ㅏㅐㅏㅐㅏ');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('ㄴㄹㅈㄷㄹ','sqsqsqsq','qsqwqwqw');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('adsfwe','werwerwerwwefrw','dsfsdfsdw');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('dwdwww','wdwwwww','wqwqwqw');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('thggbtmb@gmail.com','2222','拓哉');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('1113@1113.com','1113','1113ㅇ');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testJooyoung1','testJooyoung1','sad');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testJooyoung2','testJooyoung2','happy');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testJooyoung3','testJooyoung2','anger');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testJooyoung4','testJooyoung2','disgusted');
Insert into SKETTO.MEMBER (ID,PW,NAME) values ('testJooyoung5','testJooyoung2','fear');
REM INSERTING into SKETTO.PJINVITEDLIST
SET DEFINE OFF;
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (224,'qtw0626@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (223,'qtw0626@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (224,'jyncho@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (225,'kimys1986@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (226,'000nectar@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (243,'thggbtmb@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (298,'ikkk@ikkk.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (298,'iooo@iooo.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (298,'12333@eeee.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (314,'kio@kio.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (223,'alex26park@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (243,'qtw0626@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (242,'qtw0626@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (228,'000nectar@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (228,'thggbtmb@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (232,'thggbtmb@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (242,'kimys1986@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (223,'1113@1113.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (297,'000nectar@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (294,'qtw0626@gmail.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (336,'k@k.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (323,'000nectar@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (323,'kimys1986@naver.com');
Insert into SKETTO.PJINVITEDLIST (PJNO,ID) values (340,'123@332.com');
REM INSERTING into SKETTO.PJMEMLIST
SET DEFINE OFF;
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('000nectar@naver.com',226);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',223);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('000nectar@naver.com',223);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',226);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('TESTID',228);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',229);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',298);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',231);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('hello',232);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',299);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('123',234);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',232);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',232);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',241);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('jyncho@naver.com',242);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',229);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('1113@1113.com',243);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',229);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('1113@1113.com',231);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('123456',231);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('TESTID1',231);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',336);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('1113@1113.com',223);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',323);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('000nectar@naver.com',323);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',323);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',338);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',294);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',294);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',297);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',300);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',298);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('qtw0626@gmail.com',298);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',299);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',338);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',336);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',336);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',339);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',339);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',340);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',341);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',341);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@naver.com',305);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',305);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',299);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('thggbtmb@gmail.com',314);
Insert into SKETTO.PJMEMLIST (ID,PJNO) values ('kimys1986@gmail.com',314);
REM INSERTING into SKETTO.PLAN
SET DEFINE OFF;
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (162,'""',to_date('18/04/16','RR/MM/DD'),to_date('18/04/17','RR/MM/DD'),21,'["ChoiId"]',5,0.4,'"#FF0000"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (222,'"이게 대체 무슨 일이야"',to_date('18/04/27','RR/MM/DD'),to_date('18/04/27','RR/MM/DD'),44,'["KimYoungId","ParkId","ChoiId"]',5,0.4,'"#FFA7F9"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (164,'""',to_date('18/04/18','RR/MM/DD'),to_date('18/04/20','RR/MM/DD'),21,'["joId"]',5,0.4,'"#2200FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (165,'"추가테스트"',to_date('18/03/30','RR/MM/DD'),to_date('18/04/04','RR/MM/DD'),21,'["ChoiId"]',5,0.4,'"#CACFFF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (166,'""',to_date('18/04/11','RR/MM/DD'),to_date('18/04/13','RR/MM/DD'),21,'["ParkId"]',5,0.4,'"#FFFF5A"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (98,'""',to_date('18/04/26','RR/MM/DD'),to_date('18/04/28','RR/MM/DD'),21,'["KimGyeId","ParkId","ChoiId"]',3,0.4,'"#FF7626"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (223,'"일본가자"',to_date('17/12/31','RR/MM/DD'),to_date('17/12/31','RR/MM/DD'),21,'["joId","KimYoungId","KimGyeId","ParkId","ChoiId"]',3,0.4,'"#DAFFBA"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (157,'"테스트"',to_date('18/04/09','RR/MM/DD'),to_date('18/04/11','RR/MM/DD'),21,'["ParkId"]',5,0.4,'"#FF9CDB"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (169,'"die죠부"',to_date('18/04/21','RR/MM/DD'),to_date('18/04/22','RR/MM/DD'),21,'["KimGyeId","ParkId","ChoiId"]',5,0.4,'"#FF3BA3"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (229,'"1"',to_date('18/04/29','RR/MM/DD'),to_date('18/04/29','RR/MM/DD'),231,'["KimYoungId","KimGyeId","ParkId"]',5,0.4,'"#FF3472"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (225,'"adassd"',to_date('18/04/28','RR/MM/DD'),to_date('18/04/28','RR/MM/DD'),44,'["joId"]',5,0.4,'"#FFD0E8"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (226,'"vvvvvv"',to_date('18/04/17','RR/MM/DD'),to_date('18/04/19','RR/MM/DD'),21,'["ChoiId"]',3,0.4,'"#FF143C"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (235,'"플랜생겨라"',to_date('18/05/02','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'),231,'["joId","KimYoungId","KimGyeId","ParkId"]',5,0.4,'"#FF6FA2"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (238,'"sxsxs"',to_date('18/04/04','RR/MM/DD'),to_date('18/04/26','RR/MM/DD'),242,'["ChoiId"]',5,0.4,'"#EFFF8A"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (232,'"2"',to_date('18/04/30','RR/MM/DD'),to_date('18/05/02','RR/MM/DD'),231,'["KimYoungId","KimGyeId"]',5,0.4,'"#0E77FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (220,'"오아시스"',to_date('18/04/27','RR/MM/DD'),to_date('18/04/27','RR/MM/DD'),44,'["ChoiId"]',5,0.4,'"#8BE0FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (233,'"ㅎㅎ"',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'),231,'["joId","KimYoungId","KimGyeId","ParkId","ChoiId"]',5,0.4,'"#FF2551"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (234,'"되라되라"',to_date('18/05/01','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'),231,'["joId","KimYoungId","KimGyeId"]',5,0.4,'"#6FF3FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (269,'"ㅁㄴㅁㄴㅇ"',to_date('18/03/25','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'),242,'[]',5,0.4,'"#7452FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (244,'"kio"',to_date('18/04/30','RR/MM/DD'),to_date('18/05/01','RR/MM/DD'),241,'["joId","ChoiId"]',5,0.4,'"#9BFFF5"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (245,'"이력서 등록"',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'),299,'["KimYoungId"]',5,0.4,'"#A2ECFF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (246,'"asdfasdf"',to_date('18/04/05','RR/MM/DD'),to_date('18/05/12','RR/MM/DD'),223,'["KimYoungId"]',5,0.4,'"#DF87FF"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (247,'"오빠 일 좀 해"',to_date('18/04/01','RR/MM/DD'),to_date('18/06/09','RR/MM/DD'),223,'["ParkId"]',5,0.4,'"#24FF84"');
Insert into SKETTO.PLAN (PNO,PLANNAME,PSTARTDATE,PENDDATE,PJNO,ID,PRIORITY,PROGRESS,COLOR) values (253,'"프로젝트 영상촬영"',to_date('18/04/30','RR/MM/DD'),to_date('18/05/02','RR/MM/DD'),299,'["KimYoungId","KimGyeId"]',5,0.4,'"#FF2E1B"');
REM INSERTING into SKETTO.PROJECT
SET DEFINE OFF;
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (225,'dddfdfd','kimys1986@naver.com',to_date('18/05/16','RR/MM/DD'),to_date('18/05/30','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (21,'바뀌냐눈','TESTID',to_date('18/03/21','RR/MM/DD'),to_date('18/06/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (223,'1234','000nectar@naver.com',to_date('18/04/03','RR/MM/DD'),to_date('18/04/04','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (44,'야호','TESTID',to_date('18/03/03','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (224,'개인기술발표','kimys1986@naver.com',to_date('18/04/24','RR/MM/DD'),to_date('18/04/27','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (226,'박소정이 리더인 프로젝트','qtw0626@gmail.com',to_date('18/04/04','RR/MM/DD'),to_date('18/04/25','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (227,'23rwefasd','000nectar@naver.com',to_date('18/04/04','RR/MM/DD'),to_date('18/04/26','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (228,'Much','TESTID',to_date('18/04/25','RR/MM/DD'),to_date('18/04/30','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (229,'tttt','kimys1986@naver.com',to_date('18/04/11','RR/MM/DD'),to_date('18/04/20','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (298,'kimysys','kimys1986@naver.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (231,'테스트1','kimys1986@naver.com',to_date('18/04/27','RR/MM/DD'),to_date('18/04/29','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (232,'hellopj','hello',to_date('18/04/27','RR/MM/DD'),to_date('18/04/30','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (299,'일본취업','kimys1986@naver.com',to_date('18/04/29','RR/MM/DD'),to_date('18/06/30','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (234,'z','123',to_date('18/04/27','RR/MM/DD'),to_date('18/04/28','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (241,'ggpj','thggbtmb@gmail.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/02','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (242,'sdsd','jyncho@naver.com',to_date('18/04/29','RR/MM/DD'),to_date('18/06/10','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (243,'z','1113@1113.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/01','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (338,'popop','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/04','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (294,'adfasdf','qtw0626@gmail.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/19','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (295,'adfasdf','qtw0626@gmail.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/19','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (297,'hellop','kimys1986@gmail.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/04','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (300,'kkkkk','kimys1986@naver.com',to_date('18/04/29','RR/MM/DD'),to_date('18/05/04','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (328,'iikik','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (329,'iikik','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (336,'kiiii','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (331,'iikkk','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (305,'테스트2','kimys1986@naver.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/31','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (314,'ggpjte','thggbtmb@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (323,'테스트','qtw0626@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/19','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (332,'iikkk','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (309,'ggpjte','thggbtmb@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (337,'kiiii','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (311,'ggpjte','thggbtmb@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (312,'ggpjte','thggbtmb@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (313,'ggpjte','thggbtmb@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (326,'iikik','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (333,'iikkk','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (334,'iikkk','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (335,'iikkk','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/03','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (339,'qokwoi','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (340,'qokwoioo','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/05','RR/MM/DD'));
Insert into SKETTO.PROJECT (PJNO,PJTITLE,ID,STARTDATE,ENDDATE) values (341,'13123','kimys1986@gmail.com',to_date('18/04/30','RR/MM/DD'),to_date('18/05/04','RR/MM/DD'));
--------------------------------------------------------
--  DDL for Index CHECKBOX_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "SKETTO"."CHECKBOX_PK" ON "SKETTO"."CHECKBOX" ("CNO") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;
--------------------------------------------------------
--  Constraints for Table CHATLIBRARY
--------------------------------------------------------

  ALTER TABLE "SKETTO"."CHATLIBRARY" MODIFY ("MEETINGNO" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."CHATLIBRARY" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."CHATLIBRARY" ADD PRIMARY KEY ("FILENUM")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PJINVITEDLIST
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PJINVITEDLIST" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MEETING
--------------------------------------------------------

  ALTER TABLE "SKETTO"."MEETING" ADD UNIQUE ("MEETINGTITLE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."MEETING" ADD PRIMARY KEY ("MEETINGNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."MEETING" MODIFY ("PJNO" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."MEETING" MODIFY ("MEETINGDATE" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."MEETING" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PJMEMLIST
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PJMEMLIST" ADD PRIMARY KEY ("ID") DISABLE;
  ALTER TABLE "SKETTO"."PJMEMLIST" MODIFY ("PJNO" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MEMBER
--------------------------------------------------------

  ALTER TABLE "SKETTO"."MEMBER" ADD PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."MEMBER" MODIFY ("NAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PLAN
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PLAN" ADD PRIMARY KEY ("PNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."PLAN" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."PLAN" MODIFY ("PJNO" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."PLAN" MODIFY ("PLANNAME" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PROJECT
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PROJECT" ADD PRIMARY KEY ("PJNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."PROJECT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "SKETTO"."PROJECT" MODIFY ("PJTITLE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CHECKBOX
--------------------------------------------------------

  ALTER TABLE "SKETTO"."CHECKBOX" ADD CONSTRAINT "CHECKBOX_PK" PRIMARY KEY ("CNO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "SKETTO"."CHECKBOX" MODIFY ("CNO" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table CHATLIBRARY
--------------------------------------------------------

  ALTER TABLE "SKETTO"."CHATLIBRARY" ADD CONSTRAINT "CHATID_FK" FOREIGN KEY ("ID")
	  REFERENCES "SKETTO"."MEMBER" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "SKETTO"."CHATLIBRARY" ADD CONSTRAINT "MEETINGNO_FK" FOREIGN KEY ("MEETINGNO")
	  REFERENCES "SKETTO"."MEETING" ("MEETINGNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MEETING
--------------------------------------------------------

  ALTER TABLE "SKETTO"."MEETING" ADD CONSTRAINT "MEETINGID_FK" FOREIGN KEY ("ID")
	  REFERENCES "SKETTO"."MEMBER" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "SKETTO"."MEETING" ADD CONSTRAINT "MEETINGPJNO_FK" FOREIGN KEY ("PJNO")
	  REFERENCES "SKETTO"."PROJECT" ("PJNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MEETINGMEMLIST
--------------------------------------------------------

  ALTER TABLE "SKETTO"."MEETINGMEMLIST" ADD CONSTRAINT "ID_MML_FK" FOREIGN KEY ("ID")
	  REFERENCES "SKETTO"."MEMBER" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "SKETTO"."MEETINGMEMLIST" ADD CONSTRAINT "MEETINGNO_MML_FK" FOREIGN KEY ("MEETINGNO")
	  REFERENCES "SKETTO"."MEETING" ("MEETINGNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PJINVITEDLIST
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PJINVITEDLIST" ADD CONSTRAINT "INVITEDPJNO_FK" FOREIGN KEY ("PJNO")
	  REFERENCES "SKETTO"."PROJECT" ("PJNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PJMEMLIST
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PJMEMLIST" ADD CONSTRAINT "MEMID_FK" FOREIGN KEY ("ID")
	  REFERENCES "SKETTO"."MEMBER" ("ID") ON DELETE CASCADE ENABLE;
  ALTER TABLE "SKETTO"."PJMEMLIST" ADD CONSTRAINT "MEMPJNO_FK" FOREIGN KEY ("PJNO")
	  REFERENCES "SKETTO"."PROJECT" ("PJNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PLAN
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PLAN" ADD CONSTRAINT "PLANPJNO_FK" FOREIGN KEY ("PJNO")
	  REFERENCES "SKETTO"."PROJECT" ("PJNO") ON DELETE CASCADE ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PROJECT
--------------------------------------------------------

  ALTER TABLE "SKETTO"."PROJECT" ADD CONSTRAINT "ID_FK" FOREIGN KEY ("ID")
	  REFERENCES "SKETTO"."MEMBER" ("ID") ON DELETE CASCADE ENABLE;
