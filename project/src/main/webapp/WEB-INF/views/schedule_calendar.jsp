<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page errorPage="error" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link
	href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"
	rel="stylesheet" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js">
</script> 

 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<style>

div {
	font-size: 1.5vw;
}
/* 
.boxCalendar {
	display: inline-block;
	background-color: red''
	width: 50px;
	height: 50px;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}
 */
.boxCalendarParent {
	position: relative;
	overflow: hidden;
	display: inline-block;
	width: 10vw;
	height: 10vw;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.planClass {
	position: static;
	top: auto;
	left: auto;
	display: inline-block;
	width: 100%;
	height: 1vw;
	border: 0px solid #aaaaaa;
	background-color: coral;
}
</style>

<script src="resources/moment-with-locales.js"></script>
<script type="text/javascript">

var planId;

	/* 프로젝트 시작일과 마감일 받고 시작할 것 */

	var startdate = moment('${pj.startdate}').format('YYYY-MM-DD-hh-mm-ss');
	var enddate = moment('${pj.enddate}').format('YYYY-MM-DD-hh-mm-ss');

	console.log('원래 프로젝트 기간:::' + startdate + '~' + enddate);

	var firstDate = stringToDate(startdate);
	console.log('firstDate ::::::::' + firstDate);
	var firstDateYear = moment(firstDate).format('YYYY');
	console.log('firstDateYear ::::::::' + firstDateYear);
	firstDate = new Date(firstDateYear, firstDate.getMonth(), 1);
	startdate = moment(firstDate).format('YYYY-MM-DD-hh-mm-ss');

	var lastDate = stringToDate(enddate);
	console.log('lastDate ::::::::' + lastDate);
	var lastDateYear = moment(lastDate).format('YYYY');
	console.log('lastDateYear ::::::::' + lastDateYear);
	lastDate = new Date(lastDateYear, lastDate.getMonth() + 1, 0);

	enddate = moment(lastDate).format('YYYY-MM-DD-HH-mm-ss');

	console.log('달력 출력:::' + startdate + '~' + enddate);

	/* 	var startdate = "2018-04-01-00-00-00";
	 var enddate = "2018-06-30-00-00-00"; */
	//프로젝트 종료일 추가 / 변경 가능하도록 할 것(1개월 추가, 3개월 추가 등)
	//혹은 시작/종료를 초과하는 일정이 추가되면 시작일과 종료일이 조정되도록.
	//진행도가 하단에 추가되는 것도 고려할 것.
	var todayDate = new Date();
	var todayDateToString = moment(todayDate).format('YYYY-MM-DD-HH-mm-ss');

	var todayDateXval = null; //아래의 calDateRange에서 값을 초기화할 계획.
	//todayDateXval = calDateRange(startdate, todayDateToString); 아래에 이거 쓰여져 있음

	var scheduleVariety = 10;
	//기본적인 스케쥴러 행 수. 이를 조절하면 11, 12줄... 30줄... 등 열의 증감이 가능하다

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function calDateRange(startdate, enddate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크 startdate
		if (startdate.length != 19) {
			return null;
		}

		// FORMAT을 포함한 길이 체크 enddate 
		if (enddate.length != 19) {
			return null;
		}

		// FORMAT이 있는지 체크
		if (startdate.indexOf(FORMAT) < 0 || enddate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var start_dt = startdate.split(FORMAT);
		var end_dt = enddate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		start_dt[1] = (Number(start_dt[1]) - 1) + "";
		end_dt[1] = (Number(end_dt[1]) - 1) + "";

		var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2],
				00, 00, 00);
		var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2], 00,
				00, 00);

		if (from_dt > to_dt) {
			return parseInt((from_dt.getTime() - to_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else {
			return parseInt((to_dt.getTime() - from_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		}
	}

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function stringToDate(varDate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크
		if (varDate.length != 19)
			return null;

		// FORMAT이 있는지 체크
		if (varDate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var var_dt = varDate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		var_dt[1] = (Number(var_dt[1]) - 1) + "";

		var return_dt = new Date(var_dt[0], var_dt[1], var_dt[2], var_dt[3],
				var_dt[4], var_dt[5]);

		return return_dt;
	}

	function pageLoad() {

		document.getElementById('scheduleDiv').innerHTML = "";

		var varDate = stringToDate(startdate);
		//startdate를 stringToDate() 메소드를 사용해서 date 포맷으로 변환한 것.
		//이걸 통해서 요일이나 월 등을 분석할 때 써먹는다. 하루씩 더해가지고 getMonth하고 getDay하고

		var varDate = stringToDate(startdate);

		var schedulePeriod = calDateRange(startdate, enddate) + 1;

		document.getElementById('scheduleDiv').innerHTML += '<font size="0.2em">';
		
		/* 여기서부터는 일요일 표시, 기준 점 작성 */
		for (var j = 0; j <= schedulePeriod; j++) {
			
			var newDiv = document.createElement('DIV'); // DIV 객체 생성

			newDiv.setAttribute('class', 'boxCalendarParent');
			newDiv.innerHTML += varDate.getDate();
			newDiv.innerHTML += '<input type="hidden" value='+moment(
					varDate).format('MMDD').toString()+'>';

			if (varDate.getDay() == 0) {
				newDiv.setAttribute('style', 'background-color:#d95b5b');
			}

			if (varDate.getDate() == 1) {
				document.getElementById('scheduleDiv').innerHTML += '<br>';
				document.getElementById('scheduleDiv').innerHTML += '<div>'+moment(
						varDate).format('MMMM').toString()+'</div>';

				if (varDate.getDay() != 0) {

					for (var iIns = 0; iIns < Number(varDate.getDay()); iIns++) {
						var blankDiv = document.createElement('DIV'); // DIV 객체 생성
						blankDiv.setAttribute('class', 'boxCalendarParent');
						blankDiv.innerHTML = '';
						document.getElementById('scheduleDiv').appendChild(
								blankDiv);
					}

				}
			}

			if (varDate.getDay() == 0) {
				document.getElementById('scheduleDiv').innerHTML += '<br>';
			}

			if (varDate.getDate() == 1) {
				//newDiv.setAttribute('style', 'background-color:blue');
			}

			if (moment(varDate).format('YYYYMMDD') == moment(todayDate).format(
					'YYYYMMDD')) {
				newDiv.setAttribute('name', 'today');
				document.getElementById('scheduleDiv').innerHTML += '<input type="hidden" id="todayToScroll">';
			}

			var vardateLastDateofThisMonth = new Date(varDate.getYear(),
					varDate.getMonth() + 1, 0);

			//1일이 될 때마다 색을 부여한다.
			//일주일이 될 때마다 색을 부여한다.

			var varDateMMDD = moment(varDate).format('YYYYMMDD');

			newDiv.setAttribute('id', 'dropon' + varDateMMDD); // x값 지정
			newDiv.setAttribute('class', 'boxCalendarParent');

			document.getElementById('scheduleDiv').appendChild(newDiv);

			if (varDate.getDate() == vardateLastDateofThisMonth.getDate()) {
				if (varDate.getDay() != 6) {
					for (var iIns = 0; iIns < 7 - (varDate.getDay() + 1); iIns++) {
						var blankDiv = document.createElement('DIV'); // DIV 객체 생성
						blankDiv.setAttribute('class', 'boxCalendarParent');
						blankDiv.innerHTML = '';
						document.getElementById('scheduleDiv').appendChild(
								blankDiv);
					}
				}
			}

			varDate.setDate(varDate.getDate() + 1);
			if (varDate.getDate() == vardateLastDateofThisMonth.getDate()) {
				document.getElementById('scheduleDiv').innerHTML += '</div>';
			}
		}

	}

	todayDateXval = calDateRange(startdate, todayDateToString);

	function scheduleUpdate() {

		var yIns = 0;

		console.log('${planList}');

		//y축 값은 나중에 일정을 출력하면서 select에 order를 매기는 식으로 변경 가능.

		<c:forEach items="${planList}" var="planString">
		//리스트 받고 뿌려준다

		var pstartdate = '${planString.pstartdate}';
		pstartdate = pstartdate.substring(0, 19);
		pstartdate = pstartdate.replace(" ", "-");
		pstartdate = pstartdate.replace(":", "-");
		pstartdate = pstartdate.replace(":", "-");
		//javascript에서 이해하고 Date로 만들어 줄 수 있게 임의로 포맷에 끼워넣기

		var pstartdateAsDate = stringToDate(pstartdate);
		//stringToDate로 해당 날짜가 Date type이 됐어.

		var penddate = '${planString.penddate}';
		penddate = penddate.substring(0, 19);
		penddate = penddate.replace(" ", "-");
		penddate = penddate.replace(":", "-");
		penddate = penddate.replace(":", "-");

		var penddateAsDate = stringToDate(penddate);

		var color = '${planString.color}';
		color = color.replace("\"", "");
		color = color.replace("\"", "");

		var xStartDate = calDateRange(startdate, pstartdate);
		//x값이 시작되는 위치.. 필요없지만 하단에 관련 함수가 있어서 지우면 오류뜬다
		var xLengthIns = calDateRange(pstartdate, penddate);
		//해당 일정이 얼마나 진행되는가? 날짜의 갯수로

		var widthVal = (xLengthIns + 1);
		//전체 일정을 하려면 자기 자신을 세라.

		for (var i = 0; i < widthVal; i++) { //길이만큼의 div를 하나씩 배정할 예정
			if (document.getElementById('draggable' + '${planString.pno}'
					+ 'no' + i) != null) {
				$('#draggable' + '${planString.pno}' + 'no' + i).remove();
			}
			//기존에 있던 일정은 지워라

			var newDiv = document.createElement('DIV'); // DIV 객체 생성
			var xIns = xStartDate + i;

			// x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
			newDiv.setAttribute('i', i); // div의 순서. i값 지정
			newDiv.setAttribute('pno', '${planString.pno}');
			newDiv.setAttribute('pstartdate', pstartdate);
			newDiv.setAttribute('penddate', penddate);


			newDiv.setAttribute('planName', '${planString.planname}');
			newDiv.setAttribute('PJNo', '${planString.pjno}');
			newDiv.setAttribute('memberIdIns', '${planString.id}');
			newDiv.setAttribute('Priority', '${planString.priority}');
			newDiv.setAttribute('Progress', '${planString.progress}');
			newDiv.setAttribute('color', color);
			newDiv.setAttribute('xStartDate', xStartDate);
			newDiv.setAttribute('xLengthIns', xLengthIns);
			newDiv.setAttribute('onclick', "openPlan('${planString.pno}')");
			//newDiv.innerHTML += i; //i값 들어가게 해서 순번 확인하려고

			newDiv.setAttribute('widthVal', widthVal); // x값 지정
			newDiv.setAttribute('id', 'draggable' + '${planString.pno}' + 'no'
					+ i); // x값 지정

			newDiv.setAttribute('style', 'width: 100%;' + 'height: 2vw;'
					+ 'border: 0px solid #aaaaaa;' + 'background-color: '
					+ color);
			newDiv.setAttribute('onmouseover', "mouseOver(this)");
			newDiv.setAttribute('onmouseout', "mouseOut(this)");

			var tempDate = pstartdateAsDate;
			//pstartdateAsDate
			tempDate.setDate(pstartdateAsDate.getDate());
			var varDateMMDD = moment(tempDate).format('YYYYMMDD').toString();
			tempDate = pstartdateAsDate;
			//다시 초기화시킨다 : 왜 오류가 뜨는지 모르겠어
			var parentID = 'dropon' + varDateMMDD;
			
			if ( moment(todayDate).format('YYYYMMDD') == varDateMMDD) {
				newDiv.setAttribute('name', 'today');				
			}
			
			tempDate.setDate(pstartdateAsDate.getDate() + 1);

			if (document.getElementById(parentID) != null) {
				document.getElementById(parentID).appendChild(newDiv);
			} else {
			}

		}

		</c:forEach>
	}

	function openPlan(pno) {
		
		var popupWidth = 600;
		var popupHeight = 500;
		var popupX = (window.screen.width / 2) - (popupWidth / 2);
		// 만들 팝업창 좌우 크기의 1/2 만큼 보정값으로 빼주었음

		var popupY= (window.screen.height /2) - (popupHeight / 2);
		// 만들 팝업창 상하 크기의 1/2 만큼 보정값으로 빼주었음

		//pno를 갖고 컨트롤러로 이동한다
		window
				.open("getThePlan?pno=" + pno, "일정내용",
						'width='+popupWidth+'px, height='+popupHeight+'px, toolbar=no, menubar=no, scrollbars=no, resizable=yes left='
						+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	}
	
	//모달 적용해보자
	/* function openPlan(){
	     $("#myModal").modal();
	} */
	
	function mouseOver(divMouseOver) {

		divMouseOver
				.setAttribute(
						"title",
						(divMouseOver.getAttribute("pno") + " / "
								+ divMouseOver.getAttribute("planName") + " : "
								+ divMouseOver.getAttribute("pstartdate")
								+ " ~ " + divMouseOver.getAttribute("penddate")));
	}

	function mouseOut(divMouseOver) {

	}
	
	var intervalOn = false;
	var blinkIntervalBoolean = true;
	var blinkInterval = null;
	var getName = null;
	
		function blinking() {
			var blinkingObject = document.getElementsByName('today');

			intervalOn = !intervalOn;

			if (intervalOn) {
				clearInterval(blinkInterval);
				for (var i = 0; i < blinkingObject.length; i++) {
					if (blinkingObject[i].getAttribute('color') != null) {
						$(blinkingObject[i]).css("background-color",
								blinkingObject[i].getAttribute('color'));
					} else {
						$(blinkingObject[i]).css("background-color", "#ffcb5e");
					}
				}
				return;
			}


			blinkInterval = setInterval(
					function() {
						blinkIntervalBoolean = !blinkIntervalBoolean;
						console.log(blinkIntervalBoolean);

						for (var i = 0; i < document.getElementsByName('today').length; i++) {
							if (blinkIntervalBoolean) {
								$(blinkingObject[i]).css("background-color",
										"#ffcb5e");
							} else {
								if (blinkingObject[i].getAttribute('color') != null) {
									$(blinkingObject[i]).css(
											"background-color",
											blinkingObject[i]
													.getAttribute('color'));
								} else {
									$(blinkingObject[i]).css(
											"background-color", "#FFFFFF");
								}
							}
						}
					}, 500);
		}
		
</script>

</head>
<body>

	<div id="scheduleDiv"
		style="white-space: nowrap; overflow: auto; margin: 0px; padding: 0px;"
		align="center"></div>


	<script type="text/javascript">
		console.log(Number('${pjNO}'));

		pageLoad();

		scheduleUpdate();
	</script>


</body>
</html>