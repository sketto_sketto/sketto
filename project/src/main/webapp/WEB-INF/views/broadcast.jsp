<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <title>Testing websockets</title>
</head>
<body>
<h1 class="center" id="headline">음성인식 & 채팅테스트</h1>
    
    
    
    
	
<fieldset>
    <!-- 프로젝트 넘버 -->
	<input type="text" id="PJNO" style="width:49%;" value="ProjectNO.${mt.pjno }" readonly="readonly">
	
	<!-- 프로젝트 리더 -->
	<input type="text" id="PL" style="width:49%;" value="Project Leader : ${mt.id }" readonly="readonly">
	
	<!-- 회의 넘버 -->
	<input type="text" id="meetingNO" style="width:49%;" value="MeetingNO.${mt.meetingno }" readonly="readonly">
	
	<!-- 회의 제목 -->
	<input type="text" id="meetingText" style="width:49%;" value="${mt.meetingtext }" placeholder="Please create meeting name." >
	
	<!-- 채팅 참여자 -->
	<input type="text" id="userAll" width="500" style="width:100%;" placeholder="Input User ID" value="Participants : A, B, C, D">
	
    <!-- 채팅 아이디 -->
	<input type="text" id="userid" width="500" style="width:100%;" placeholder="Input User ID" value="${sessionScope.loginId }">
	
	<!-- <ul id="discussion" style="overflow-y:scroll; border:1px silver dotted; height:100px;"></ul> -->
    
    <!-- 새로운 채팅부분 -->
    <textarea id="messageWindow" rows="10" cols="50" readonly="true"></textarea><br/>
    <input id="inputMessage" type="text"/>
    <input type="submit" value="send" onclick="send()" />
    
    <!-- 음성인식 결과가 나타나는 부분 -->
	<div id="results" style="background-color: silver;">
		<span id="final_span" class="final"></span>
		<span id="interim_span" class="interim"></span>
		<p>
	</div>
	
	<!-- 파일 업로드 -->
    <form id="uploadForm" name="uploadForm" action="uploadFile" method="post" enctype="multipart/form-data">
    	<input type="file" name="upload" size="30">\
    	<input type="hidden" name="id" value="${loginId }">
    	<input type="hidden" name="meetingno" value="${mt.meetingno }">
    	<!-- originalfile, savedfile은 따로 input tag 작성 안 함 -->
    	<input type="button" id="submitFile" name="submitFile" value="파일 업로드">
    </form>
    
    <!-- 첨부된 파일이 있는 경우, 해당 파일을 다운로드 할 수 있는 링크 제공 -->
	<%-- <c:if test="${dlist != null && dlist.size()>=1}"> --%>
	<div id="downloads">
		파일 목록: 
		<%-- <c:forEach items="${dlist}" var="down">
			<a href="downloadFiles?filenum=${down.filenum}">
				${down.originalfile}
			</a>
		</c:forEach> --%>
	</div>
	<%-- </c:if> --%>
    
    <!-- 회의 종료 버튼 -->
    <input type="button" id="sendAll" value="회의 종료" width="500" style="width:100%;">
</fieldset>

<!-- 음성인식 스타트 버튼 -->
	<div class="right">
		<button id="start_button" onclick="startButton(event)">
		<img id="start_img" src="resources/image/mic.gif" alt="Start"></button>
	</div>
    
  	<!-- 언어선택 select -->
  	<div class="center">
	  <div id="div_language">
	    <select id="select_language" onchange="updateCountry()"></select>
	    &nbsp;&nbsp;
	    <select id="select_dialect"></select>
	  </div>
	  <br>
	  
	  <div class="sidebyside" style="text-align:right">
	    <!-- Copy&Paste 버튼 -->
	    <button id="copy_button" class="button" onclick="copyButton()">
	      Copy and Paste</button>  
	    <div id="copy_info" class="info">
	      Press Control-C to copy text.<br>(Command-C on Mac.)
	    </div>
	  </div>
	  
	  <div class="sidebyside">
	    <!-- Email 버튼 -->
	    <button id="email_button" class="button" onclick="emailButton()">
	      Create Email</button>
	    <div id="email_info" class="info">
	      Text sent to default email application.<br>
	      (See chrome://settings/handlers to change.)
	    </div>
	  </div>
	  <p>
	  
	  
	</div>

<!-- 음성인식 진행상태를 나타내는 문구 -->    
<div id="info">
	  <p id="info_start">Click on the microphone icon and begin speaking.</p>
	  <p id="info_speak_now">Speak now.</p>
	  <p id="info_no_speech">No speech was detected. You may need to adjust your
	    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
	      microphone settings</a>.</p>
	  <p id="info_no_microphone" style="display:none">
	    No microphone was found. Ensure that a microphone is installed and that
	    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
	    microphone settings</a> are configured correctly.</p>
	  <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
	  <p id="info_denied">Permission to use microphone was denied.</p>
	  <p id="info_blocked">Permission to use microphone is blocked. To change,
	    go to chrome://settings/contentExceptions#media-stream</p>
	  <p id="info_upgrade">Web Speech API is not supported by this browser.
	     Upgrade to <a href="//www.google.com/chrome">Chrome</a>
	     version 25 or later.</p>
	  <p id="info_stop">Click on the microphone icon and begin speaking.</p>
</div>

<!-- 언어선택 데이터 -->
<script>
//채팅관련 부분
var connection = $.hubConnection('http://demo.dongledongle.com/');
var chat = connection.createHubProxy('chatHub');

//채팅관련 부분 - defined for Sketto
var sysdate;
var discussionText;
var discussionTotal;
var projectNO = document.getElementById('PJNO');
var projectLeder = document.getElementById('PL');
var meetingNO = document.getElementById('meetingNO');
var meetingText = document.getElementById('meetingText');	//회의록 파일 제목이 아닌 회의 자체의 타이틀
var fileNO= document.getElementById('upload');


$(document).ready(function () {

    chat.on('addNewMessageToPage', function (name, message) {
        $('#discussion').append('<li><strong>' + htmlEncode(name) + '</strong> ' + htmlEncode(message) + '</li>');
        //alert($('#discussion').text());
        /*$('#discussion').each(function(index, element){
        	discussionTotal = $(element).text();
        });*/
        discussionTotal = $('#discussion li:last-child').text();
        discussionTotal += "\r\n";
      	//누를 때마다 하나씩 DB에 텍스트 파일로 저장
        sendvoice();
        $('#discussion').scrollTop($('#discussion').prop('scrollHeight')); 
    });

    $('#message').focus();

    connection.start({ jsonp: true }).done(function () {

        $('#btnSend').click(function () {	//getMonth()가 0~11인 관계로 getMonth()에서만 parseInt처리. 숫자와 문자열이 혼재되어 뒤의 처리에 지장 있을 수 있음.
            sysdate = new Date();
        	discussionText = "["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"]: "+final_transcript;
        	chat.invoke('send', $('#userid').val(), discussionText);
            $('#message').val('').focus();
        });
        
        $('#submitFile').click(function(){
        	var formData = new FormData();
        	formData.append("id", $("input[name=id]").val()); 
        	formData.append("meetingno", $("input[name=meetingno]").val()); 
        	formData.append("upload", $("input[name=upload]")[0].files[0]); 
        	$.ajax({
        		url: 'uploadFile'
        		, data: formData
        		, processData: false
        		, contentType: false
        		, type: 'POST'
        		, success: function(ch){
        			sysdate = new Date();
        			alert("파일을 등록하였습니다.\n"+ch.originalfile);
        			discussionText = "["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"](파일등록): "+ch.originalfile;
        			chat.invoke('send', $('#userid').val(), discussionText);
        			$('div#downloads').append('<a href = downloadFile?filenum='+ch.filenum+'>'+' '+ ch.originalfile +' '+'</a>')
        			$('#message').val('').focus();
        		}	//회의 시작하자마자 파일을 올리는 것은 회의록에 반영되지 않고 있음. 반드시 음성 텍스트가 입력된 뒤에만 가능함. 왜 그럴까.
        		, error: function(error){
        			alert(JSON.stringify(error));
        		}
        	});
        });

        
        $('#sendAll').click(function(){
        	//아님 이거 누르면 한꺼번에 저장?
        	alert('회의록을 저장합니다.');
        	//location.href="saveMeetingText";	//Controller의 메소드로 연결해야 함. 지금은 .jsp파일을 찾음ㅡㅡ;
        });
    });
});

function sendvoice() {
	//alert("보내는 텍스트: "+final_transcript);	//success는 되지만 값이 출력되었다가 안 되었다가 함. 여기서 final_transcript를 출력해주지 않으면 뒤에도 전부 공란임.

  $.ajax({
        url : 'voice',
        type : 'GET',
        dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
        //traditional:true,
        data : {'voice': discussionTotal},
        //data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
        //contentType:"application/json",
        success : function(voice) {
            //alert('성공\n'+voice);
        },
        error : function(error) {
            alert('에러발생\n'+JSON.stringify(error));
        }
    });

}

function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}


//음성인식 관련 부분
var langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];
for (var i = 0; i < langs.length; i++) {
  select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 6;
updateCountry();
select_dialect.selectedIndex = 6;
showInfo('info_start');


function updateCountry() {
  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}
var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;

if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'resources/image/mic-animate.gif';
  };
  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = 'resources/image/mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = 'resources/image/mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };
  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = 'resources/image/mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
    if (create_email) {
      create_email = false;
      createEmail();
    }
  };
  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
    if (final_transcript || interim_transcript) {
      showButtons('inline-block');
    }
  };
}
function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}
var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

/* 이메일 전송 */
function createEmail() {
  var n = final_transcript.indexOf('\n');
  if (n < 0 || n >= 80) {
    n = 40 + final_transcript.substring(40).indexOf(' ');
  }
  var subject = encodeURI(final_transcript.substring(0, n));
  var body = encodeURI(final_transcript.substring(n + 1));
  window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
}

/* 텍스트 복사버튼 클릭시  */
function copyButton() {
	if (recognizing) {
		recognizing = false;
		recognition.stop();
		}
	//alert()
	copy_button.style.display = 'none';
	copy_info.style.display = 'inline-block';
	showInfo('');
	//final_transcript에 인식된 텍스트가 담겨있음을 확인
	//alert(final_transcript);
}


/* 이메일 전송버튼 클릭시  */
function emailButton() {
  if (recognizing) {
    create_email = true;
    recognizing = false;
    recognition.stop();
  } else {
    createEmail();
  }
  email_button.style.display = 'none';
  email_info.style.display = 'inline-block';
  showInfo('');
}

/* 음성인식 동작 버튼 */
function startButton(event) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.lang = select_dialect.value;
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = 'resources/image/mic-slash.gif';
  showInfo('info_allow');
  showButtons('none');
  start_timestamp = event.timeStamp;
}

/* 음성인식 동작시 상태 표시  */
function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}
var current_style;

/* 음성을 검출시 버튼이 화면에 표시 */
function showButtons(style) {
  if (style == current_style) {
    return;
  }
  current_style = style;
  copy_button.style.display = style;
  email_button.style.display = style;
  copy_info.style.display = 'none';
  email_info.style.display = 'none';
}
</script>
    
</body>
    <script type="text/javascript">
        var textarea = document.getElementById("messageWindow");
        var webSocket = new WebSocket('ws://localhost:8888/web5/broadcasting');
        var inputMessage = document.getElementById('inputMessage');
    webSocket.onerror = function(event) {
      onError(event)
    };
    webSocket.onopen = function(event) {
      onOpen(event)
    };
    webSocket.onmessage = function(event) {
      onMessage(event)
    };
    function onMessage(event) {
        textarea.value += "상대 : " + event.data + "\n";
    }
    function onOpen(event) {
        textarea.value += "연결 성공\n";
    }
    function onError(event) {
      alert(event.data);
    }
    function send() {
        textarea.value += "나 : " + inputMessage.value + "\n";
        webSocket.send(inputMessage.value);
        inputMessage.value = "";
    }
  </script>
</html>