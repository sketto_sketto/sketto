<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<style type="text/css">
.Bar {
	height: 20px;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
}

#divEmptyBar {
	width: 100px;
	background-color: #000000;
	border: thin 1px solid black;
}

#divProgressBar {
	width: 0px;
	background-color: #FF0000;
}
</style>

<script type="text/javascript">
	function onClickCounting() {

		var checkboxInput = document.getElementsByName("checkBoxName");
		var checkboxCount = 0;
		for (var i = 0; i < checkboxInput.length; i++) {
			if (checkboxInput[i].type == "checkbox" && checkboxInput[i].checked == true) {
				checkboxCount++;
			}
		}

		var widthControl = Number($("#divEmptyBar").css("width").replace("px",""))
						* (checkboxCount / checkboxInput.length);
		
		$("#divProgressBar").css("width", widthControl);
		
	}

	//json 생성
	/* 			var checkBoxInsJSON = {
				"cno" : cnoIns,
				"pno" : pnoIns,
				"pjno" : pjnoIns,
				"text" : textIns,
				"checked" : checkedIns
				}; */
	
	//작업 과정 분류
	var checkBoxProcess = null;
	var urlIns = null;
	if (checkBoxProcess == "insert") {
		urlIns = "checkBoxInsert";
	}
	
	if (checkBoxProcess == "update") {
		urlIns = "checkBoxUpdate";
	}
	
	if (checkBoxProcess == "delete") {
		urlIns = "checkBoxDelete";
	}	
	
	//ajax 시작
	/* $
	.ajax({
		url : urlIns,
		type : 'GET',
		dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
		data : {
			'jsonString' : JSON.stringify(checkBoxInsJSON)
		},
		success : function(planList) {},
		error : {}
	}); */

</script>

</head>
<body>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<div id="divEmptyBar" class="Bar">
		<div id="divProgressBar" class="Bar"></div>
	</div>

	<div id="checkBoxDiv"></div>
	
<script type="text/javascript">
<c:forEach items="${checkedList}" var="checkBox">

	var inputEach = "<input type=checkbox name=checkBoxName";
	inputEach += " id=checkBox" + ${checkBox.cno};
	inputEach += " onclick='javascript: onClickCounting()'>";
	console.log(checkBox);
	console.log('${checkBox}');
	var textEach = "<div id=checkBoxText" + ${checkBox.cno};
	textEach += " onclick=renameText()><a>" + '$(checkBox)'.text; 
	textEach += "</a></div><br>";
	
	$('#checkBoxDiv').innerHTML += inputEach + textEach;
	console.log($('#checkBoxDiv').innerHTML);
</c:forEach>
</script>


   <input type="checkbox" name="checkBoxName" id="checkBoxId1"
      onclick="javascript: onClickCounting()">
   <div onclick="renameText"></div>
   <br>
   <input type="checkbox" name="checkBoxName" id="checkBoxId2"
      onclick="javascript: onClickCounting()">
   <br>
   <input type="checkbox" name="checkBoxName" id="checkBoxId3"
      onclick="javascript: onClickCounting()">
   <br>
   <input type="checkbox" name="checkBoxName" id="checkBoxId4"
      onclick="javascript: onClickCounting()">
   <br>
   <input type="checkbox" name="checkBoxName" id="checkBoxId5"
      onclick="javascript: onClickCounting()">

</body>
</html>