<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="/error" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- 리더가 프로젝트를 생성하자마자 보이는 페이지 -->
<title>${pj.pjtitle}에 팀원을 초대하세요! - Sketto!</title>
<script src="resources/jquery-3.3.1.js"></script>

<script type="text/javascript">

	function searchmember() {
		var inputed = $('#searchText').val();
		
		$.ajax({
			url: 'memberlist'
			, type: 'post'
			, dataType: 'json'
			, data: {
				'searchText':inputed
			}
			, success: function(data){
				//console.log(data);
				if((data==null)||(data=='')){
					
					var str= '<h3 style="margin:2% 0 1%">'+inputed+'님을<br>'; 
					str += '스켓토에 초대하세요!<br></h3>';
					str += '<input type="button" style="background-color: #5c948d; border-color: #5c948d;" class="btn btn-primary sendInvite" value="초대" onclick="sendinvitedmail(\''+inputed+'\')">'
					str += '<br>'
					$('#idResult').html(str);
					
				} else {
					
					var str = '<table class="table">';
					$.each(data, function(idx, elem){
						str += '<form>';
						str += '<tr>';
						str += '<td id="'+elem.id+'">'+elem.id+'</td>';
						str += '<td><input type="button" style="background-color: #5c948d; border-color: #5c948d;"  class="btn btn-primary sendInvite" value="초대" onclick="insertintopj(\''+elem.id+'\')"></td>';
						str += '</tr>';
					});
					str +='</table>';
					
					$('#idResult').html(str);
				}
				
			}//success
			, error: function(err){
				//alert('초대에 실패했습니다\n메일주소가 정확한지 확인해주세요.');
				console.log(err);
			}
		});//ajax
	}//selectmemberlist
	
	//sketto!에 없는 사람들을 초대 
	function sendinvitedmail(email){
		var email = email;
		var pjno = ${pj.pjno};
		//alert(email+'에게 '+pjno+'번 프로젝트로의 초대 실행');
		console.log(email+'에게 '+pjno+'번 프로젝트로의 초대 실행');
		
		$.ajax({
			url: 'sendinvitedmail'
			, type: 'post'
			, dataType: 'json'
			, data: {
				'id' : email
				, 'pjno' : pjno
			}
			, success: function(data){
				//console.log(invtmsg);
				alert(email+'님께 초대 메일이 발송되었습니다.');
				loadinvitedlist(pjno);
			}//success
			, error: function(err){
				//alert('메일 발송 중 오류가 있었습니다');
				console.log(err);
			}
		});//ajax
	}//sendmailtest
	
	
	//기존에 이미 sketto에 계정이 있는 멤버들을 해당 프로젝트로 초대(insert)
	function insertintopj(email){
		var email = email;
		var pjno = ${pj.pjno };
		if (confirm(email+'님을 프로젝트로 초대합니다.\n계속하시겠습니까?')) {
			$.ajax({
				url: 'insertintopj'
				, type: 'POST'
				, dataType: 'json'
				, data: {
					'id': email
					, 'pjno': pjno
				}
				, success: function(data){
					alert(email+'님을 프로젝트 멤버로 초대했습니다.');
					loadinvitedlist(pjno);
					/*location.reload();
					window.opener.location.reload(); */
				}
				, error: function(err){
					//alert('오류가 발생했습니다.');
					consol.log(err);
				}
			});
		}
		else{
			alert('초대가 취소되었습니다.');
		}
	}

	function loadinvitedlist(pjno){
		//alert('loadinvitedlist 함수 실행');
		var pjno = ${pj.pjno};
		console.log(pjno);
		
		$.ajax({
			url: 'invitedlist'
			, type: 'post'
			, dataType: 'json'
			, data: {
				'pjno' : pjno
			}	
			, success: function(data){
				console.log(data);
				//alert('email 발송 리스트 불러오기');
				if((data==null)||(data=='')){
					var str = '';
					
					$('#listUpdate').html(str);
					
				} else {
					var str = '<hr style="margin-top: 15%;">';
						str += '<h3>초대한 리스트</h3>';	
						str += '<table class="table">';
					$.each(data, function(idx, elem){
						str += '<tr>';
						str += '<td id="'+elem.id+'">'+elem.id+'</td>';
						str += '</tr>';
					});
					str +='</table>';
					str += '<hr style="margin-top: 2%; margin-bottom: 15%;">';
				
					$('#listUpdate').html(str);
				}
				
			}//success
			, error: function(err){
				//alert('초대 리스트 오류입니다');
				console.log(err);
			}
		});//ajax
	
	}
	
	
</script>
<style type="text/css">
	/* 상단 바  */
	/* Add a black background color to the top navigation */
	.topnav {
	    background-color: #1f2944;
	    overflow: hidden;
	}
	
	/* Style the links inside the navigation bar */
	.topnav a {
	    float: left;
	    color: #fff;
	    text-align: center;
	    padding: 14px 16px;
	    text-decoration: none;
	    font-size: 17px;
    	margin-top: 15px;
	}
	
	/* Change the color of links on hover */
	.topnav a:hover {
	    background-color: #ddd;
	    color: black;
	}
	
	/* Add a color to the active/current link */
	.topnav a.active {
	    background-color: #5c948d;
	    color: white;
	}
	/* 상단 바 끝 */
	
	
	.wholediv {
		/* background-color: red; */
		text-align: center;
		width: 600px;
		margin: 0% auto;
	}
	.searchdiv {
		/* display: inline-block; */
		/* background-color: blue; */
		width: 100%;
	}
	.inviteddiv {
		/* display: inline-block; */
		/* background-color: yellow; */
	    width: 100%;
	}
	.godashbutton {
		width: 600px;
	    margin: 2% auto;
	}

	/*페이지 제목*/	
	h1 {
		color: #1f2944;
	    font-family: 'Droid Serif';
	    border-bottom: 3px solid #d95b5b;
	    border-top: 3px solid #d95b5b;
		font-size: 42px;
		font-weight: 400;
		line-height: 60px;
		margin: 1% auto 45px;
		max-width: 600px;
		padding: 45px 0;
		text-align: center;
	}
			
	/*페이지 제목 끝*/	
	input[type=text] {
	    background-color: white;
	    background-image: url('resources/img/search1.png');
	    background-position: 2px 1px;
	    background-repeat: no-repeat;
	    background-size: 39px;
	    padding-left: 45px;
	}
</style>
</head>
	
<body style="margin: 0%;">
	<!-- 상단 바 시작 -->
		<div id="topnav">
			<div class="topnav">
				<div class="topnavdiv">
					<a href="./"><img src="resources/img/logo2-white.png" style="height: 50px;margin-top: -20px;"></a>
					<a href="logout" >로그아웃</a>
 				<!--<a href="goplan">일정</a>
					<a href="goplanCalendar">일정_달력형</a>
					<a href="myScheduleToday">일정_개인형</a>
					<a href="goproject">프로젝트 기능 테스트</a>	
					<a href="meetingForm">회의생성/참여</a> -->
				</div>
			</div>
			
		</div>
	<!-- 상단 바 끝 -->
	
	
	
	<h1 style="text-align: center">${pj.pjtitle}</h1>
	<h2 style="text-align: center">함께할 팀원을 초대하세요</h2>

	<div class="wholediv">

		<div class="searchdiv">

			<input type="text" class="form-control form-control-lg"  id="searchText" name="searchText" placeholder="sketto@sketto.com" onkeyup="searchmember()">
			<!-- 검색 결과 나오는 div 영역-->
			<div id="idResult"> </div>
			<!-- 검색 결과 나오는 div 영역끝 -->
		</div>
		
		<div class="inviteddiv">
				<div id="listUpdate">
				</div>
		</div>
		
	</div>
	
	
	<div class="godashbutton" style="background-color: #5c948d; border-color: #5c948d;" >
		<a href="./"><input type="button" class="btn btn-primary" style="text-align: center; width: 100%; padding: 2% 0%; background-color: #5c948d; border-color: #5c948d;" value="대시보드로 가기"></a>
	</div>
		
	
</body>
</html>