<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<script src=<c:url value="resources/jquery-3.3.1.js"/>></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>	
<title>Sketto! - 부트스트랩 테스트</title>

<style type="text/css">
/* Style the video: 100% width and height to cover the entire window */
#myVideo {
    position: absolute;
    right: 0;
    bottom: 0;
    min-width: 100%; 
    min-height: 100%;
    
     z-index: 1;
}

/* Add some content at the bottom of the video/page */
.content {
    position: absolute;
    bottom: 0;
    right: 0;
    background: rgba(255, 255, 255, 0.5);
    color: #f1f1f1;
    width: 30%;
    padding: 20px;
    height: 100%;
     z-index: 1000;
}

/* Style the button used to pause/play the video */
#myBtn {
    width: 200px;
    font-size: 18px;
    padding: 10px;
    border: none;
    background: #000;
    color: #fff;
    cursor: pointer;
}

#myBtn:hover {
    background: #ddd;
    color: black;
}
		input[type=text], input[type=password] {
		    width: 100%;
		    padding: 12px 20px;
		    margin: 8px 0;
		    box-sizing: border-box;
		}
		
		input[type=submit]{
			background-color: #4c94af;
		    border: none;
		    color: white;
		    padding: 16px 32px;
		    text-decoration: none;
		    margin: 4px 2px;
		    cursor: pointer;
		    width: 100%;
		}
		
		form{
		    text-align: center;
		    margin-left: auto;
		    margin-right: auto;
		    width: 224px;
		}
		table{
			text-align: center;
		}
		img.logo{
			width:200px;
			margin-left:auto;
			margin-right:auto;
			display: block;
		}
		.logindiv{
			text-align: center;
		}
		.imghome{
			position: relative;
			top: 760px;
		}
 		img.home{
			width: 100%;
		} 
		
		
		
	.wholediv{
		width: 100%;
	    /* margin-left: auto; */
	    /* margin-right: auto; */
	    /* padding: 2%; */
	    text-align: center;
		margin: 10% auto;
	}
	.pjdetailicon {
	    margin-left: auto;
	    margin-right: auto;
	    width: 30%;
	    text-align: center;
	    border: solid black 1px;
	}
	.pjitems1 {
	    margin-left: auto;
	    margin-right: auto;
	    width: 50%;
	    text-align: center;
	    border: solid black 1px;
     padding: 3%;
	}
	.pjitems2 {
		position: relative;
 	 	width: 30%; /* 원하는 너비 */
  	 	margin: 1%;
  	 	text-align: center;
		display: inline-block;
	    /* border: solid black 1px; */
	}
	.pjitems2:before {
	    content: "";
	    display: block;
	    padding-top: 100%; /* 1:1 비율 */
	    /* border: solid black 1px; */
	}
	.pjitems2inner {
	    position: absolute;
	    top: 0;
	    right: 0;
	    bottom: 0;
	    left: 0;
	    border: solid black 1px;
	}
	.icon {
		width: 50%;
	    background: rgba(0,0,0,0.2);
	    margin-left: auto;
	    margin-right: auto;
	}
	footer {
		bottom:0;
	}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>

$(document).ready(function(){

    $("#topnav").load("http://localhost:8888/board/topnav");

});

</script>

</head>
<body>

	<!-- 비회원 접근일 때 로그인 창 띄우기 -->
	<c:if test="${loginId==null }">
		
			<!-- The video -->
			<video autoplay muted loop id="myVideo">
			  <source src="resources/video/meeting1.mp4" type="video/mp4">
			</video>
			
			<!-- Optional: some overlay text to describe the video -->
			<div class="content">
			<br><br><br><br><br><br><br>
				<div class="logindiv">
					<img src="resources/img/logo2.png" class="logo">
					<form id="login" action="logintoSketto" method="post">
					<table>
					<tr>
						<td><input type="text" name="id" id="id" placeholder="abc@sketto.com"/></td>
					</tr>
					<tr>
						<td><input type="password" name="password" id="password" /></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="errorMsg">
								${errorMsg}
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="submit" value="Login" />
						</td>
						
					</tr>
					<tr>
						<td colspan="2">
							<a href="joinform" style="color : #5a5a5a;">Join Sketto</a>
						</td>
					</tr>
					</table>
				</form>
				</div>
			</div><!-- headerlogin -->
	   
		<div class="imghome">
			<img src="resources/img/home2-1.png" class="home"><br>
			<img src="resources/img/home2-2.png" class="home"><br>
			<img src="resources/img/home2-3.png" class="home"><br>
		
		</div>
		
		
	</c:if><!-- 비회원 접근 뷰 -->
	
	
	
	<!-- 회원 로그인 시 -->
	
	
	<!-- 메뉴 -->
	<div class="container" style="width: 100%">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target="#navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span>
						</button>
						<!-- <a class="navbar-brand">Brand</a> -->
					</div>
					
				<c:if test="${loginid != null }">
					<div class="navbar-collapse collapse" id="navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="logout" style="color : #5a5a5a;">로그아웃</a></li>
						</ul>
						<!-- <ul class="nav navbar-nav">
							<li><a href="#" onclick="checkpassword()" style="color : #5a5a5a;">개인정보수정</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="#" onclick="deletemember()" style="color : #5a5a5a;">회원탈퇴</a></li>
						</ul> -->
						<ul class="nav navbar-nav">
							<li><a href="goplan" style="color : #5a5a5a;">일정</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="goplanCalendar" style="color : #5a5a5a;">일정_달력형</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="goproject" style="color : #5a5a5a;">프로젝트 기능 테스트</a></li>
						</ul>
						<!-- <ul class="nav navbar-nav">
							<li><a href="dashboard" style="color : #5a5a5a;">대시보드</a></li>
						</ul> -->
						<ul class="nav navbar-nav">
							<li><a href="meetingForm" style="color : #5a5a5a;">회의생성/참여</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li></li>
						</ul>
					</div>
				</c:if><%-- 
		
				<!-- 로그인 하지않았을때 -->
				<c:if test="${loginid == null }">
					<div class="navbar-collapse collapse" id="navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="skettologin" style="color : #5a5a5a;">로그인</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="joinform" style="color : #5a5a5a;">Join Sketto</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="goplan" style="color : #5a5a5a;">일정</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="goplanCalendar" style="color : #5a5a5a;">일정_달력형</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="meetingForm" style="color : #5a5a5a;">회의생성/참여</a></li>
						</ul>
						<!-- <ul class="nav navbar-nav">
							<li><a href="speechDemo" style="color : #5a5a5a;">음성인식</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="broadcast" style="color : #5a5a5a;">새로운채팅</a></li>
						</ul> -->
						<ul class="nav navbar-nav">
							<li><a href="goproject" style="color : #5a5a5a;">프로젝트 기능 테스트</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="gosendmail" style="color : #5a5a5a;">초대메일 발송</a></li>
						</ul>
						<ul class="nav navbar-nav">
							<li><a href="gocheck" style="color : #5a5a5a;">초대 후 가입 여부 확인</a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right">
							<li></li>
						</ul>
					</div>
				</c:if>
					 --%>
					
				</div>
			</nav>
		</div>
		
		
	
	
	<div class="imghome">
			<!-- 참여중인 프젝 암것도 없을 때 -->
		<div class="wholediv">	
			<c:if test="${list==null }">
				<div class="pjitems1" >
					<h1>새로운 프로젝트를 생성하고<br>
					함께할 사람들을 부르세요!</h1>
					<a href="goproject"><img src="resources/img/plus1.svg" class="icon"></a>
				</div>
			</c:if>
	
			<!-- 참여중인 프젝 있을 때 -->
			
		 	<c:if test="${list!=null }">
		 		<c:forEach var="pj" items="${list }">
					<div class="pjitems2" id="${pj.pjtitle }">
						<div class="pjitems2inner">
							<c:if test="${pj.id==loginid }">
								<div class="leader">
									<img src="resources/img/crown1.svg" style="width: 60%;">
								</div>
							</c:if>
							<div class="dday">
								<p class="ddaytext">D-</p>
							</div>
							<div class="projecttable">
								<table border="1">
								<tr>
									<td colspan="2">
										<span class="title"><a href="project?pjno=${pj.pjno }">${pj.pjtitle }</a></span>
									</td>
								</tr>
								<tr>
									<td>
										<span class="pjdetail">${pj.startdate } ~ ${pj.enddate }</span>
									</td>
								</tr>
							</table>
							</div>
						</div>
					</div>
				</c:forEach>
				<div class="pjitems2">
					<div class="pjitems2inner">
						<p>새로운 프로젝트를 생성하고<br>
						함께할 사람들을 부르세요!</p>
						<a href="goproject"><img src="resources/img/plus1.svg" class="icon"></a>
					</div>
				</div>	 
			</c:if>
			
		</div>
	</div>
	
	
	
	
	
</body>


	<div class="imghome">
		
		<footer>
	        <p class="pull-right"><a href="#">상위로 올라가기</a></p>
	        <p>&copy; 2018 Sketto, Inc. &middot;</p>
		</footer>
	</div>
	
</html>