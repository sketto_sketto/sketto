<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page errorPage="error" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link
	href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css"
	rel="stylesheet" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<style>

.boxCalendar {
	display: inline-block;
	width: 100%;
	height: 2.9vh;
	font-size: 2.2vh;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.boxDiv {
	overflow: visible;
	display: inline-block;
	width: 100%;
	/* height: 2.9vh;
	font-size: 1.2vh; */
	font-size: 5vh;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.dragClass {
	position: static;
	top: auto;
	left: auto;
	display: inline-block;
	width: 60%;
	height: 100%;
	font-size: 1.2vh;
	border: 0px solid #aaaaaa;
	background-color: coral;
}
</style>

<script src="resources/moment-with-locales.js"></script>
<script type="text/javascript">
	/* 프로젝트 시작일과 마감일 받고 시작할 것 */
	//프로젝트 종료일 추가 / 변경 가능하도록 할 것(1개월 추가, 3개월 추가 등)
	//혹은 시작/종료를 초과하는 일정이 추가되면 시작일과 종료일이 조정되도록.
	//진행도가 하단에 추가되는 것도 고려할 것.
	var todayDate = new Date();	
	var todayDateToString = moment(todayDate).format('YYYY-MM-DD-hh-mm-ss');	
	
	//todayDateXval = calDateRange(startdate, todayDateToString); 아래에 이거 쓰여져 있음
	
	function calDateRange(startdate, enddate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크 startdate
		if (startdate.length != 19) {
			return null;
		}

		// FORMAT을 포함한 길이 체크 enddate 
		if (enddate.length != 19) {
			return null;
		}

		// FORMAT이 있는지 체크
		if (startdate.indexOf(FORMAT) < 0 || enddate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var start_dt = startdate.split(FORMAT);
		var end_dt = enddate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		start_dt[1] = (Number(start_dt[1]) - 1) + "";
		end_dt[1] = (Number(end_dt[1]) - 1) + "";

		var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2],
				start_dt[3], start_dt[4], start_dt[5]);
		var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2], end_dt[3],
				end_dt[4], end_dt[5]);

		if (from_dt > to_dt) {
			return parseInt((from_dt.getTime() - to_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else {
			return parseInt((to_dt.getTime() - from_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		}
	}

	function stringToDate(varDate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크
		if (varDate.length != 19)
			return null;

		// FORMAT이 있는지 체크
		if (varDate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var var_dt = varDate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		var_dt[1] = (Number(var_dt[1]) - 1) + "";

		var return_dt = new Date(var_dt[0], var_dt[1], var_dt[2], var_dt[3],
				var_dt[4], var_dt[5]);

		return return_dt;
	}

	var oldParentID;
	var newParentID;

	function pageLoad() {

		document.getElementById('scheduleDiv').innerHTML = "";

		var varDate = new Date();
		varDate.setHours(00);
		varDate.setMinutes(00);
		
		console.log(varDate);
		//startdate를 stringToDate() 메소드를 사용해서 date 포맷으로 변환한 것.
		//이걸 통해서 요일이나 월 등을 분석할 때 써먹는다. 하루씩 더해가지고 getMonth하고 getDay하고

		var newDiv = '<DIV class="boxDiv"';
		
		newDiv += ' id="scheduleToday" align="left">';

		newDiv += '</div><br>';	

		document.getElementById('scheduleDiv').innerHTML += newDiv;

		/* 여기서부터는 일요일 표시, 기준 점 작성 */
		for (var j = 0; j < 48; j++) {
			var newDiv = '<DIV class="boxDiv" ';
			
			if (varDate.getMinutes() == 0) {
				newDiv += 'style="border-top: 2px ';
			} else {
				newDiv += 'style="border-top: 0px ';				
			}

			if (varDate.getHours() >= 12) {
				newDiv += 'solid #1f2944; color:#1f2944;"';
			} else { //00:00~12:00
				newDiv += 'solid #1f2944; color:#1f2944;"';
			}

			var newDivId = "";
			var newDivInnerHTML = "";
			if (varDate.getHours() < '10') {
				newDivId += '0' + varDate.getHours();			
				newDivInnerHTML += '0' + varDate.getHours();			
			} else {				
				newDivId += varDate.getHours();
				newDivInnerHTML += varDate.getHours();
			}
			
			if (varDate.getMinutes().toString() == '0') {
				newDivId += '00';			
				newDivInnerHTML += ':' + '00';			
			} else {				
				newDivId += varDate.getMinutes();
				newDivInnerHTML += ':' + varDate.getMinutes();
			}
			
			if (
					Number(moment(todayDate).format('HH00')) == Number(moment(varDate).format('HHmm'))
					&& Number(moment(todayDate).format('HHmm')) > Number(moment(varDate).format('HH00'))
					&& Number(moment(todayDate).format('HHmm')) < Number(moment(varDate).format('HH30'))) {
				
				newDiv += ' name="today"';
				newDivInnerHTML += '<input type="hidden" id="todayToScroll">';
			}
			
				
			newDiv += ' id="divPlan' + newDivId;
			
			newDiv += '" >';
			
			//console.log(newDivId);
			
			newDiv += newDivInnerHTML;
			
			newDiv += '</div>';			
			
			//console.log(newDiv);
			
			varDate.setMinutes(varDate.getMinutes() + 30);
			
			document.getElementById('scheduleDiv').innerHTML += newDiv;
			document.getElementById('scheduleDiv').innerHTML += '<br>';
		}

	}

	function scheduleUpdate() {

		//console.log('${planList}');

		//y축 값은 나중에 일정을 출력하면서 select에 order를 매기는 식으로 변경 가능.

		<c:forEach items="${planList}" var="planString">

		console.log('${planString.pstartdate}');

		var pstartdate = '${planString.pstartdate}';
		pstartdate = pstartdate.substring(0, 19);
		pstartdate = pstartdate.replace(" ", "-");
		pstartdate = pstartdate.replace(":", "-");
		pstartdate = pstartdate.replace(":", "-");

		var pstartdateDate = stringToDate(pstartdate);
		
		var penddate = '${planString.penddate}';
		penddate = penddate.substring(0, 19);
		penddate = penddate.replace(" ", "-");
		penddate = penddate.replace(":", "-");
		penddate = penddate.replace(":", "-");

		var penddateDate = stringToDate(penddate);

		todayDateYMD = moment(todayDate).format('YYYYMMDD');
		pstartdateYMD = moment(pstartdateDate).format('YYYYMMDD');
		penddateYMD = moment(penddateDate).format('YYYYMMDD');

		var isThisToday = (todayDateYMD == pstartdateYMD && todayDateYMD == penddateYMD);

		var includeToday = (
				todayDateYMD == pstartdateYMD ||
				todayDateYMD == penddateYMD ||
				(todayDate > pstartdateDate && todayDate < penddateDate));
		
		console.log('today::::::: '+ todayDateYMD);
		console.log('plan Start:: '+ pstartdateYMD);
		console.log('plan Ends::: '+ penddateYMD);
		
		console.log('${planString.planname} is today?? ' + isThisToday);
		console.log('${planString.planname} include today?? ' + includeToday);
		
		var color = '${planString.color}';
		color = color.replace("\"", "");
		color = color.replace("\"", "");
		
		if (includeToday) {
			var newDiv = '<DIV class="boxDiv" ';
			newDiv += 'onclick="openPlan(${planString.pno})" ';
			newDiv += '><font color="'+color+'">■</font> ${planString.planname} : ' + pstartdateYMD + ' ~ ' + penddateYMD;
			newDiv += '</div><br>';
			
			document.getElementById("scheduleToday").innerHTML += newDiv;
		}
		
	if (isThisToday) {
		pstartdateHM = moment(pstartdateDate).format('HHmm');
		penddateHM = moment(penddateDate).format('HHmm');
		
		console.log('::::::: ${planString.planname}'+pstartdateHM+'~'+penddateHM);
		
		var i = pstartdateDate;
		i.setMinutes(0);
		var StartEndcompare = false;
		while (!StartEndcompare) {

			if (i > penddateDate) {
				console.log('true');
				StartEndcompare = true;
			}
			
			console.log('is it working?');
			
			var appendTo = 'divPlan' + moment(i).format('HHmm');
			console.log(appendTo);
			if (document.getElementById(appendTo) != null) {
				var appendToHTML = "";
				appendToHTML += '<DIV class="boxDiv" onclick=';
				appendToHTML += '"javascript: openPlan(${planString.pno})" ';
				appendToHTML += '><font color="'+color+'">■</font> ${planString.planname}' + ' : ' + pstartdateHM + ' ~ ' + penddateHM;
				appendToHTML += '</div>';
				document.getElementById(appendTo).innerHTML += appendToHTML;
			}//end if
			
			i.setMinutes(i.getMinutes()+30);
			//end if
		}//end while
		
	}//end if
	
	</c:forEach>
	
}//end function scheduleUpdate()

/* 	
	function createPlan() {

		var planName = document.getElementById("planNameIns").value;

		var format = 'YYYY/MM/DD, HH:mm';
		var pstartdate = moment($('#pStartDateIns').val(), format, 'ko')
				.format('YYYY-MM-DD-HH-mm-ss');//format
		var penddate = moment($('#pEndDateIns').val(), format, 'ko').format(
				'YYYY-MM-DD-HH-mm-ss');//format

		console.log(pstartdate + '~'+ penddate);
				
		var idArray = [];
		var id = document.getElementsByName("idIns");
		for (var i = 0; i < id.length; i++) {

			if (id[i].checked) {
				idArray.push(id[i].value);
			}
			
		}
		//위에 이거 바꾸면 아래에서 신규 div appending 하는 것도 바꿔야 함

		var color = "#" + document.getElementById("colorIns").value;

		var planInsJSON = {
			"PNO" : 0, //irrelevant
			"planName" : planName,
			"pStartDate" : pstartdate,
			"pEndDate" : penddate,
			"PJNo" : Number('${pjNO}'),
			"id" : idArray,
			"Priority" : 5,
			"Progress" : 0.4,
			"color" : color
		};
		//"PJNo" : $.session.get("pjNO"),
		//VO와 같은 형식으로 일치시켜야 한다.
		//id는 추후 ArrayList로 변경하여, 여러 팀원이 배정 받고 빠져나갈 수 있도록 해야 한다.

		$
				.ajax({
					url : 'insertJ',
					type : 'GET',
					dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
					//data : {'voice': final_transcript},
					data : {
						'jsonString' : JSON.stringify(planInsJSON)
					},
					//data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
					//contentType:"application/json",
					success : function(planList) {

						var planList = JSON.parse(planList);
						
						planList
								.forEach(function(planString, index) {

									console.log('${planString.pstartdate}');

									var pstartdate = '${planString.pstartdate}';
									pstartdate = pstartdate.substring(0, 19);
									pstartdate = pstartdate.replace(" ", "-");
									pstartdate = pstartdate.replace(":", "-");
									pstartdate = pstartdate.replace(":", "-");

									var pstartdateDate = stringToDate(pstartdate);
									
									var penddate = '${planString.penddate}';
									penddate = penddate.substring(0, 19);
									penddate = penddate.replace(" ", "-");
									penddate = penddate.replace(":", "-");
									penddate = penddate.replace(":", "-");

									var penddateDate = stringToDate(penddate);

									todayDateYMD = moment(todayDate).format('YYYYMMDD');
									pstartdateYMD = moment(pstartdateDate).format('YYYYMMDD');
									penddateYMD = moment(penddateDate).format('YYYYMMDD');

									var color = '${planString.color}';
									color = color.replace("\"", "");
									color = color.replace("\"", "");
									
									if (todayDate > pstartdateDate && todayDate < penddateDate) {
										var newDiv = '<DIV class="boxDiv" ';
										newDiv += 'onclick="openPlan('+'${planString.pno}'+')" ';
										newDiv += '>'+'${planString.planname}'+ ' : ' + pstartdateYMD + ' ~ ' + penddateYMD;
										newDiv += '</div><br>';
										
										document.getElementById("scheduleToday").innerHTML += newDiv;
									}
									
									
									if (todayDateYMD == pstartdateYMD && todayDateYMD == pstartdateYMD) {
										pstartdateHM = moment(pstartdateDate).format('HHmm');
										penddateHM = moment(penddateDate).format('HHmm');
										
										console.log('${planString.planname}'+pstartdateHM+'~'+penddateHM);
										
										var i = pstartdateDate;
										var StartEndcompare = false;
										while (!StartEndcompare) {
											console.log('is it working?');
											
											var appendTo = 'divPlan' + moment(i).format('HHmm');
											if (document.getElementById(appendTo) != null) {
												document.getElementById(appendTo).innerHTML += '<DIV class="boxDiv" ';
												document.getElementById(appendTo).innerHTML += 'onclick="openPlan('+'${planString.pno}'+')" ';
												document.getElementById(appendTo).innerHTML += '>'+'${planString.planname}'+ ' : ' + pstartdateYMD + ' ~ ' + penddateYMD;
												document.getElementById(appendTo).innerHTML += '</div><br>';
											}
											
											i.setMinutes(i.getMinutes()+30);
											
											if (i > penddateDate) {
												console.log('true');
												StartEndcompare = true;
											}
										}
										
									}
									
								})
					},
					error : function(error) {
						alert('에러발생\n' + JSON.stringify(error));
					}
				});

	}
 */
	function openPlan(pno) {
		
		var popupWidth = 600;
		var popupHeight = 500;
		var popupX = (window.screen.width / 2) - (popupWidth / 2);
		// 만들 팝업창 좌우 크기의 1/2 만큼 보정값으로 빼주었음

		var popupY= (window.screen.height / 2) - (popupHeight / 2);
		// 만들 팝업창 상하 크기의 1/2 만큼 보정값으로 빼주었음
		
		//pno를 갖고 컨트롤러로 이동한다
		window
				.open("getThePlan?pno=" + pno, "일정내용",
						'width='+popupWidth+'px, height='+popupHeight+'px, toolbar=no, menubar=no, scrollbars=no, resizable=yes left='
						+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	}
	
	function mouseOver(divMouseOver) {

	divMouseOver
				.setAttribute(
						"title",
						(divMouseOver.getAttribute("pno") + " / "
								+ divMouseOver.getAttribute("planName") + " : "
								+ divMouseOver.getAttribute("pstartdate")
								+ " ~ " + divMouseOver.getAttribute("penddate")));
	}

	function mouseOut(divMouseOver) {

	}

	var intervalOn = false;
	var blinkIntervalBoolean = false;
	var blinkInterval = null;
	var getName = null;
	
		function blinking() {
			var blinkingObject = document.getElementsByName('today');

			intervalOn = !intervalOn;

			if (intervalOn) {
				clearInterval(blinkInterval);
				for (var i = 0; i < blinkingObject.length; i++) {
					if (blinkingObject[i].getAttribute('color') != null) {
						$(blinkingObject[i]).css("background-color",
								blinkingObject[i].getAttribute('color'));
					} else {
						$(blinkingObject[i]).css("background-color", "#FFFFFF"); //#ffcb5e
					}
				}
				return;
			}


			blinkInterval = setInterval(
					function() {
						blinkIntervalBoolean = !blinkIntervalBoolean;
						console.log(blinkIntervalBoolean);

						for (var i = 0; i < document.getElementsByName('today').length; i++) {
							if (blinkIntervalBoolean) {
								$(blinkingObject[i]).css("background-color",
										"#ffcb5e"); //#ffcb5e
							} else {
								if (blinkingObject[i].getAttribute('color') != null) {
									$(blinkingObject[i]).css(
											"background-color",
											blinkingObject[i]
													.getAttribute('color'));
								} else {
									$(blinkingObject[i]).css(
											"background-color", "#FFFFFF");//"#FFFFFF"
								}
							}
						}
					}, 500);
		}

</script>

</head>
<body>

	<div id="scheduleDiv"
		style="white-space: nowrap; overflow: auto; margin: 0px; padding: 0px" align="left"></div>

	<script type="text/javascript">
		console.log(Number('${pjNO}'));

		pageLoad();

		scheduleUpdate();
	</script>
</body>
</html>