<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="resources/css/bootstrapu.css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<style>
.boxCalendar {
	display: inline-block;
	width: 22px;
	height: 22px;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.boxDiv {
	position: relative;
	overflow: hidden;
	display: inline-block;
	width: 22px;
	height: 22px;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.dragClass {
	position: static;
	top: auto;
	left: auto;
	display: inline-block;
	width: 20px;
	height: 20px;
	border: 0px solid #aaaaaa;
	background-color: coral;
}
</style>

<script src="resources/moment-with-locales.js"></script>
<script type="text/javascript">
	/* 프로젝트 시작일과 마감일 받고 시작할 것 */
	var startdate = "2018-04-01-00-00-00";
	var enddate = "2018-06-30-00-00-00";
	//프로젝트 종료일 추가 / 변경 가능하도록 할 것(1개월 추가, 3개월 추가 등)
	//혹은 시작/종료를 초과하는 일정이 추가되면 시작일과 종료일이 조정되도록.
	//진행도가 하단에 추가되는 것도 고려할 것.
	var todayDate = new Date();	
	var todayDateToString = moment(todayDate).format('YYYY-MM-DD-hh-mm-ss');	
	
	var todayDateXval = null; //아래의 calDateRange에서 값을 초기화할 계획.
	//todayDateXval = calDateRange(startdate, todayDateToString); 아래에 이거 쓰여져 있음
	
	var scheduleVariety = 10;
	//기본적인 스케쥴러 행 수. 이를 조절하면 11, 12줄... 30줄... 등 열의 증감이 가능하다

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function calDateRange(startdate, enddate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크 startdate
		if (startdate.length != 19) {
			return null;
		}

		// FORMAT을 포함한 길이 체크 enddate 
		if (enddate.length != 19) {
			return null;
		}

		// FORMAT이 있는지 체크
		if (startdate.indexOf(FORMAT) < 0 || enddate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var start_dt = startdate.split(FORMAT);
		var end_dt = enddate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		start_dt[1] = (Number(start_dt[1]) - 1) + "";
		end_dt[1] = (Number(end_dt[1]) - 1) + "";

		var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2],
				start_dt[3], start_dt[4], start_dt[5]);
		var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2], end_dt[3],
				end_dt[4], end_dt[5]);

		if (from_dt > to_dt) {
			return parseInt((from_dt.getTime() - to_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else {
			return parseInt((to_dt.getTime() - from_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		}
	}

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function stringToDate(varDate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크
		if (varDate.length != 19)
			return null;

		// FORMAT이 있는지 체크
		if (varDate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var var_dt = varDate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		var_dt[1] = (Number(var_dt[1]) - 1) + "";

		var return_dt = new Date(var_dt[0], var_dt[1], var_dt[2], var_dt[3],
				var_dt[4], var_dt[5]);

		return return_dt;
	}

	function allowDrop(ev) {
		ev.preventDefault();
	}

	var oldParentID;
	var newParentID;

	function drag(ev) {
		//drag 시의 이벤트
		ev.dataTransfer.setData("dragText", ev.target.id);
		//dragText라는 임의 속성 부여해 그 것에 id 넣는다.
		var divId = ev.dataTransfer.getData("dragText");
		//해당 id를 되받아오는 과정. 이를 통해 정말 dragText에 id가 들어갔는지 확실하게 한다.
		var originalX = document.getElementById(divId).getAttribute('x');
		//divId를 응용해 그 x 값을 가져온다.
		var originalY = document.getElementById(divId).getAttribute('y');
		//divId를 응용해 그 y 값을 가져온다.
		oldParentID = 'dropon' + originalX + originalY;
		//drag한 divId가 속한 빈 상자의 id를 가져온다.

	} //드래그하면 넣을 div에 이름을 부여한다.

	function drop(ev) {
		//드롭 기능은 배치 등 사용자가 실행한 변경 사항이기 때문에,
		//그에 따른 일정의 수정 및 업데이트 기능 역시 병행한다

		if (confirm("해당 일정을 변경하시겠습니까?")) {
			//이후에 이것을 일정 변경에 대한 yes / no 여부로 확인
			ev.preventDefault();

			var divId = ev.dataTransfer.getData("dragText");
			//Drop한 id 가져오기 (drag할 때 "dragText"에 속성 부여했음)
			var originalX = document.getElementById(divId).getAttribute('x');
			var originalY = document.getElementById(divId).getAttribute('y');
			//Drop한  div의 x, y값 가져온다
			var originalI = document.getElementById(divId).getAttribute('i');
			//i값 가져오기 : 같은 일정의 div에서 몇번째인가
			var originalWidthVal = document.getElementById(divId).getAttribute(
					'widthVal');
			//widthVal값 가져오기 : 같은 일정의 div에서 몇번째까지 있는가

			var newX = Number(document.getElementById(ev.target.id)
					.getAttribute('x'));
			var newY = Number(document.getElementById(ev.target.id)
					.getAttribute('y')); //새로  보낼 부모 div의 x,y 가져옴
			//Drop한  div의 x, y값 가져온다
			newParentID = 'dropon' + newX + newY;
			//일정 옮기기 위한 드롭할 부모 div의 이름

			var Xdifference = (Number(originalX) - Number(newX));

			console.log('Number(originalX) (' + Number(originalX)
					+ ') - Number(newX)(' + Number(newX) + ') = Xdifference('
					+ Xdifference + ')');

			var Ydifference = (Number(originalY) - Number(newY));

			console.log('Number(originalY) (' + Number(originalY)
					+ ') - Number(newY)(' + Number(newY) + ') = Ydifference('
					+ Ydifference + ')');

			//id 형식 : 'draggable' + '${planString.pno}' + 'no' + i
			var idForm = document.getElementById(divId).getAttribute('id');
			idForm = idForm.slice(0, idForm.lastIndexOf("o") + 1);
			//해당 일정이 가지고 있는 id 규격(draggable + 일정넘버. 여기에 '작은 div 순서'를 추가할 것임)을 가져온다.

			var pstartdate = document.getElementById(idForm + 0).getAttribute(
					'pstartdate');
			var penddate = document.getElementById(idForm + 0).getAttribute(
					'penddate');
			var xStartDate = Number(document.getElementById(idForm + 0)
					.getAttribute('xStartDate')); //처음 시작되는 x의 값
			var xLengthIns = Number(document.getElementById(idForm + 0)
					.getAttribute('xLengthIns')); //처음 설정되는 x의 길이
			//xStartDate // div들 줄 있던 곳 첫번째
			//xLengthIns // div들 줄 있던 곳 마지막까지의 길이 
			//originalX //예전의 div 위치 //떠난 곳
			//newX //새롭게 이동하는 div의 위치 //가는 곳

			var booleanChangingOnlyEndDate = false;
			//End Date만 변경하겠다. 는 조건 / 사용자의 결정을 묻고자 하는 Boolean

			if (Ydifference == 0
					&& divId == (idForm + Number(originalWidthVal - 1))) {
				// Ydifference가 0일 때 : 같은 줄 안에서 기간의 div만 움직였는데.
				// divId가 div 포맷에서 div 길이의 마지막번째일때 : 마지막 div를 움직였을 때.

				if (Number(newX) > Number(xStartDate)
						&& Number(newX) < Number(xStartDate + xLengthIns)) {
					//Number(newX) > Number(xStartDate) : 
					//새로 이동하는 div의 위치가 원래의 div 중간에 있을 경우
					//StartDate는 변경하지 않는다. EndDate만 변경한다.
					if (confirm('기간 조절이 이뤄집니다 : 기간 축소')) {
						booleanChangingOnlyEndDate = true;
						//싫다고 하면 return한다.
					} else {
						booleanChangingOnlyEndDate = false;
						return;
						//y값은 그대로고 x값은 범위 안에서 변경 되었으니까 EndDate만 변경하자
					}
				}

				else if (Number(newX) > Number(xStartDate + xLengthIns)) {
					//Number(newX) > Number(xStartDate) : 
					//새로 이동하는 div의 위치가 원래의 div보다 더 오래 있을 경우
					//StartDate는 변경하지 않는다. EndDate만 변경한다.
					if (confirm('기간 조절이 이뤄집니다 : 기간 연장')) {
						booleanChangingOnlyEndDate = true;
						//싫다고 하면 return한다.
					} else {
						booleanChangingOnlyEndDate = false;
						return;
						//y값은 그대로고 x값은 범위 안에서 변경 되었으니까 EndDate만 변경하자
					}
				}
			}

			var IntoX; //한꺼번에 여러 div 들을 옮기기 위해 임시로 값을 넣을 intoX를 만든다
			var IntoY; //한꺼번에 여러 div 들을 옮기기 위해 임시로 값을 넣을 intoY를 만든다

			for (var j = 0; j < originalWidthVal; j++) {
				//일정의 길이만큼 for문 돌아가게 한다.

				//document.getElementById(idForm+j)는 해당 일정의 id 배열
				IntoX = Number(document.getElementById(idForm + j)
						.getAttribute('x'))
						- Xdifference;
				//각 'idForm + j'의 x 값에서, x 차이값 빼서 새로 넣을 intoX 설정해주기 
				IntoY = Number(document.getElementById(idForm + j)
						.getAttribute('y'))
						- Ydifference;
				//각 'idForm + j'의 y 값에서, y 차이값 빼서 새로 넣을 intoX 설정해주기 

				if (document.getElementById(divId) == document
						.getElementById(idForm + j)
						|| booleanChangingOnlyEndDate) {
					//원래 가져다놓는 div일 때 : Xdifference / Ydifference를 이미 한번 뺐음
					//또 뺄 필요 없으니까 위의 IntoX, IntoY 계산을 무효화

					//booleanChangingOnlyEndDate가 true면 이동할 필요가 없음
					IntoX = Number(document.getElementById(idForm + j)
							.getAttribute('x'));

					IntoY = Number(document.getElementById(idForm + j)
							.getAttribute('y'));
				}

				document.getElementById(idForm + j).setAttribute('x', IntoX);
				document.getElementById(idForm + j).setAttribute('y', IntoY);
				//일단은 계산했든 무효화했든 새로운 좌표 부여

				newParentID = 'dropon' + IntoX + IntoY;
				//그 x,y를 대입한 새로운 parent에
				if (document.getElementById(newParentID) != null) {
					document.getElementById(newParentID).appendChild(
							document.getElementById(idForm + j));
				}
				//append 한다.

				// if 안에 매겨서 드래그한 해당 div인지 아닌지 구분한다
				//해당 div일 경우에는 옮긴만큼 옮기면 되고,
				//해당 div가 아니면 옮긴 div의 변화 값만큼 다른 div의 x, y에 대입하여 옮긴다.

				var dateChangeIns = Xdifference;
				//Xdifference는 originalX에서 newX를 뺀 값
				//날짜값의 변경 정도 (현재는 일 단위이기 때문에)
				var startdateToDate = stringToDate(pstartdate);
				//원래의 pstartdate를 Date형식의 변수 startdateToDate로 전환
				var enddateToDate = stringToDate(penddate);
				//원래의 penddate를 Date형식의 변수 enddateToDate로 전환

				if (booleanChangingOnlyEndDate) {
					startdateToDate.setDate(stringToDate(pstartdate).getDate());
					//EndDate만 변경하겠다. 의 Boolean
				} else {
					startdateToDate.setDate(startdateToDate.getDate()
							- dateChangeIns);
					//varDate에서 변경된만큼(varDate.getDate() - dateChange)의 날짜 변경 적용
				}
				enddateToDate.setDate(enddateToDate.getDate() - dateChangeIns);

				if (startdateToDate > enddateToDate) {
					concole.log('startdateToDate(' + startdateToDate
							+ ') > enddateToDate(' + enddateToDate + ')');

					var temp = startdateToDate;
					startdateToDate = enddateToDate;
					enddateToDate = temp;
				}

				var startdateEdittedString = moment(startdateToDate).format(
						'YYYY-MM-DD-hh-mm-ss');
				//해당 페이지에서 조절할 수 있는 형식으로 전환

				document.getElementById(idForm + j).setAttribute('pstartdate',
						startdateEdittedString);

				//varDate에서 변경된만큼(varDate.getDate() - dateChange)의 날짜 변경 적용
				var enddateEdittedString = moment(enddateToDate).format(
						'YYYY-MM-DD-hh-mm-ss');//해당 페이지에서 조절할 수 있는 형식으로 전환

				document.getElementById(idForm + j).setAttribute('penddate',
						enddateEdittedString);

			}//for문 끝나는 곳

			//update ajax 시작

			var insPNO = document.getElementById(idForm + 0).getAttribute('pno');
			insPNO = insPNO.replace("\"", "");
			insPNO = insPNO.replace("\"", "");

			var planNameIns = document.getElementById(idForm + 0).getAttribute(
					'planName').toString().replace("\"", "");
			planNameIns = planNameIns.replace("\"", "");
			//양 옆의 큰따옴표 제거

			var idListIns = JSON.parse(document.getElementById(idForm + 0)
					.getAttribute('memberIdIns'));

			//idList

			console.log("insPNO ::::::::::::::" + insPNO);
			console.log("planName ::::::::::::::"
					+ document.getElementById(idForm + 0).getAttribute(
							'planName'));
			console.log("planName ::::::::::::::" + planNameIns);
			console.log("memberIdIns ::::::::::::::"
					+ document.getElementById(idForm + 0).getAttribute(
							'memberIdIns'));
			console.log("memberIdIns ::::::::::::::" + idListIns);

			var planInsJSON = {
				"PNO" : insPNO,
				"planName" : planNameIns,
				"pStartDate" : document.getElementById(idForm + 0)
						.getAttribute('pstartdate'),
				"pEndDate" : document.getElementById(idForm + 0).getAttribute(
						'penddate'),
				"PJNo" : document.getElementById(idForm + 0).getAttribute(
						'PJNo'),
				"id" : idListIns,
				"Priority" : document.getElementById(idForm + 0).getAttribute(
						'Priority'),
				"Progress" : document.getElementById(idForm + 0).getAttribute(
						'Progress'),
				"color" : document.getElementById(idForm + 0).getAttribute(
						'color')
			};
			//VO와 같은 형식으로 일치시켜야 한다.
			//id는 추후 ArrayList로 변경하여, 여러 팀원이 배정 받고 빠져나갈 수 있도록 해야 한다.

			pageLoad();

			$
					.ajax({
						url : 'updateJ',
						type : 'GET',
						dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
						//data : {'voice': final_transcript},
						data : {
							'jsonString' : JSON.stringify(planInsJSON)
						},
						//data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
						//contentType:"application/json",
						success : function(planList) {

							var planList = JSON.parse(planList);
							var yIns = 0;
							planList
									.forEach(function(item, index) {

										var pstartdateIns = item.pstartdate;
										pstartdateIns = pstartdateIns
												.substring(0, 19);
										pstartdateIns = pstartdateIns.replace(
												" ", "-");
										pstartdateIns = pstartdateIns.replace(
												":", "-");
										pstartdateIns = pstartdateIns.replace(
												":", "-");

										var penddateIns = item.penddate;
										penddateIns = penddateIns.substring(0,
												19);
										penddateIns = penddateIns.replace(" ",
												"-");
										penddateIns = penddateIns.replace(":",
												"-");
										penddateIns = penddateIns.replace(":",
												"-");

										console.log('item.pno = ' + item.pno
												+ ' / pstartdate = '
												+ pstartdate);
										console.log('color (' + item.color
												+ ')');

										var colorIns = item.color;
										colorIns = colorIns.replace("\"", "");
										colorIns = colorIns.replace("\"", "");

										var xStartDateIns = calDateRange(
												startdate, pstartdateIns);
										//x값이 시작되는 위치
										var xLengthIns = calDateRange(
												pstartdateIns, penddateIns);
										//해당 일정 마지막 날부터 일정 시작일을 뺀 값만큼이 x축 값.

										var widthVal = (xLengthIns + 1);

										for (var i = 0; i < widthVal; i++) { //길이만큼의 div를 하나씩 배정할 예정
											if (document
													.getElementById('draggable'
															+ item.pno + 'no'
															+ i) != null) {
												$(
														'#draggable' + item.pno
																+ 'no' + i)
														.remove();
											}
											var newDiv = '<div '; // DIV 객체 생성
											var xIns = xStartDateIns + i;
											// x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
											newDiv += 'x="' + xIns; // x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
											newDiv += '" y="' + yIns; // y값 지정
											newDiv += '" i="' + i; // div의 순서. i값 지정
											newDiv += '" pno="' + item.pno;
											newDiv += '" pstartdate="'
													+ pstartdateIns;
											newDiv += '" penddate="'
													+ penddateIns;
											newDiv += '" planName="'
													+ item.planname;
											newDiv += '" PJNo="' + item.pjno;
											newDiv += '" memberIdIns='
													+ item.id;
											newDiv += ' Priority="'
													+ item.priority;
											newDiv += '" Progress="'
													+ item.progress;
											newDiv += '" color="' + colorIns;
											newDiv += '" xStartDate="'
													+ xStartDateIns;
											
											if (xIns == todayDateXval) {
												newDiv += '" name="today';				
											}
											
											newDiv += '" xLengthIns="'
													+ xLengthIns;
											var onclickFunctionName = 'openPlan('
													+ item.pno + ')';
											newDiv += '" onclick ="'
													+ onclickFunctionName;

											newDiv += '" widthVal="' + widthVal; // x값 지정
											newDiv += '" id="draggable'
													+ item.pno + 'no' + i; // x값 지정
											newDiv += '" style="width: 22px; height: 20px; border: 0px solid #aaaaaa; background-color: '
													+ colorIns;
											newDiv += ';" draggable="true"';
											newDiv += ' ondragstart="drag(event)"';
											newDiv += ' onmouseover="mouseOver(this)" onmouseout="mouseOut(this)">';
											//newDiv += i; //i값 들어가게 해서 순번 확인하려고
											newDiv += '</div>';

											var parentID = 'dropon' + xIns
													+ yIns;

											console.log(newDiv);
											document.getElementById(parentID).innerHTML = newDiv;

										}

										yIns++;

									})
						},
						error : function(error) {
							alert('에러발생\n' + JSON.stringify(error));
						}
					});

			//update ajax 끝
		}//function 끝나는 부분

	}

	function pageLoad() {

		console.log(moment(todayDate).format('YYYY-MM-DD'));
		//원하는 포맷으로 날짜 가져오기 샘플
		
		document.getElementById('scheduleDiv').innerHTML = "";

		var varDate = stringToDate(startdate);
		console.log(varDate);
		//startdate를 stringToDate() 메소드를 사용해서 date 포맷으로 변환한 것.
		//이걸 통해서 요일이나 월 등을 분석할 때 써먹는다. 하루씩 더해가지고 getMonth하고 getDay하고

		var schedulePeriod = calDateRange(startdate, enddate);

		/* 여기서부터는 일요일 표시, 기준 점 작성 */
		for (var j = 0; j < schedulePeriod; j++) {
			var newDiv = document.createElement('DIV'); // DIV 객체 생성

			newDiv.setAttribute('class', 'boxDiv');
			newDiv.innerHTML += varDate.getDate();

			if (varDate.getDay() == 0) {
				newDiv.setAttribute('style', 'background-color:red');
			}

			if (varDate.getDate() == 1) {
				newDiv.setAttribute('style', 'background-color:blue');
			}

			if (moment(varDate).format('YYYYMMDD') ==
				moment(todayDate).format('YYYYMMDD')) {
				newDiv.setAttribute('name', 'today');
			}
			
			varDate.setDate(varDate.getDate() + 1);

			//1일이 될 때마다 색을 부여한다.
			//일주일이 될 때마다 색을 부여한다.

			document.getElementById('scheduleDiv').appendChild(newDiv);
		}

		document.getElementById('scheduleDiv').innerHTML += '<br>';
		//엔터 친다.

		for (var i = 0; i < scheduleVariety; i++) {
			var varDateEach = stringToDate(startdate);
			//각각의 y 좌표 (일정 구분)에 따른 x 값 기준(날짜)이 될 것
			for (var j = 0; j < schedulePeriod; j++) {
				var newDiv = document.createElement('DIV'); // DIV 객체 생성

				newDiv.setAttribute('x', j); // x값 지정
				newDiv.setAttribute('y', i); // y값 지정
				newDiv.setAttribute('id', 'dropon' + j + i); // x값 지정
				newDiv.setAttribute('class', 'boxDiv');
				newDiv.setAttribute('ondrop', 'drop(event)');
				newDiv.setAttribute('ondragover', 'allowDrop(event)');

				document.getElementById('scheduleDiv').appendChild(newDiv);

			}
			document.getElementById('scheduleDiv').innerHTML += '<br>';
		}

	}

	todayDateXval = calDateRange(startdate, todayDateToString);
	
	function scheduleUpdate() {

		var yIns = 0;

		console.log('${planList}');

		//y축 값은 나중에 일정을 출력하면서 select에 order를 매기는 식으로 변경 가능.

		<c:forEach items="${planList}" var="planString">

		console.log('${planString.pstartdate}');

		var pstartdate = '${planString.pstartdate}';
		pstartdate = pstartdate.substring(0, 19);
		pstartdate = pstartdate.replace(" ", "-");
		pstartdate = pstartdate.replace(":", "-");
		pstartdate = pstartdate.replace(":", "-");

		var penddate = '${planString.penddate}';
		penddate = penddate.substring(0, 19);
		penddate = penddate.replace(" ", "-");
		penddate = penddate.replace(":", "-");
		penddate = penddate.replace(":", "-");

		console.log(pstartdate);
		console.log('${planString.color}');

		var color = '${planString.color}';
		color = color.replace("\"", "");
		color = color.replace("\"", "");

		var xStartDate = calDateRange(startdate, pstartdate);
		//x값이 시작되는 위치
		var xLengthIns = calDateRange(pstartdate, penddate);
		//프로젝트 시작일부터 해당 일정 시작일을 뺀 값만큼이 x축 값.

		var widthVal = (xLengthIns + 1);

		for (var i = 0; i < xLengthIns + 1; i++) { //길이만큼의 div를 하나씩 배정할 예정
			if (document.getElementById('draggable' + '${planString.pno}'
					+ 'no' + i) != null) {
				$('#draggable' + '${planString.pno}' + 'no' + i).remove();
				console.log('::::::::::::::::::::::::제거')
			}
			var newDiv = document.createElement('DIV'); // DIV 객체 생성
			var xIns = xStartDate + i;
			// x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
			newDiv.setAttribute('x', xIns); // x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
			newDiv.setAttribute('y', yIns); // y값 지정
			newDiv.setAttribute('i', i); // div의 순서. i값 지정
			newDiv.setAttribute('pno', '${planString.pno}');
			newDiv.setAttribute('pstartdate', pstartdate);
			newDiv.setAttribute('penddate', penddate);
		
			if (xIns == todayDateXval) {
				//해당 x 값이 today의 x값과 같다면
				newDiv.setAttribute('name','today');				
			}
			
			newDiv.setAttribute('planName', '${planString.planname}');
			newDiv.setAttribute('PJNo', '${planString.pjno}');
			newDiv.setAttribute('memberIdIns', '${planString.id}');
			newDiv.setAttribute('Priority', '${planString.priority}');
			newDiv.setAttribute('Progress', '${planString.progress}');
			newDiv.setAttribute('color', color);
			newDiv.setAttribute('xStartDate', xStartDate);
			newDiv.setAttribute('xLengthIns', xLengthIns);
			newDiv.setAttribute('onclick', "openPlan('${planString.pno}')");
			//newDiv.innerHTML += i; //i값 들어가게 해서 순번 확인하려고

			newDiv.setAttribute('widthVal', widthVal); // x값 지정
			newDiv.setAttribute('id', 'draggable' + '${planString.pno}' + 'no'
					+ i); // x값 지정
			newDiv.setAttribute('style', 'width: 22px;' + 'height: 20px;'
					+ 'border: 0px solid #aaaaaa;' + 'background-color: '
					+ color);
			newDiv.setAttribute('draggable', 'true');
			newDiv.setAttribute('ondragstart', 'drag(event)');
			newDiv.setAttribute('onmouseover',"mouseOver(this)");
			newDiv.setAttribute('onmouseout',"mouseOut(this)");

			var parentID = 'dropon' + xIns + yIns;

			console.log(':::::::::::::::::::::appending');

			if (document.getElementById(parentID) != null) {
				document.getElementById(parentID).appendChild(newDiv);
			}

		}

		yIns++;

		</c:forEach>
	}

	
	function createPlan() {
		var planName = document.getElementById("planNameIns").value;

		var format = 'MM/DD/YYYY, hh:mm a';
		var pstartdate = moment($('#pStartDateIns').val(), format, 'ko')
				.format('YYYY-MM-DD-hh-mm-ss');//format
		var penddate = moment($('#pEndDateIns').val(), format, 'ko').format(
				'YYYY-MM-DD-hh-mm-ss');//format

		var idArray = [];
		var id = document.getElementsByName("idIns");
		for (var i = 0; i < id.length; i++) {

			if (id[i].checked) {
				idArray.push(id[i].value);
			}
		}
		//위에 이거 바꾸면 아래에서 신규 div appending 하는 것도 바꿔야 함

		var color = "#" + document.getElementById("colorIns").value;

		var planInsJSON = {
			"PNO" : 0, //irrelevant
			"planName" : planName,
			"pStartDate" : pstartdate,
			"pEndDate" : penddate,
			"PJNo" : Number('${pjNO}'),
			"id" : idArray,
			"Priority" : 5,
			"Progress" : 0.4,
			"color" : color
		};
		//"PJNo" : $.session.get("pjNO"),
		//VO와 같은 형식으로 일치시켜야 한다.
		//id는 추후 ArrayList로 변경하여, 여러 팀원이 배정 받고 빠져나갈 수 있도록 해야 한다.

		$
				.ajax({
					url : 'insertJ',
					type : 'GET',
					dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
					//data : {'voice': final_transcript},
					data : {
						'jsonString' : JSON.stringify(planInsJSON)
					},
					//data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
					//contentType:"application/json",
					success : function(planList) {

						var planList = JSON.parse(planList);
						var yIns = 0;
						planList
								.forEach(function(item, index) {

									var pstartdateIns = item.pstartdate;
									pstartdateIns = pstartdateIns.substring(0,
											19);
									pstartdateIns = pstartdateIns.replace(" ",
											"-");
									pstartdateIns = pstartdateIns.replace(":",
											"-");
									pstartdateIns = pstartdateIns.replace(":",
											"-");

									var penddateIns = item.penddate;
									penddateIns = penddateIns.substring(0, 19);
									penddateIns = penddateIns.replace(" ", "-");
									penddateIns = penddateIns.replace(":", "-");
									penddateIns = penddateIns.replace(":", "-");

									console.log(pstartdate);
									console.log(item.color);

									var colorIns = item.color;
									colorIns = colorIns.replace("\"", "");
									colorIns = colorIns.replace("\"", "");

									var xStartDateIns = calDateRange(startdate,
											pstartdateIns);
									//x값이 시작되는 위치
									var xLengthIns = calDateRange(
											pstartdateIns, penddateIns);
									//해당 일정 마지막 날부터 일정 시작일을 뺀 값만큼이 x축 값.

									var widthVal = (xLengthIns + 1);

									for (var i = 0; i < widthVal; i++) { //길이만큼의 div를 하나씩 배정할 예정
										if (document.getElementById('draggable'
												+ item.pno + 'no' + i) != null) {
											$(
													'#draggable' + item.pno
															+ 'no' + i)
													.remove();
										}

										var newDiv = '<div '; // DIV 객체 생성
										var xIns = xStartDateIns + i;
										// x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
										newDiv += 'x="' + xIns; // x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
										newDiv += '" y="' + yIns; // y값 지정
										newDiv += '" i="' + i; // div의 순서. i값 지정
										newDiv += '" pno="' + item.pno;
										newDiv += '" pstartdate="'
												+ pstartdateIns;
										newDiv += '" penddate="' + penddateIns;
										
										if (xIns == todayDateXval) {
											newDiv += '" name="today';				
										}
										
										newDiv += '" planName='
												+ item.planname;
										newDiv += ' PJNo="' + item.pjno;
										newDiv += '" memberIdIns=' + item.id;
										newDiv += ' Priority="' + item.priority;
										newDiv += '" Progress="'
												+ item.progress;
										newDiv += '" color="' + colorIns;
										newDiv += '" xStartDate="'
												+ xStartDateIns;
										newDiv += '" xLengthIns="' + xLengthIns;
										var onclickFunctionName = 'openPlan('
												+ item.pno + ')';
										newDiv += '" onclick ="'
												+ onclickFunctionName;

										newDiv += '" widthVal="' + widthVal; // x값 지정
										newDiv += '" id="draggable' + item.pno
												+ 'no' + i; // x값 지정
										newDiv += '" style="width: 22px; height: 20px; border: 0px solid #aaaaaa; background-color: '
												+ colorIns;
										newDiv += ';" draggable="true"';
										newDiv += ' ondragstart="drag(event)"';
										newDiv += ' onmouseover="mouseOver(this)" onmouseout="mouseOut(this)">';
										//newDiv += i; //i값 들어가게 해서 순번 확인하려고
										newDiv += '</div>';

										var parentID = 'dropon' + xIns + yIns;

										if (document.getElementById(parentID) != null) {
											document.getElementById(parentID).innerHTML = newDiv;
										}

									}

									yIns++;

								})
					},
					error : function(error) {
						alert('에러발생\n' + JSON.stringify(error));
					}
				});

	}

	function openPlan(pno) {
		//pno를 갖고 컨트롤러로 이동한다
		window
				.open("getThePlan?pno=" + pno, "일정내용",
						"width=800, height=700, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
	}
	
	function mouseOver(divMouseOver) {

	divMouseOver
				.setAttribute(
						"title",
						(divMouseOver.getAttribute("pno") + " / "
								+ divMouseOver.getAttribute("planName") + " : "
								+ divMouseOver.getAttribute("pstartdate")
								+ " ~ " + divMouseOver.getAttribute("penddate")));
	}

	function mouseOut(divMouseOver) {

	}
</script>

</head>
<body>

	<!-- <div id="scheduleDiv" ondrop="drop(event)" ondragover="allowDrop(event)"></div>
 -->
	<div id="scheduleDiv"
		style="white-space: nowrap; overflow: auto; margin: 0px; padding: 0px"></div>

	<script type="text/javascript">
		console.log(Number('${pjNO}'));

		pageLoad();

		scheduleUpdate();
	</script>

	<!-- <div style="position: relative">
		<form action='javascript: addPlanGraph()'>
			<p>
				일정 이름<input id="planNameIns" type="text" />
			</p>
			<p>
				시작 날짜<input id="pStartDateIns" type="text" />
			</p>
			<p>
				마감 날짜<input id="pEndDateIns" type="text" />
			</p>

			<script type="text/javascript" src="resources/bootstrap.min.js"></script>
			<script src="resources/moment-with-locales.js"></script>
			<script src="resources/bootstrap-datetimepicker.js"></script>
			<script>
				$('#pStartDateIns').datetimepicker();
				$('#pEndDateIns').datetimepicker();
			</script>

			<p>
				담당 팀원 <input name="idIns" type="checkbox" value="joId" />조승희 <input
					name="idIns" type="checkbox" value="KimYoungId" />김영성 <input
					name="idIns" type="checkbox" value="KimGyeId" />김계환 <input
					name="idIns" type="checkbox" value="ParkId" />박소정 <input
					name="idIns" type="checkbox" value="ChoiId" />최주영
			</p>
			해당 팀원은 ArrayList로 변경 예정
			<script src="resources/jscolor.js"></script>
			<p>
				색깔<input class="jscolor" id="colorIns" />
			</p>
			priority 추가함
			<div>
				<select id="priority">
					<option value='' selected>-- priority --</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
				</select>
			</div>
			<input type="submit">
		</form> -->

		<script type="text/javascript">
			function addPlanGraph() {

				createPlan();

				scheduleUpdate();

			}
		</script>
	</div>

	<input type="button" value="today" onclick="javascript: blinking(this)">
	<!-- value에 원하는 div name의 값을 넣어 Blink 기능을 실행 -->

	<script type="text/javascript">
	
	var intervalOn = false;
	var blinkIntervalBoolean = false;
	var blinkInterval = null;
	var getName = null;
	
		function blinking(thisObject) {
			getName = thisObject.getAttribute("value");
			var blinkingObject = document.getElementsByName(getName);

			intervalOn = !intervalOn;

			if (intervalOn) {
				clearInterval(blinkInterval);
				for (var i = 0; i < blinkingObject.length; i++) {
					if (blinkingObject[i].getAttribute('color') != null) {
						$(blinkingObject[i]).css("background-color",
								blinkingObject[i].getAttribute('color'));
					} else {
						$(blinkingObject[i]).css("background-color", "#FFFFFF");
					}
				}
				return;
			}


			blinkInterval = setInterval(
					function() {
						blinkIntervalBoolean = !blinkIntervalBoolean;
						console.log(blinkIntervalBoolean);

						console.log(getName);

						for (var i = 0; i < document.getElementsByName(getName).length; i++) {
							if (blinkIntervalBoolean) {
								$(blinkingObject[i]).css("background-color",
										"#FFFFFF");
							} else {
								if (blinkingObject[i].getAttribute('color') != null) {
									$(blinkingObject[i]).css(
											"background-color",
											blinkingObject[i]
													.getAttribute('color'));
								} else {
									$(blinkingObject[i]).css(
											"background-color", "#FFFFFF");
								}
							}
						}
					}, 500);
		}
	</script>
<br>
<br>

<!-- 부트스트랩 -->
<section class='bb-slice' id='faq'>
  <div class='bb-container bb-container-variant-small'>
    <h1 class='bb-section-title'>Add Plan</h1>

<div class='bb-accordion' style="width: 50%;">
      <span class='bb-accordion-title' >일정추가</span>
      <div class='bb-accordion-content' >
      <br>
      <br>
    <!-- 일정을 넣자! -->
      	<div style="position: relative">
		<form action='javascript: addPlanGraph()'>
			<p>
				일정 이름 <input id="planNameIns" type="text" />
			</p>
			<p>
				시작 날짜 <input id="pStartDateIns" type="text" />
			</p>
			<p>
				마감 날짜 <input id="pEndDateIns" type="text" />
			</p>

			<script type="text/javascript" src="resources/bootstrap.min.js"></script>
			<script src="resources/moment-with-locales.js"></script>
			<script src="resources/bootstrap-datetimepicker.js"></script>
			<script>
				$('#pStartDateIns').datetimepicker();
				$('#pEndDateIns').datetimepicker();
			</script>

			<p>
				담당 팀원 <input name="idIns" type="checkbox" value="joId" />조승희 <input
					name="idIns" type="checkbox" value="KimYoungId" />김영성 <input
					name="idIns" type="checkbox" value="KimGyeId" />김계환 <input
					name="idIns" type="checkbox" value="ParkId" />박소정 <input
					name="idIns" type="checkbox" value="ChoiId" />최주영
			</p>
			<!-- 해당 팀원은 ArrayList로 변경 예정 -->
			<script src="resources/jscolor.js"></script>
			<p>
				색깔 <input class="jscolor" id="colorIns" />
			</p>
			<!-- priority 추가함 -->
			<div>
				<select id="priority">
					<option value='' selected>-- priority --</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
				</select>
			</div>
			<br>
			<br>
			<input type="submit">
			
		</form>
      </div>
</div>
</div>
</div>
</section>

</body>

  <script src="resources/css/bb-043d654a.js"></script>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68685304-3', 'auto');
    ga('send', 'pageview');
  </script>


</html>