<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isErrorPage="true" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error! Please Try again - Sketto!</title>
<style type="text/css">
		/* 상단 바  */
	/* Add a black background color to the top navigation */
	.topnav {
	    background-color: #1f2944;
	    overflow: hidden;
	}
	
	/* Style the links inside the navigation bar */
	.topnav a {
	    float: left;
	    color: #fff;
	    text-align: center;
	    padding: 14px 16px;
	    text-decoration: none;
	    font-size: 17px;
    	margin-top: 15px;
	}
	
	/* Change the color of links on hover */
	.topnav a:hover {
	    background-color: #ddd;
	    color: black;
	}
	
	/* Add a color to the active/current link */
	.topnav a.active {
	    background-color: #5c948d;
	    color: white;
	}
	/* 상단 바 끝 */
		
		
	/*페이지 제목*/	

	h1 {
		color: #1f2944;
	    font-family: 'Droid Serif';
	    border-bottom: 3px solid #d95b5b;
	    border-top: 3px solid #d95b5b;
		font-size: 42px;
		font-weight: 400;
		line-height: 60px;
		margin: 0 auto 45px;
		max-width: 600px;
		padding: 45px 0;
		text-align: center;
		margin-top: 1%;
	}
			
	/*페이지 제목 끝*/

	.errorimg {
		width: 300px;
		margin: 0 auto;
	}
</style>
</head>
<body style="margin:0">


		<div id="topnav">
			<div class="topnav">
				<div class="topnavdiv">
					<a href="./"><img src="resources/img/logo2-white.png" style="height: 50px;margin-top: -20px;"></a>
					<a href="logout" >로그아웃</a>
					<a href="goplan">일정</a>
					<!-- <a href="goplanCalendar">일정_달력형</a>
					<a href="myScheduleToday">일정_개인형</a> 
					<a href="goproject">프로젝트 기능 테스트</a>
					<a href="meetingForm">회의생성/참여</a> -->
				</div>
			</div>
			
		</div>
		
		
		<h1 style="text-align: center;">에러입니다.</h1>
		
		<div class="errorimg">
			<img src="./resources/img/error1.png" style="width: 100%;">
		</div>

		<h2 style="text-align: center;">잠시후 다시 시도해주세요</h2>
		<h2 style="text-align: center;">계속해서 같은 오류 발생시 문의 부탁드립니다.</h2>



</body>
</html>