<%@ page language="java" contentType="text/html; charset=UTF-8" 
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> --%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>sketto! 회의</title>
<link href="resources/css/bootstrapu.css" rel="stylesheet" />
<link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Ubuntu:300italic,400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<style>
  * {
    font-family: Verdana, Arial, sans-serif;
    text-align: center;
    margin: 0;
    padding: 0;
  }
  body {
  	
  }
  a:link {
    color:#000;
    text-decoration: none;
  }
  a:visited {
    color:#000;
  }
  a:hover {
    color:#33F;
  }
  .button {
    background: -webkit-linear-gradient(top,#008dfd 0,#0370ea 100%);
    border: 1px solid #076bd2;
    border-radius: 3px;
    color: #fff;
    display: none;
    font-size: 13px;
    font-weight: bold;
    line-height: 1.3;
    padding: 8px 25px;
    text-align: center;
    text-shadow: 1px 1px 1px #076bd2;
    letter-spacing: normal;
  }
  .center {
    padding: 2%;
    text-align: center;
  }
  .final {
    color: #ffcb5e;
    padding-right: 3px; 
  }
  .interim {
    color: #ffcb5e;
  }
  .info {
    font-size: 14px;
    text-align: center;
    color: #777;
    display: none;
  }
  .right {
    float: right;
    width: 7%;
  }
  .sidebyside {
    display: inline-block;
    width: 45%;
    min-height: 40px;
    text-align: left;
    vertical-align: top;
  }
  #headline {
  	padding: 10px;
    width: 100%;
    
  }
  #info {
    font-size: 20px;
    text-align: center;
    background-color: #d95b5b;
    color: #fff;
    visibility: hidden;
  }
  #results {
  	margin-bottom: 1%;
    font-size: 30px;
    font-weight: bold;
    border: 1px solid #ddd;
    background-color: #1f2944;
    color:  #ffcb5e;
    padding: 4%;
    text-align: left;
    min-height: 50px;
    width: 100%;
  }
  #start_button {
    border: 0;
    background-color:transparent;
    padding: 0;
  }
  #leftwin {
	width: 30%;
	heght: 100%;
	text-align: left;
  }
  #rightwin {
  	width: 70%;
	heght: 100%;
  
  }
  #messageWindow {
  	background-color: #ffcb5e;
  	color: #1f2944;
  	font-size: x-large;
  	width: 100%;
  
  }
  input {
  	font-size: large;
  	text-align: left;
  	height: 30px;
  }
  
  
/* 상단 바  */
	/* Add a black background color to the top navigation  */
	.topnav {
	    background-color: #1f2944;
	    overflow: hidden;
	}
	
	/* Style the links inside the navigation bar */
	.topnav a {
	    float: left;
	    color: #fff;
	    text-align: center;
	    padding: 14px 16px;
	    text-decoration: none;
	    font-size: 17px;
    	margin-top: 15px;
	}
	
	/* Change the color of links on hover */
	.topnav a:hover {
	    background-color: #ddd;
	    color: black;
	}
	
	/* Add a color to the active/current link */
	.topnav a.active {
	    background-color: #5c948d;
	    color: white;
	}
	/* 상단 바 끝 */
	
	.card text-center{
	    width: 18rem;
   		FLOAT: LEFT;
	}
	#inputMessage {
		width: 87%;
		height: 100%;
		border: solid thick;
	}
	#uploadForm {
		width: 100%;
	}
	#file {
		width: 60%;
	}
	#submitFile {
		width: 20%;
	}
	
	
	
	/* 3단 나누기  */
	#wrap {
		width: 100%;
	    margin: 0 auto;
	    text-align: center;
	}
	#all {
    	padding: 0 0;
    	width: 100%;
    	text-align: center;
	}
	
	#menu {
		float: left;
	    
	    width: 20%;
	    height: 100%;
	    padding: 0 0 0 0;
	}
	#gaunde {
		float: left;
	    
	    width: 56%;
	    height: 100%;
	    padding: 0 0 0 0;
	}
	#aside {
		float: right;
	   
	    width: 20%;
	    height: 100%;
	    padding: 0 0 0 0;
	    margin-left: 2%;
	    margin-right: 2%;
	}
	.menu {
		height: 40px;
	}
	.menu_content {
		text-align: center;
	}
	select {
		/* margin: 0;
		padding: 0;
		width: 20%;
		height: 40px;
		font-size: x-large; */
		width: 200px;
	    height: 30px;
	    padding-left: 10px;
	    font-size: 18px;
	    color: #1f2944;
	    border: 1px solid #1f2944;
	    border-radius: 3px;
		
	}
	#library {
	  display: inline-block;
	  padding: .5em .75em;
	  color: #fff;
	  font-size: inherit;
	  line-height: normal;
	  vertical-align: middle;
	  background-color: #fdfdfd;
	  cursor: pointer;
	  border: 1px solid #ebebeb;
	  border-bottom-color: #e2e2e2;
	  border-radius: .25em;
	}
	
	/* 새로운 파일창 */
	.filebox input[type="file"] {
	  position: absolute;
	  width: 1px;
	  height: 1px;
	  padding: 0;
	  margin: -1px;
	  overflow: hidden;
	  clip: rect(0, 0, 0, 0);
	  border: 0;
	}
	.filebox label {
	  display: inline-block;
	  padding: .5em .75em;
	  color: #fff;
	  font-size: inherit;
	  line-height: normal;
	  vertical-align: middle;
	  background-color: #fdfdfd;
	  cursor: pointer;
	  border: 1px solid #ebebeb;
	  border-bottom-color: #e2e2e2;
	  border-radius: .25em;
	}
	
	/* named upload */
	.filebox .upload-name {
	  display: inline-block;
	  padding: .5em .75em; /* label의 패딩값과 일치 */
	  font-size: inherit;
	  font-family: inherit;
	  line-height: normal;
	  vertical-align: middle;
	  background-color: #f5f5f5;
	  border: 1px solid #ebebeb;
	  border-bottom-color: #e2e2e2;
	  border-radius: .25em;
	  -webkit-appearance: none; /* 네이티브 외형 감추기 */
	  -moz-appearance: none;
	  appearance: none;
	}
	
	h1 {
	
      color: #1f2944;
      font-family: 'Droid Serif';
      border-bottom: 3px solid #d95b5b;
      border-top: 3px solid #d95b5b;
      font-size: 42px;
      font-weight: 400;
      line-height: 60px;
      margin: 0 auto 45px;
      max-width: 600px;
      padding: 45px 0;
      text-align: center;
      
   }
	.headline {
		text-align: center;
	}	
</style>
</head>

<body>
	<!-- 회원 로그인 시 상단 바-->
	<c:if test="${loginid!=null }">
		<div id="topnav">
			<div class="topnav">
				<div class="topnavdiv">
					<a href="./"><img src="resources/img/logo2-white.png" style="height: 50px; margin-top: -20px;"></a>
					<a href="logout" >로그아웃</a>
					<!-- <a href="goplan">일정</a> -->
					<!-- <a href="goplanCalendar">일정_달력형</a>
					<a href="myScheduleToday">일정_개인형</a> -->
					<!-- <a href="goproject">프로젝트 기능 테스트</a> -->
					<!-- <a href="meetingForm">회의생성/참여</a> -->
				</div>
			</div>
			
		</div>
	</c:if>	<!-- 회원 로그인시 상단 바 끝 -->






<!-- <section class='bb-slice' id='faq'> -->
	<!-- <div class='bb-container bb-container-variant-small' style="margin: 2%"> -->
	<br>
	<h1 id="headline" style="text-align: center; width: 100%; padding: 2%; margin-left: auto; margin-right: auto;"> Realtime Meeting System	</h1>
	<%-- <table>
		<tr>
			<td>
				<input type="text" id="PJNO" style="border: none; background: transparent;" value="ProjectNO.${mt.pjno }" readonly="readonly">
				<input type="text" id="PL" style="width: 33%" value="Project Leader : ${mt.id }" readonly="readonly">
				<input type="text" id="meetingNO" style="width: 32%" value="MeetingNO.${mt.meetingno }" readonly="readonly">
			</td>
		</tr>
		<tr>
			<td>
				<input type="text" id="meetingTitle" style="width: 100%" value="${mt.meetingtitle }" placeholder="Please create meeting name." >
			</td>
		</tr>
	</table> --%>
<div id="wrap">
	<div id="all">
		<p></p>
		<div id="menu">
			<!-- 프로젝트 리더 -->
			<input type="text" class="menu" style="border: none; background-color: #1f2944; color: #fff; margin-top: 2%;" value=" * Project Leader" readonly="readonly"><br>
			<input type="text" class="menu_content" id="PL" style="border: none; background: transparent;" value="${mt.id }" readonly="readonly">
			<br>
			
			<!-- 프로젝트 넘버 -->
			<input type="text" class="menu" style="border: none; background-color: #1f2944; color: #fff; margin-top: 2%;" value=" * Project Number" readonly="readonly"><br>
			<input type="text" class="menu_content" id="PJNO" style="border: none; background: transparent;" value="PJNo. ${mt.pjno }" readonly="readonly">
			<br>
			
			<!-- 회의 넘버 -->
			<input type="text" class="menu" style="border: none; background-color: #1f2944; color: #fff; margin-top: 2%;" value=" * Meeting Number" readonly="readonly"><br>
			<input type="text" class="menu_content" id="meetingNO" style="border: none; background: transparent;" value="No. ${mt.meetingno }" readonly="readonly">
			<br>
			<!-- 회의 제목 -->
			<input type="text" class="menu" style="border: none; background-color: #1f2944; color: #fff; margin-top: 2%;" value=" * Meeting Title" readonly="readonly"><br>
			<input type="text" class="menu_content" id="meetingTitle" style="border: none; background: transparent;" value="< ${mt.meetingtitle } >" placeholder="Please create meeting name." readonly="readonly">
			<br>
			
		    <!-- 채팅 아이디 -->
		    <input type="text" class="menu" style="border: none; background-color: #1f2944; color: #fff; margin-top: 2%;" value=" * User ID" readonly="readonly"><br>
			<input type="text" class="menu_content" id="userid" style="border: none; background: transparent;" placeholder="Input User ID" value="${loginid }" readonly="readonly">
			<br>
		</div>
		<div id="gaunde">
			<!-- 채팅 참여자 -->
			<input type="text" id="userAll" style="text-align: center; border: none; background-color: #1f2944; color: #fff; font-size:x-large; padding: 2%; width: 100%;" placeholder="IDs of members" value=" Participants: ${plist }" readonly="readonly">
			
			<div id="chat">
				<!-- 더 새로운 채팅 aMAZING -->
				<!-- 새로운 채팅부분 -->
			    <textarea id="messageWindow" rows="10"  cols="15" style="text-align: left;" readonly="true"></textarea><br/>
			    <!-- 채팅 내용 입력 창 -->
			    <!-- <input type="text" id="message" name="message" width="500" style="width:100%;" placeholder="Input Message."> -->
			    <!-- 음성인식 진행상태를 나타내는 문구 -->   
				<div id="info">
				  <p id="info_start">Click on the microphone icon and begin speaking.</p>
				  <p id="info_speak_now">Speak now.</p>
				  <p id="info_no_speech">No speech was detected. You may need to adjust your
				    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
				      microphone settings</a>.</p>
				  <p id="info_no_microphone" style="display:none">
				    No microphone was found. Ensure that a microphone is installed and that
				    <a href="//support.google.com/chrome/bin/answer.py?hl=en&amp;answer=1407892">
				    microphone settings</a> are configured correctly.</p>
				  <p id="info_allow">Click the "Allow" button above to enable your microphone.</p>
				  <p id="info_denied">Permission to use microphone was denied.</p>
				  <p id="info_blocked">Permission to use microphone is blocked. To change,
				    go to chrome://settings/contentExceptions#media-stream</p>
				  <p id="info_upgrade">Web Speech API is not supported by this browser.
				     Upgrade to <a href="//www.google.com/chrome">Chrome</a>
				     version 25 or later.</p>
				  <p id="info_stop">Click on the microphone icon and begin speaking.</p>
				</div>
		    
			    <!-- 음성인식 결과가 나타나는 부분 -->
				<div id="results">
					<span id="final_span" class="final"></span>
					<span id="interim_span" class="interim"></span>
					<p>
				</div>
			</div>
			<div id="key">
			    <input type="text" id="inputMessage" style="font-size: x-large; border-color: #1f2944; float:left; width: 79.5%; height: 55px;" name="inputMessage" placeholder="  I n p u t    M e s s a g e ."/>
			    
			    <!-- 텍스트 전송버튼 -->
			    <button id="send" style="background-color:  #1f2944; float:left; text-align: left; font-size: x-large; width: 13%;  height: 55px; margin-right: 0.5%;" onclick="send()">Send</button>
		   		<!-- <input type="button" id="send" style=" float:left; text-align: center; width: 10%; height: 50px;" value="send" onclick="send()" /> -->		   		
		   		
		   		<!-- 음성인식 스타트 버튼 -->
				<div class="right">
					<button id="start_button" style="box-shadow: inset 0 -0.2352941176rem rgba(0,0,0,0)" onclick="startButton(event)">
						<img id="start_img" style="width: inherit; height: 55px; " src="resources/img/mic.gif" alt="Start">
					</button>
				</div>
				<!-- 언어선택 select -->
			  	<div class="center">
					<div id="div_language">
				    <select id="select_language" onchange="updateCountry()"></select>
				    &nbsp;&nbsp;
				    <select id="select_dialect"></select>
					</div>
				  <br>
				</div>
			</div>
		    
			
			
		    
		  	
		</div>
		<div id="aside">
			<div id="asidein" style="float: left;">
				
				<!-- 새로운 파일 업로드 -->
				<!-- <div class="filebox">
				  <input class="upload-name" value="파일선택" disabled="disabled">
				  <label for="ex_filename">업로드</label>
				  <input type="file" name="upload" id="ex_filename" class="upload-hidden">
				</div> -->
			
			
			
			
				<!-- 파일 업로드 -->
			    <form id="uploadForm" name="uploadForm" action="uploadFile" method="post" enctype="multipart/form-data">
			    	
			    	<div class="filebox">
					  <input style="margin-bottom: 5%; background-color: #fff; width: 100%" class="upload-name" value="파일을 선택해 주세요." disabled="disabled">
					  <br><label style="width: 100%; background-color: #000000; height: 50px;" for="ex_filename">파일 선택</label>
					  <input type="file" name="upload" id="ex_filename" class="upload-hidden">
					</div>
			    	<!-- <input type="file" style="width: 100%; height: 50px;" id="file" name="upload" size="30"> -->
			    	<input type="hidden" name="id" value="${loginid }">
			    	<input type="hidden" name="meetingno" value="${mt.meetingno }">
			    	<!-- originalfile, savedfile은 따로 input tag 작성 안 함 -->
			    	<br>
			    	<input type="button" class="library" id="submitFile"  style="display: inline-block; padding: .5em .75em; color: #fff; font-size: inherit; line-height: normal; vertical-align: middle; background-color: #fdfdfd; cursor: pointer; border: 1px solid #ebebeb; border-bottom-color: #e2e2e2; border-radius: .25em;width: 100%; height: 50px; text-align: center; background-color: #000000; color: #fff" name="submitFile" value="파일 업로드">
			    </form>
			    <br>
			    <!-- 첨부된 파일이 있는 경우, 해당 파일을 다운로드 할 수 있는 링크 제공 -->
				<%-- <c:if test="${dlist != null}"> 
				<div id="downloads">
					파일 목록: 
					<c:forEach items="${dlist}" var="down">
						<a href="downloadFile?filenum=${down.filenum}">${down.originalfile}</a>
					</c:forEach>
				</div>
				
				</c:if> --%>	
				<button class="library" id="download" style="display: inline-block; padding: .5em .75em; color: #fff; font-size: inherit; line-height: normal; vertical-align: middle; background-color: #fdfdfd; cursor: pointer; border: 1px solid #ebebeb; border-bottom-color: #e2e2e2; border-radius: .25em;width: 100%; height: 50px; text-align: center; background-color: #000000; color: #fff" onclick="download()">파일다운로드</button>
				<button class="library" style="display: inline-block; padding: .5em .75em; color: #fff; font-size: inherit; line-height: normal; vertical-align: middle; background-color: #fdfdfd; cursor: pointer; border: 1px solid #ebebeb; border-bottom-color: #e2e2e2; border-radius: .25em;width: 100%; height: 50px; text-align: center; background-color: #000000; color: #fff"><a href="hitherto" style="color: #fff;">지난 회의록 다운로드</a></button>		<!-- fileoutputstream에서 true 붙임으로써 해결 -->
				<br>
				
				<button class="library" id="sendAll" style="display: inline-block; padding: .5em .75em; color: #fff; font-size: inherit; line-height: normal; vertical-align: middle; background-color: #fdfdfd; cursor: pointer; border: 1px solid #ebebeb; border-bottom-color: #e2e2e2; border-radius: .25em;width: 100%; height: 50px; text-align: center; background-color: #d95b5b; color: #fff">회의종료</button>
				<!-- <input type="button" id="download" value="파일다운로드" style="width:80%;" onclick="download()"><br> -->
				<!-- 회의 종료 버튼 -->
		    	<!-- <input type="button" id="sendAll" value="회의 종료" style="width:80%;" > -->
		    
		    	<br>
			</div>
			
		</div>
	</div>
</div>
	
	
		
	<%-- <ul id="discussion" style="overflow-y:scroll; border:1px silver dotted; height:100px;">
		${mtxt }
	</ul> --%>
	
	
	
    
    
    
    
	
    
    
    
    
    
    
	  
	  <!-- <div class="sidebyside" style="text-align:right">
	    Copy&Paste 버튼
	    <button id="copy_button" class="button" onclick="copyButton()">
	      Copy and Paste</button>  
	    <div id="copy_info" class="info">
	      Press Control-C to copy text.<br>(Command-C on Mac.)
	    </div>
	  </div>
	  
	  <div class="sidebyside">
	    Email 버튼
	    <button id="email_button" class="button" onclick="emailButton()">
	      Create Email</button>
	    <div id="email_info" class="info">
	      Text sent to default email application.<br>
	      (See chrome://settings/handlers to change.)
	    </div>
	  </div> -->
	  <!-- <p> -->
	  
	  
	<!-- </div>
	
</div> -->
<!-- </section> -->
	
        
  	
	
    <!-- 웹소켓 서버관련 -->
    <script src="resources/jquery-3.3.1.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/signalr/jquery.signalr-2.2.1.min.js"></script>

    <!-- <script type="text/javascript">
        
    </script> -->










<!-- 언어선택 데이터 -->
<script>
//채팅관련 부분
var connection = $.hubConnection('http://demo.dongledongle.com/');
var chat = connection.createHubProxy('chatHub');

//채팅관련 부분 - defined for Sketto
var sysdate;
var discussionText;
var discussionTotal;
var aDown;
var projectNO = document.getElementById('PJNO');
var messageText = document.getElementById('message');
var projectLeder = document.getElementById('PL');
var meetingNO = document.getElementById('meetingNO');
var meetingTitle = document.getElementById('meetingTitle');	//회의록 파일 제목이 아닌 회의 자체의 타이틀
var fileNO= document.getElementById('upload');


$(document).ready(function () {

    

       
        
        $('input[name=message]').keydown(function (key) {	//getMonth()가 0~11인 관계로 getMonth()에서만 parseInt처리. 숫자와 문자열이 혼재되어 뒤의 처리에 지장 있을 수 있음.
            if(key.keyCode == 13) {
            	if(final_transcript == ''){
                	sysdate = new Date();
                	discussionText = "["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"]: "+messageText.value;
                	chat.invoke('send', $('#userid').val(), discussionText);
                    $('#message').val('').focus();
                } else { 
                	sysdate = new Date();
                	discussionText = "["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"]: "+final_transcript;
                	chat.invoke('send', $('#userid').val(), discussionText);
                    $('#message').val('').focus();
                }
            	$('#final_span').html('');
            	$('#interim_span').html('');
            	//$('#results').val('');
            	final_transcript = '';
            }
        	
        });
        
        $('#submitFile').click(function(){
        	var formData = new FormData();
        	formData.append("id", $("input[name=id]").val()); 
        	formData.append("meetingno", $("input[name=meetingno]").val()); 
        	formData.append("upload", $("input[name=upload]")[0].files[0]); 
        	$.ajax({
        		url: 'uploadFile'
        		, data: formData
        		, processData: false
        		, contentType: false
        		, type: 'POST'
        		, success: function(ch){
        			sysdate = new Date();
        			alert("파일을 등록하였습니다.\n"+ch.originalfile);
        			discussionText = "${loginname} "+"["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"](파일등록): " + ch.originalfile  + "\r\n";
        			textarea.value += discussionText;
        	    	webSocket.send(discussionText);
        	    	sendvoice();
        			/* chat.invoke('send', $('#userid').val(), discussionText); */
        			/* aDown = document.createElement('a');
        			var aDownText = document.createTextNode('<다운로드: '+ch.originalfile+'>');
        			aDown.appendChild(aDownText);
        			aDown.href = 'downloadFile?filenum=' + ch.filenum; */
        			$('div#downloads').append('<a href = downloadFile?filenum='+ch.filenum+'>'+ch.originalfile+'</a>');	//이거 파일 올린 당사자한테만 적용됨 ;(
        			$('#message').val('').focus();
        		}
        		, error: function(error){
        			alert(JSON.stringify(error));
        		}
        	});
        });

        
        $('#sendAll').click(function(){
        	//아님 이거 누르면 한꺼번에 저장?
        	alert('회의록을 저장합니다.');
        	location.href="saveMeetingText";	//Controller의 메소드로 연결해야 함. 지금은 .jsp파일을 찾음ㅡㅡ;
        });
        
        /* 2018-04-28 채팅 엔터로 입력 */
        $("#inputMessage").keyup(function(e){if(e.keyCode == 13)  send(); });

       
        
    });


    
//다운로드 버튼 클릭시 새창 열기    
function download() {
	window.open("download","newwin","top=200,left=400,width=400,height=300,resizable=no");
}
    
    
//새로운 채팅 메소드
var textarea = document.getElementById("messageWindow");
//ip부분에 본인 ip입력하면 .
var webSocket = new WebSocket('ws://10.10.12.227:8888/board/broadcasting');
var inputMessage = document.getElementById('inputMessage');
webSocket.onerror = function(event) {
onError(event)
};
webSocket.onopen = function(event) {
onOpen(event)
};
webSocket.onmessage = function(event) {
onMessage(event)
/* $('#messageWindow').scrollTop($('#messageWindow').prop('scrollHeight')); */

};
function onMessage(event) {
textarea.value += event.data;
$('#messageWindow').scrollTop($('#messageWindow').prop('scrollHeight'));
}
function onOpen(event) {
textarea.value += "	 		<< ${loginname}님이 회의에 참여하셨습니다. >>\n";
}
function onError(event) {
alert(event.data);
}
function send() {
	if(final_transcript == ''){
    	sysdate = new Date();
    	discussionText = "${loginname} "+"["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"]: "+inputMessage.value + "\r\n";
    	textarea.value += discussionText;
    	webSocket.send(discussionText);
    	//$('#inputmessage').val('').focus();
        $('#inputmessage').val('');
        $('#inputmessage').focus();
        //alert(textarea.value);
        sendvoice();
    } else { 
    	sysdate = new Date();
    	discussionText = "${loginname} "+"["+sysdate.getFullYear()+'-'+parseInt(sysdate.getMonth()+1)+'-'+sysdate.getDate()+'-'+sysdate.getHours()+'-'+sysdate.getMinutes()+'-'+sysdate.getSeconds()+"]: "+final_transcript + "\r\n";
    	textarea.value += discussionText;
    	webSocket.send(discussionText);
        //$('#inputmessage').val('').focus();
        $('#inputmessage').val('');
       	$('#inputmessage').focus();
        //alert(textarea.value);
       	sendvoice();
    }
	$('#final_span').html('');
	$('#interim_span').html('');
	//$('#results').val('');
	final_transcript = '';
	inputMessage.value = "";
	
/* textarea.value += "나 : " + inputMessage.value + "\n";
webSocket.send(inputMessage.value);
inputMessage.value = "";
alert(textarea.value); */
}


//send보이스
function sendvoice() {
	//alert("보내는 텍스트: "+final_transcript);	//success는 되지만 값이 출력되었다가 안 되었다가 함. 여기서 final_transcript를 출력해주지 않으면 뒤에도 전부 공란임.

  $.ajax({
        url : 'voice',
        type : 'GET',
        dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
        //traditional:true,
        data : {'voice': discussionText	},
        //data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
        //contentType:"application/json",
        success : function(voice) {
            //alert('성공\n'+voice);
        },
        error : function(error) {
            alert('에러발생\n'+JSON.stringify(error));
        }
    });
	
  $('#messageWindow').scrollTop($('#messageWindow').prop('scrollHeight'));

}

function htmlEncode(value) {
    var encodedValue = $('<div />').text(value).html();
    return encodedValue;
}


//음성인식 관련 부분
var langs =
[['Afrikaans',       ['af-ZA']],
 ['Bahasa Indonesia',['id-ID']],
 ['Bahasa Melayu',   ['ms-MY']],
 ['Català',          ['ca-ES']],
 ['Čeština',         ['cs-CZ']],
 ['Deutsch',         ['de-DE']],
 ['English',         ['en-AU', 'Australia'],
                     ['en-CA', 'Canada'],
                     ['en-IN', 'India'],
                     ['en-NZ', 'New Zealand'],
                     ['en-ZA', 'South Africa'],
                     ['en-GB', 'United Kingdom'],
                     ['en-US', 'United States']],
 ['Español',         ['es-AR', 'Argentina'],
                     ['es-BO', 'Bolivia'],
                     ['es-CL', 'Chile'],
                     ['es-CO', 'Colombia'],
                     ['es-CR', 'Costa Rica'],
                     ['es-EC', 'Ecuador'],
                     ['es-SV', 'El Salvador'],
                     ['es-ES', 'España'],
                     ['es-US', 'Estados Unidos'],
                     ['es-GT', 'Guatemala'],
                     ['es-HN', 'Honduras'],
                     ['es-MX', 'México'],
                     ['es-NI', 'Nicaragua'],
                     ['es-PA', 'Panamá'],
                     ['es-PY', 'Paraguay'],
                     ['es-PE', 'Perú'],
                     ['es-PR', 'Puerto Rico'],
                     ['es-DO', 'República Dominicana'],
                     ['es-UY', 'Uruguay'],
                     ['es-VE', 'Venezuela']],
 ['Euskara',         ['eu-ES']],
 ['Français',        ['fr-FR']],
 ['Galego',          ['gl-ES']],
 ['Hrvatski',        ['hr_HR']],
 ['IsiZulu',         ['zu-ZA']],
 ['Íslenska',        ['is-IS']],
 ['Italiano',        ['it-IT', 'Italia'],
                     ['it-CH', 'Svizzera']],
 ['Magyar',          ['hu-HU']],
 ['Nederlands',      ['nl-NL']],
 ['Norsk bokmål',    ['nb-NO']],
 ['Polski',          ['pl-PL']],
 ['Português',       ['pt-BR', 'Brasil'],
                     ['pt-PT', 'Portugal']],
 ['Română',          ['ro-RO']],
 ['Slovenčina',      ['sk-SK']],
 ['Suomi',           ['fi-FI']],
 ['Svenska',         ['sv-SE']],
 ['Türkçe',          ['tr-TR']],
 ['български',       ['bg-BG']],
 ['Pусский',         ['ru-RU']],
 ['Српски',          ['sr-RS']],
 ['한국어',            ['ko-KR']],
 ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
                     ['cmn-Hans-HK', '普通话 (香港)'],
                     ['cmn-Hant-TW', '中文 (台灣)'],
                     ['yue-Hant-HK', '粵語 (香港)']],
 ['日本語',           ['ja-JP']],
 ['Lingua latīna',   ['la']]];
for (var i = 0; i < langs.length; i++) {
  select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 6;
updateCountry();
select_dialect.selectedIndex = 6;
showInfo('info_start');


function updateCountry() {
  for (var i = select_dialect.options.length - 1; i >= 0; i--) {
    select_dialect.remove(i);
  }
  var list = langs[select_language.selectedIndex];
  for (var i = 1; i < list.length; i++) {
    select_dialect.options.add(new Option(list[i][1], list[i][0]));
  }
  select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}
var create_email = false;
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;

if (!('webkitSpeechRecognition' in window)) {
  upgrade();
} else {
  start_button.style.display = 'inline-block';
  
  var recognition = new webkitSpeechRecognition();
  recognition.continuous = true;
  recognition.interimResults = true;
  recognition.onstart = function() {
    recognizing = true;
    showInfo('info_speak_now');
    start_img.src = 'resources/img/mic-animate.gif';
  };
  recognition.onerror = function(event) {
    if (event.error == 'no-speech') {
      start_img.src = 'resources/img/mic.gif';
      showInfo('info_no_speech');
      ignore_onend = true;
    }
    if (event.error == 'audio-capture') {
      start_img.src = 'resources/img/mic.gif';
      showInfo('info_no_microphone');
      ignore_onend = true;
    }
    if (event.error == 'not-allowed') {
      if (event.timeStamp - start_timestamp < 100) {
        showInfo('info_blocked');
      } else {
        showInfo('info_denied');
      }
      ignore_onend = true;
    }
  };
  recognition.onend = function() {
    recognizing = false;
    if (ignore_onend) {
      return;
    }
    start_img.src = 'resources/img/mic.gif';
    if (!final_transcript) {
      showInfo('info_start');
      return;
    }
    showInfo('info_stop');
    if (window.getSelection) {
      window.getSelection().removeAllRanges();
      var range = document.createRange();
      range.selectNode(document.getElementById('final_span'));
      window.getSelection().addRange(range);
    }
    if (create_email) {
      create_email = false;
      createEmail();
    }
  };
  recognition.onresult = function(event) {
    var interim_transcript = '';
    for (var i = event.resultIndex; i < event.results.length; ++i) {
      if (event.results[i].isFinal) {
        final_transcript += event.results[i][0].transcript;
      } else {
        interim_transcript += event.results[i][0].transcript;
      }
    }
    final_transcript = capitalize(final_transcript);
    final_span.innerHTML = linebreak(final_transcript);
    interim_span.innerHTML = linebreak(interim_transcript);
    if (final_transcript || interim_transcript) {
      showButtons('inline-block');
      showInfo('info_upgrade');
      
    }
  };
}



function upgrade() {
  start_button.style.visibility = 'hidden';
  showInfo('info_upgrade');
}
var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
  return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
var first_char = /\S/;
function capitalize(s) {
  return s.replace(first_char, function(m) { return m.toUpperCase(); });
}

/* 이메일 전송 */
/* function createEmail() {
  var n = final_transcript.indexOf('\n');
  if (n < 0 || n >= 80) {
    n = 40 + final_transcript.substring(40).indexOf(' ');
  }
  var subject = encodeURI(final_transcript.substring(0, n));
  var body = encodeURI(final_transcript.substring(n + 1));
  window.location.href = 'mailto:?subject=' + subject + '&body=' + body;
} */

/* 텍스트 복사버튼 클릭시  */
/* function copyButton() {
	if (recognizing) {
		recognizing = false;
		recognition.stop();
		}
	//alert()
	copy_button.style.display = 'none';
	copy_info.style.display = 'inline-block';
	showInfo('');
	//final_transcript에 인식된 텍스트가 담겨있음을 확인
	//alert(final_transcript);
} */


/* 이메일 전송버튼 클릭시  */
/* function emailButton() {
  if (recognizing) {
    create_email = true;
    recognizing = false;
    recognition.stop();
  } else {
    createEmail();
  }
  email_button.style.display = 'none';
  email_info.style.display = 'inline-block';
  showInfo('');
} */

/* 음성인식 동작 버튼 */
function startButton(event) {
  if (recognizing) {
    recognition.stop();
    return;
  }
  final_transcript = '';
  recognition.lang = select_dialect.value;
  recognition.start();
  ignore_onend = false;
  final_span.innerHTML = '';
  interim_span.innerHTML = '';
  start_img.src = 'resources/img/mic-slash.gif';
  showInfo('info_allow');
  showButtons('none');
  start_timestamp = event.timeStamp;
}

/* 음성인식 동작시 상태 표시  */
function showInfo(s) {
  if (s) {
    for (var child = info.firstChild; child; child = child.nextSibling) {
      if (child.style) {
        child.style.display = child.id == s ? 'inline' : 'none';
      }
    }
    info.style.visibility = 'visible';
  } else {
    info.style.visibility = 'hidden';
  }
}
var current_style;

/* 음성을 검출시 버튼이 화면에 표시 */
/* function showButtons(style) {
  if (style == current_style) {
    return;
  }
  current_style = style;
  copy_button.style.display = style;
  email_button.style.display = style;
  copy_info.style.display = 'none';
  email_info.style.display = 'none';
} */


/* 새로운 파일시스템 */
var fileTarget = $('.filebox .upload-hidden');

fileTarget.on('change', function(){ // 값이 변경되면
  if(window.FileReader){ // modern browser
    var filename = $(this)[0].files[0].name;
  }
  else { // old IE
    var filename = $(this).val().split('/').pop().split('\\').pop(); // 파일명만 추출
  }

  // 추출한 파일명 삽입
  $(this).siblings('.upload-name').val(filename);
});




</script>
</body>
</html>