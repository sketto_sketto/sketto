<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ page errorPage="error" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="resources/css/bootstrapu.css" rel="stylesheet" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<style>
div {
 font-size:5vw;
}

.boxCalendar {
	display: inline-block;
	width: 5vw;
	height: 5vw;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
	font-size: 1px;
}

.boxDiv {
	position: relative;
	overflow: hidden;
	display: inline-block;
	width: 5vw;
	height: 5vw;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
	border-top: 2px solid #ffcb5e;
	font-size: 2vw;
}

.dragClass {
	position: static;
	top: auto;
	left: auto;
	display: inline-block;
	width: 100%;
	height: 100%;
	border: 0px solid #aaaaaa;
	background-color: coral;
	font-size: 1px;
}
</style>

<script src="resources/moment-with-locales.js"></script>
<script type="text/javascript">
	/* 프로젝트 시작일과 마감일 받고 시작할 것 */
	
	var startdate = moment('${pj.startdate}').format('YYYY-MM-DD-hh-mm-ss');
	var enddate = moment('${pj.enddate}').format('YYYY-MM-DD-hh-mm-ss');

	console.log('원래 프로젝트 기간:::' + startdate + '~' + enddate);

	var firstDate = stringToDate(startdate);
	console.log('firstDate ::::::::' + firstDate);
	var firstDateYear = moment(firstDate).format('YYYY');
	console.log('firstDateYear ::::::::' + firstDateYear);
	firstDate = new Date(firstDateYear, firstDate.getMonth(), 1);
	startdate = moment(firstDate).format('YYYY-MM-DD-hh-mm-ss');

	var lastDate = stringToDate(enddate);
	console.log('lastDate ::::::::' + lastDate);
	var lastDateYear = moment(lastDate).format('YYYY');
	console.log('lastDateYear ::::::::' + lastDateYear);
	lastDate = new Date(lastDateYear, lastDate.getMonth() + 1, 0);

	enddate = moment(lastDate).format('YYYY-MM-DD-HH-mm-ss');

	console.log('달력 출력:::' + startdate + '~' + enddate);
	
	var todayDate = new Date();	
	var todayDateToString = moment(todayDate).format('YYYY-MM-DD-hh-mm-ss');	
	
	var todayDateXval = null; //아래의 calDateRange에서 값을 초기화할 계획.
	//todayDateXval = calDateRange(startdate, todayDateToString); 아래에 이거 쓰여져 있음
	
	
	var scheduleVariety = 5;
	<c:forEach items="${planList}" var="planString">
	scheduleVariety = scheduleVariety+1;
	</c:forEach>
	
	//기본적인 스케쥴러 행 수. 이를 조절하면 11, 12줄... 30줄... 등 열의 증감이 가능하다

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function calDateRange(startdate, enddate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크 startdate
		if (startdate.length != 19) {
			return null;
		}

		// FORMAT을 포함한 길이 체크 enddate 
		if (enddate.length != 19) {
			return null;
		}

		// FORMAT이 있는지 체크
		if (startdate.indexOf(FORMAT) < 0 || enddate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var start_dt = startdate.split(FORMAT);
		var end_dt = enddate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		start_dt[1] = (Number(start_dt[1]) - 1) + "";
		end_dt[1] = (Number(end_dt[1]) - 1) + "";

		var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2],
				00, 00, 00);
		var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2], 00,
				00, 00);

		if (from_dt > to_dt) {
			return parseInt((from_dt.getTime() - to_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else if (from_dt < to_dt) {
			return parseInt((to_dt.getTime() - from_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else {
			return 1;
		}
	}

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function stringToDate(varDate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크
		if (varDate.length != 19)
			return null;

		// FORMAT이 있는지 체크
		if (varDate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var var_dt = varDate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		var_dt[1] = (Number(var_dt[1]) - 1) + "";

		var return_dt = new Date(var_dt[0], var_dt[1], var_dt[2], var_dt[3],
				var_dt[4], var_dt[5]);

		return return_dt;
	}//end StringToDate

	function allowDrop(ev) {
		ev.preventDefault();
	}

	var oldParentID;
	var newParentID;

	function drag(ev) {
		//drag 시의 이벤트
		ev.dataTransfer.setData("dragText", ev.target.id);
		//dragText라는 임의 속성 부여해 그 것에 id 넣는다.
		var divId = ev.dataTransfer.getData("dragText");
		//해당 id를 되받아오는 과정. 이를 통해 정말 dragText에 id가 들어갔는지 확실하게 한다.
		var originalX = document.getElementById(divId).getAttribute('x');
		//divId를 응용해 그 x 값을 가져온다.
		var originalY = document.getElementById(divId).getAttribute('y');
		//divId를 응용해 그 y 값을 가져온다.
		oldParentID = 'dropon' + originalX + originalY;
		//drag한 divId가 속한 빈 상자의 id를 가져온다.

	} //드래그하면 넣을 div에 이름을 부여한다.

	function drop(ev) {
		//드롭 기능은 배치 등 사용자가 실행한 변경 사항이기 때문에,
		//그에 따른 일정의 수정 및 업데이트 기능 역시 병행한다

		if (confirm("해당 일정을 변경하시겠습니까?")) {
			//이후에 이것을 일정 변경에 대한 yes / no 여부로 확인
			ev.preventDefault();

			var divId = ev.dataTransfer.getData("dragText");
			//Drop한 id 가져오기 (drag할 때 "dragText"에 속성 부여했음)
			var originalX = document.getElementById(divId).getAttribute('x');
			var originalY = document.getElementById(divId).getAttribute('y');
			//Drop한  div의 x, y값 가져온다
			var originalI = document.getElementById(divId).getAttribute('i');
			//i값 가져오기 : 같은 일정의 div에서 몇번째인가
			var originalWidthVal = document.getElementById(divId).getAttribute(
					'widthVal');
			//widthVal값 가져오기 : 같은 일정의 div에서 몇번째까지 있는가

			var newX = Number(document.getElementById(ev.target.id)
					.getAttribute('x'));
			var newY = Number(document.getElementById(ev.target.id)
					.getAttribute('y')); //새로  보낼 부모 div의 x,y 가져옴
			//Drop한  div의 x, y값 가져온다
			newParentID = 'dropon' + newX + newY;
			//일정 옮기기 위한 드롭할 부모 div의 이름

			var Xdifference = (Number(originalX) - Number(newX));

			console.log('Number(originalX) (' + Number(originalX)
					+ ') - Number(newX)(' + Number(newX) + ') = Xdifference('
					+ Xdifference + ')');

			var Ydifference = (Number(originalY) - Number(newY));

			console.log('Number(originalY) (' + Number(originalY)
					+ ') - Number(newY)(' + Number(newY) + ') = Ydifference('
					+ Ydifference + ')');

			//id 형식 : 'draggable' + '${planString.pno}' + 'no' + i
			var idForm = document.getElementById(divId).getAttribute('id');
			idForm = idForm.slice(0, idForm.lastIndexOf("o") + 1);
			//해당 일정이 가지고 있는 id 규격(draggable + 일정넘버. 여기에 '작은 div 순서'를 추가할 것임)을 가져온다.

			var pstartdate = document.getElementById(idForm + 0).getAttribute(
					'pstartdate');
			var penddate = document.getElementById(idForm + 0).getAttribute(
					'penddate');
			var xStartDate = Number(document.getElementById(idForm + 0)
					.getAttribute('xStartDate')); //처음 시작되는 x의 값
			var xLengthIns = Number(document.getElementById(idForm + 0)
					.getAttribute('xLengthIns')); //처음 설정되는 x의 길이
			//xStartDate // div들 줄 있던 곳 첫번째
			//xLengthIns // div들 줄 있던 곳 마지막까지의 길이 
			//originalX //예전의 div 위치 //떠난 곳
			//newX //새롭게 이동하는 div의 위치 //가는 곳

			var booleanChangingOnlyEndDate = false;
			//End Date만 변경하겠다. 는 조건 / 사용자의 결정을 묻고자 하는 Boolean

			if (divId == (idForm + Number(originalWidthVal - 1))) {
				// Ydifference가 0일 때 : 같은 줄 안에서 기간의 div만 움직였는데.
				// divId가 div 포맷에서 div 길이의 마지막번째일때 : 마지막 div를 움직였을 때.

				if (Number(newX) > Number(xStartDate)
						&& Number(newX) < Number(xStartDate + xLengthIns)) {
					//Number(newX) > Number(xStartDate) : 
					//새로 이동하는 div의 위치가 원래의 div 중간에 있을 경우
					//StartDate는 변경하지 않는다. EndDate만 변경한다.
					if (confirm('기간 조절이 이뤄집니다 : 기간 축소')) {
						booleanChangingOnlyEndDate = true;
						//싫다고 하면 return한다.
					} else {
						booleanChangingOnlyEndDate = false;
						return;
						//y값은 그대로고 x값은 범위 안에서 변경 되었으니까 EndDate만 변경하자
					}
				}

				else if (Number(newX) > Number(xStartDate + xLengthIns)) {
					//Number(newX) > Number(xStartDate) : 
					//새로 이동하는 div의 위치가 원래의 div보다 더 오래 있을 경우
					//StartDate는 변경하지 않는다. EndDate만 변경한다.
					if (confirm('기간 조절이 이뤄집니다 : 기간 연장')) {
						booleanChangingOnlyEndDate = true;
						//싫다고 하면 return한다.
					} else {
						booleanChangingOnlyEndDate = false;
						return;
						//y값은 그대로고 x값은 범위 안에서 변경 되었으니까 EndDate만 변경하자
					}
				}
			}

			var IntoX; //한꺼번에 여러 div 들을 옮기기 위해 임시로 값을 넣을 intoX를 만든다
			var IntoY; //한꺼번에 여러 div 들을 옮기기 위해 임시로 값을 넣을 intoY를 만든다

			for (var j = 0; j < originalWidthVal; j++) {
				//일정의 길이만큼 for문 돌아가게 한다.

				//document.getElementById(idForm+j)는 해당 일정의 id 배열
				IntoX = Number(document.getElementById(idForm + j)
						.getAttribute('x'))
						- Xdifference;
				//각 'idForm + j'의 x 값에서, x 차이값 빼서 새로 넣을 intoX 설정해주기 
				IntoY = Number(document.getElementById(idForm + j)
						.getAttribute('y'))
						- Ydifference;
				//각 'idForm + j'의 y 값에서, y 차이값 빼서 새로 넣을 intoX 설정해주기 

				if (document.getElementById(divId) == document
						.getElementById(idForm + j)
						|| booleanChangingOnlyEndDate) {
					//원래 가져다놓는 div일 때 : Xdifference / Ydifference를 이미 한번 뺐음
					//또 뺄 필요 없으니까 위의 IntoX, IntoY 계산을 무효화

					//booleanChangingOnlyEndDate가 true면 이동할 필요가 없음
					IntoX = Number(document.getElementById(idForm + j)
							.getAttribute('x'));

					IntoY = Number(document.getElementById(idForm + j)
							.getAttribute('y'));
				}

				document.getElementById(idForm + j).setAttribute('x', IntoX);
				document.getElementById(idForm + j).setAttribute('y', IntoY);
				//일단은 계산했든 무효화했든 새로운 좌표 부여

				newParentID = 'dropon' + IntoX + IntoY;
				//그 x,y를 대입한 새로운 parent에
				if (document.getElementById(newParentID) != null) {
					document.getElementById(newParentID).appendChild(
							document.getElementById(idForm + j));
				}
				//append 한다.

				// if 안에 매겨서 드래그한 해당 div인지 아닌지 구분한다
				//해당 div일 경우에는 옮긴만큼 옮기면 되고,
				//해당 div가 아니면 옮긴 div의 변화 값만큼 다른 div의 x, y에 대입하여 옮긴다.

				var dateChangeIns = Xdifference;
				//Xdifference는 originalX에서 newX를 뺀 값
				//날짜값의 변경 정도 (현재는 일 단위이기 때문에)
				var startdateToDate = stringToDate(pstartdate);
				//원래의 pstartdate를 Date형식의 변수 startdateToDate로 전환
				var enddateToDate = stringToDate(penddate);
				//원래의 penddate를 Date형식의 변수 enddateToDate로 전환

				if (booleanChangingOnlyEndDate) {
					startdateToDate.setDate(stringToDate(pstartdate).getDate());
					//EndDate만 변경하겠다. 의 Boolean
				} else {
					startdateToDate.setDate(startdateToDate.getDate()
							- dateChangeIns);
					//varDate에서 변경된만큼(varDate.getDate() - dateChange)의 날짜 변경 적용
				}
				enddateToDate.setDate(enddateToDate.getDate() - dateChangeIns);

				if (startdateToDate > enddateToDate) {
					concole.log('startdateToDate(' + startdateToDate
							+ ') > enddateToDate(' + enddateToDate + ')');

					var temp = startdateToDate;
					startdateToDate = enddateToDate;
					enddateToDate = temp;
				}

				var startdateEdittedString = moment(startdateToDate).format(
						'YYYY-MM-DD-hh-mm-ss');
				//해당 페이지에서 조절할 수 있는 형식으로 전환

				document.getElementById(idForm + j).setAttribute('pstartdate',
						startdateEdittedString);

				//varDate에서 변경된만큼(varDate.getDate() - dateChange)의 날짜 변경 적용
				var enddateEdittedString = moment(enddateToDate).format(
						'YYYY-MM-DD-hh-mm-ss');//해당 페이지에서 조절할 수 있는 형식으로 전환

				document.getElementById(idForm + j).setAttribute('penddate',
						enddateEdittedString);

			}//for문 끝나는 곳

			//update ajax 시작

			var insPNO = document.getElementById(idForm + 0).getAttribute('pno');
			insPNO = insPNO.replace("\"", "");
			insPNO = insPNO.replace("\"", "");

			var planNameIns = document.getElementById(idForm + 0).getAttribute(
					'planName').toString().replace("\"", "");
			planNameIns = planNameIns.replace("\"", "");
			//양 옆의 큰따옴표 제거

			var idListIns = JSON.parse(document.getElementById(idForm + 0)
					.getAttribute('memberIdIns')).toString();

			//idList

			console.log("insPNO ::::::::::::::" + insPNO);
			console.log("planName ::::::::::::::"
					+ document.getElementById(idForm + 0).getAttribute(
							'planName'));
			console.log("planName ::::::::::::::" + planNameIns);
			console.log("memberIdIns ::::::::::::::"
					+ document.getElementById(idForm + 0).getAttribute(
							'memberIdIns'));
			console.log("memberIdIns ::::::::::::::" + idListIns);

			var planInsJSON = {
				"PNO" : insPNO,
				"planName" : planNameIns,
				"pStartDate" : document.getElementById(idForm + 0)
						.getAttribute('pstartdate'),
				"pEndDate" : document.getElementById(idForm + 0).getAttribute(
						'penddate'),
				"PJNo" : document.getElementById(idForm + 0).getAttribute(
						'PJNo'),
				"id" : idListIns,
				"Priority" : document.getElementById(idForm + 0).getAttribute(
						'Priority'),
				"Progress" : document.getElementById(idForm + 0).getAttribute(
						'Progress'),
				"color" : document.getElementById(idForm + 0).getAttribute(
						'color')
			};
			//VO와 같은 형식으로 일치시켜야 한다.
			//id는 추후 ArrayList로 변경하여, 여러 팀원이 배정 받고 빠져나갈 수 있도록 해야 한다.

			pageLoad();

			$
					.ajax({
						url : 'updateJ',
						type : 'GET',
						dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
						//data : {'voice': final_transcript},
						data : {
							'jsonString' : JSON.stringify(planInsJSON)
						},
						//data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
						//contentType:"application/json",
						success : function() {
							parent.scheduleIframe.src = parent.scheduleIframe.src;
						},
						error : function(error) {
							alert('에러발생\n' + JSON.stringify(error));
						}
					});

			//update ajax 끝
		}//function 끝나는 부분

	}//end drop

	function pageLoad() {
/* 
		console.log(moment(todayDate).format('YYYY-MM-DD'));
		//원하는 포맷으로 날짜 가져오기 샘플 */
		
		document.getElementById('scheduleDiv').innerHTML = "";

		var varDate = stringToDate(startdate);
		console.log(varDate);
		//startdate를 stringToDate() 메소드를 사용해서 date 포맷으로 변환한 것.
		//이걸 통해서 요일이나 월 등을 분석할 때 써먹는다. 하루씩 더해가지고 getMonth하고 getDay하고

		var schedulePeriod = calDateRange(startdate, enddate);

		/* 여기서부터는 일요일 표시, 기준 점 작성 */
		for (var j = 0; j < schedulePeriod; j++) {
			var newDiv = document.createElement('DIV'); // DIV 객체 생성

			newDiv.setAttribute('class', 'boxDiv');

			if (varDate.getDay() == 0) {
				newDiv.setAttribute('style', 'background-color:#d95b5b');
			}

			if (varDate.getDate() == 1) {
				newDiv.setAttribute('style', 'background-color:#1f2944; color:#fff; border-left: 1px solid #5c948d;');
				newDiv.innerHTML += moment(varDate).format('MMMM') + '<br>';
			} else {
				newDiv.innerHTML += '<br>';
			}

			if (moment(varDate).format('YYYYMMDD') ==
				moment(todayDate).format('YYYYMMDD')) {
				newDiv.setAttribute('name', 'today');
				newDiv.innerHTML += ' <input type="hidden" id="todayToScroll">';
			}
			
			newDiv.innerHTML += varDate.getDate();
			varDate.setDate(varDate.getDate() + 1);

			//1일이 될 때마다 색을 부여한다.
			//일주일이 될 때마다 색을 부여한다.

			document.getElementById('scheduleDiv').appendChild(newDiv);
		}

		document.getElementById('scheduleDiv').innerHTML += '<br>';
		//엔터 친다.

		for (var i = 0; i < scheduleVariety; i++) {
			var varDateEach = stringToDate(startdate);
			//각각의 y 좌표 (일정 구분)에 따른 x 값 기준(날짜)이 될 것
			for (var j = 0; j < schedulePeriod; j++) {
				var newDiv = document.createElement('DIV'); // DIV 객체 생성

				newDiv.setAttribute('x', j); // x값 지정;
				newDiv.setAttribute('y', i); // y값 지정
				newDiv.setAttribute('id', 'dropon' + j + i); // x값 지정
				newDiv.setAttribute('class', 'boxDiv');
				newDiv.setAttribute('ondrop', 'drop(event)');
				newDiv.setAttribute('ondragover', 'allowDrop(event)');
				document.getElementById('scheduleDiv').appendChild(newDiv);

			}
			document.getElementById('scheduleDiv').innerHTML += '<br>';
		}

	}//end pageload

	todayDateXval = calDateRange(startdate, todayDateToString);
	
	function scheduleUpdate() {

		var yIns = 0;

		console.log('${planList}');

		//y축 값은 나중에 일정을 출력하면서 select에 order를 매기는 식으로 변경 가능.

		<c:forEach items="${planList}" var="planString">

		console.log('${planString.pstartdate}');

		var pstartdate = '${planString.pstartdate}';
		pstartdate = pstartdate.substring(0, 19);
		pstartdate = pstartdate.replace(" ", "-");
		pstartdate = pstartdate.replace(":", "-");
		pstartdate = pstartdate.replace(":", "-");

		var penddate = '${planString.penddate}';
		penddate = penddate.substring(0, 19);
		penddate = penddate.replace(" ", "-");
		penddate = penddate.replace(":", "-");
		penddate = penddate.replace(":", "-");

		console.log(pstartdate);
		console.log('${planString.color}');

		var color = '${planString.color}';
		color = color.replace("\"", "");
		color = color.replace("\"", "");

		var xStartDate = calDateRange(startdate, pstartdate);
		//x값이 시작되는 위치
		var xLengthIns = calDateRange(pstartdate, penddate);
		//프로젝트 시작일부터 해당 일정 시작일을 뺀 값만큼이 x축 값.

		var widthVal = xLengthIns + 1;

		for (var i = 0; i < widthVal; i++) { //길이만큼의 div를 하나씩 배정할 예정
			if (document.getElementById('draggable' + '${planString.pno}'
					+ 'no' + i) != null) {
				$('#draggable' + '${planString.pno}' + 'no' + i).remove();
				console.log('::::::::::::::::::::::::제거')
			}
			var newDiv = document.createElement('DIV'); // DIV 객체 생성
			var xIns = xStartDate + i;
			// x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
			newDiv.setAttribute('x', xIns); // x값 지정. i값만큼 옆으로 옮기고, 그만큼 x좌표값을 추가한다.
			newDiv.setAttribute('y', yIns); // y값 지정
			newDiv.setAttribute('i', i); // div의 순서. i값 지정
			newDiv.setAttribute('pno', '${planString.pno}');
			newDiv.setAttribute('pstartdate', pstartdate);
			newDiv.setAttribute('penddate', penddate);
		
			if (xIns == todayDateXval) {
				//해당 x 값이 today의 x값과 같다면
				newDiv.setAttribute('name','today');
			}
			
			newDiv.setAttribute('planName', '${planString.planname}');
			newDiv.setAttribute('PJNo', '${planString.pjno}');
			newDiv.setAttribute('memberIdIns', '${planString.id}');
			newDiv.setAttribute('Priority', '${planString.priority}');
			newDiv.setAttribute('Progress', '${planString.progress}');
			newDiv.setAttribute('color', color);
			newDiv.setAttribute('xStartDate', xStartDate);
			newDiv.setAttribute('xLengthIns', xLengthIns);
			newDiv.setAttribute('onclick', "openPlan('${planString.pno}')");
			//newDiv.innerHTML += i; //i값 들어가게 해서 순번 확인하려고

			newDiv.setAttribute('widthVal', widthVal); // x값 지정
			newDiv.setAttribute('id', 'draggable' + '${planString.pno}' + 'no'
					+ i); // x값 지정
			newDiv.setAttribute('style', 'width: 100%;' + 'height: 100%;'
					+ 'border: 0px solid #aaaaaa;' + 'background-color: '
					+ color);
			newDiv.setAttribute('draggable', 'true');
			newDiv.setAttribute('ondragstart', 'drag(event)');
			newDiv.setAttribute('onmouseover',"mouseOver(this)");
			newDiv.setAttribute('onmouseout',"mouseOut(this)");

			var parentID = 'dropon' + xIns + yIns;

			console.log(':::::::::::::::::::::appending');

			if (document.getElementById(parentID) != null) {
				document.getElementById(parentID).appendChild(newDiv);
			}

		}

		yIns++;

		</c:forEach>
	}//end scheduleUpdate()

	function openPlan(pno) {
		
		var popupWidth = 600;
		var popupHeight = 500;
		var popupX = (window.screen.width / 2) - (popupWidth / 2);
		// 만들 팝업창 좌우 크기의 1/2 만큼 보정값으로 빼주었음

		var popupY= (window.screen.height /2) - (popupHeight / 2);
		// 만들 팝업창 상하 크기의 1/2 만큼 보정값으로 빼주었음

		//pno를 갖고 컨트롤러로 이동한다
		window
				.open("getThePlan?pno=" + pno, "일정내용",
						'width='+popupWidth+'px, height='+popupHeight+'px, toolbar=no, menubar=no, scrollbars=no, resizable=yes left='
						+ popupX + ', top='+ popupY + ', screenX='+ popupX + ', screenY= '+ popupY);
	}
	
	function mouseOver(divMouseOver) {
		console.log(divMouseOver);
	}

	function mouseOut(divMouseOver) {
		console.log(divMouseOver);
	}
	
	var intervalOn = false;
	var blinkIntervalBoolean = false;
	var blinkInterval = null;
	var getName = null;
	
		function blinking() {
			var blinkingObject = document.getElementsByName('today');

			intervalOn = !intervalOn;

			if (intervalOn) {
				clearInterval(blinkInterval);
				for (var i = 0; i < blinkingObject.length; i++) {
					if (blinkingObject[i].getAttribute('color') != null) {
						$(blinkingObject[i]).css("background-color",
								blinkingObject[i].getAttribute('color'));
					} else {
						$(blinkingObject[i]).css("background-color", "#ffcb5e");
					}
				}
				return;
			}


			blinkInterval = setInterval(
					function() {
						blinkIntervalBoolean = !blinkIntervalBoolean;
						console.log(blinkIntervalBoolean);

						for (var i = 0; i < document.getElementsByName('today').length; i++) {
							if (blinkIntervalBoolean) {
								$(blinkingObject[i]).css("background-color",
										"#ffcb5e");
							} else {
								if (blinkingObject[i].getAttribute('color') != null) {
									$(blinkingObject[i]).css(
											"background-color",
											blinkingObject[i]
													.getAttribute('color'));
								} else {
									$(blinkingObject[i]).css(
											"background-color", "#FFFFFF");
								}
							}
						}
					}, 500);
		}

</script>

</head>
<body>

	<div id="scheduleDiv"
		style="white-space: nowrap; overflow: auto; margin: 0px; padding: 0px" align="center"></div>

	<script type="text/javascript">
		console.log(Number('${pjNO}'));

		pageLoad();
		
		scheduleUpdate();
		
	</script>
	
</html>