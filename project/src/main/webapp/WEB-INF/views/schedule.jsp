<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page errorPage="error"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="resources/css/bootstrapu.css" rel="stylesheet" />
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<!-- <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
<script src="resources/jquery-3.3.1.js"></script>

<style>
div {
	font-size: large;
}

.boxCalendar {
	display: inline-block;
	width: 22px;
	height: 22px;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.boxDiv {
	position: relative;
	overflow: hidden;
	display: inline-block;
	width: 22px;
	height: 22px;
	margin: 0px;
	padding: 0px;
	border: 0px solid #aaaaaa;
}

.dragClass {
	position: static;
	top: auto;
	left: auto;
	display: inline-block;
	width: 20px;
	height: 20px;
	border: 0px solid #aaaaaa;
	background-color: coral;
}
/* 상단 바  */
/* Add a black background color to the top navigation */
.topnav {
	background-color: #1f2944;
	overflow: hidden;
}

/* Style the links inside the navigation bar */
.topnav a {
	float: left;
	color: #fff;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	font-size: 17px;
	margin-top: 15px;
}

/* Change the color of links on hover */
.topnav a:hover {
	background-color: #ddd;
	color: black;
}

/* Add a color to the active/current link */
.topnav a.active {
	background-color: #5c948d;
	color: white;
}
/* 상단 바 끝 */
.card text-center {
	width: 18rem;
	FLOAT: LEFT;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
	position: relative;
	display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f1f1f1;
	min-width: 160px;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
	color: black;
	padding: 12px 16px;
	text-decoration: none;
	display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {
	background-color: #ddd
}

/* Show the dropdown menu on hover */
.dropdown:hover .dropdown-content {
	display: block;
}

/* Change the background color of the dropdown button when the dropdown content is shown */
.dropdown:hover .dropbtn {
	background-color: #3e8e41;
}
</style>

<script src="resources/moment-with-locales.js"></script>
<script type="text/javascript">

window.onload=function(){window.ActionDeterminator=function(){};};

	/* 프로젝트 시작일과 마감일 받고 시작할 것 */
	var startdate = "2018-04-01-00-00-00";
	var enddate = "2018-06-30-00-00-00";
	//프로젝트 종료일 추가 / 변경 가능하도록 할 것(1개월 추가, 3개월 추가 등)
	//혹은 시작/종료를 초과하는 일정이 추가되면 시작일과 종료일이 조정되도록.
	//진행도가 하단에 추가되는 것도 고려할 것.
	var todayDate = new Date();
	var todayDateToString = moment(todayDate).format('YYYY-MM-DD-HH-mm-ss');

	var todayDateXval = null; //아래의 calDateRange에서 값을 초기화할 계획.
	//todayDateXval = calDateRange(startdate, todayDateToString); 아래에 이거 쓰여져 있음

	var scheduleVariety = 10;
	//기본적인 스케쥴러 행 수. 이를 조절하면 11, 12줄... 30줄... 등 열의 증감이 가능하다

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function calDateRange(startdate, enddate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크 startdate
		if (startdate.length != 19) {
			return null;
		}

		// FORMAT을 포함한 길이 체크 enddate 
		if (enddate.length != 19) {
			return null;
		}

		// FORMAT이 있는지 체크
		if (startdate.indexOf(FORMAT) < 0 || enddate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var start_dt = startdate.split(FORMAT);
		var end_dt = enddate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		start_dt[1] = (Number(start_dt[1]) - 1) + "";
		end_dt[1] = (Number(end_dt[1]) - 1) + "";

		var from_dt = new Date(start_dt[0], start_dt[1], start_dt[2],
				start_dt[3], start_dt[4], start_dt[5]);
		var to_dt = new Date(end_dt[0], end_dt[1], end_dt[2], end_dt[3],
				end_dt[4], end_dt[5]);

		if (from_dt > to_dt) {
			return parseInt((from_dt.getTime() - to_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		} else {
			return parseInt((to_dt.getTime() - from_dt.getTime())
					/ (1000 * 60 * 60 * 24));
		}
	}

	/**
	 * 두 날짜의 차이를 일자로 구한다.(조회 종료일 - 조회 시작일)
	 *
	 * @param val1 - 조회 시작일(날짜 ex.2002-01-01)
	 * @param val2 - 조회 종료일(날짜 ex.2002-01-01)
	 * @return 기간에 해당하는 일자
	 */
	function stringToDate(varDate) {
		var FORMAT = "-";

		// FORMAT을 포함한 길이 체크
		if (varDate.length != 19)
			return null;

		// FORMAT이 있는지 체크
		if (varDate.indexOf(FORMAT) < 0)
			return null;

		// 년도, 월, 일로 분리
		var var_dt = varDate.split(FORMAT);

		// 월 - 1(자바스크립트는 월이 0부터 시작하기 때문에...)
		// Number()를 이용하여 08, 09월을 10진수로 인식하게 함.
		var_dt[1] = (Number(var_dt[1]) - 1) + "";

		var return_dt = new Date(var_dt[0], var_dt[1], var_dt[2], var_dt[3],
				var_dt[4], var_dt[5]);

		return return_dt;
	}

	function createPlan() {
		var planName = document.getElementById("planNameIns").value;

		var format = 'YYYY-MM-DDThh:mm:ss.ms';
		var pstartdate = moment($('#pStartDateIns').val(), format, 'ko')
				.format('YYYY-MM-DD-HH-mm-ss');//format
		var penddate = moment($('#pEndDateIns').val(), format, 'ko').format(
				'YYYY-MM-DD-HH-mm-ss');//format

		console.log(pstartdate+'::::::::::::::');
		var priorityIns = document.getElementById("priority").value;

		var planmemberArray = [];
		var planmemberByName = document.getElementsByName("planmember");

		for (var i = 0; i < planmemberByName.length; i++) {

			var checkedBoolean = planmemberByName.item(i).getAttribute("checked");
			var checkedValue = planmemberByName.item(i).value;
			
			if (checkedBoolean == "checked") {
				planmemberArray.push(planmemberByName.item(i).value);
			}
			
		}

		console.log(planmemberArray);		
		
		var color = "#" + document.getElementById("colorIns").value;

		var planInsJSON = {
			"PNO" : 0, //irrelevant
			"planName" : planName,
			"pStartDate" : pstartdate,
			"pEndDate" : penddate,
			"PJNo" : Number('${pjno}'),
			"id" : planmemberArray,
			"Priority" : priorityIns,
			"Progress" : 0,
			"color" : color
		};

		console.log(planInsJSON);
		//"PJNo" : $.session.get("pjNO"),
		//VO와 같은 형식으로 일치시켜야 한다.
		//id는 추후 ArrayList로 변경하여, 여러 팀원이 배정 받고 빠져나갈 수 있도록 해야 한다.

		$
				.ajax({
					url : 'insertJ',
					type : 'GET',
					dataType : 'text', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨
					//data : {'voice': final_transcript},
					data : {
						'jsonString' : JSON.stringify(planInsJSON)
					},
					//data : JSON.stringify(voice),	//contentType:"application/json"에 반드시 붙어야 한다고 함
					//contentType:"application/json",
					success : function() {
						document.getElementById('scheduleIframe').contentDocument.location
								.reload(true);
						document.getElementById('addPlanForm').reset();
						document.getElementById('colorIns').setAttribute(
								'style', 'FFFFFF');
					},
					error : function(error) {
						document.getElementById('scheduleIframe').contentDocument.location
						.reload(true);
						document.getElementById('addPlanForm').reset();
						document.getElementById('colorIns').setAttribute(
						'style', 'FFFFFF');
					}
				});

	}//end createPlan
	
	
	/* function openPlan(pno) {
		//pno를 갖고 컨트롤러로 이동한다
		window
				.open("getThePlan?pno=" + pno, "일정내용",
						"width=800, height=700, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
	} */
	

	function mouseOver(divMouseOver) {

		divMouseOver
				.setAttribute(
						"title",
						(divMouseOver.getAttribute("pno") + " / "
								+ divMouseOver.getAttribute("planName") + " : "
								+ divMouseOver.getAttribute("pstartdate")
								+ " ~ " + divMouseOver.getAttribute("penddate")));
	}

	function mouseOut(divMouseOver) {

	}

	function goToGant() {
		document.getElementById('scheduleIframe').setAttribute('src',
				'goplanGant?pjno=${pjno}');
	}
	function goToGantById() {
		document.getElementById('scheduleIframe').setAttribute('src',
				'goplanGantById?pjno='+'${pjno}'+'&loginname='+'${loginname}');
	}
	function goToGantByPriority() {
		document.getElementById('scheduleIframe').setAttribute('src',
				'goplanGantByPriority?pjno=${pjno}');
	}	
	function goToCalendar() {
		document.getElementById('scheduleIframe').setAttribute('height', '40%');
		document.getElementById('scheduleIframe').setAttribute('src',
				'goplanCalendar?pjno=${pjno}');
	}
	function goToPersonal() {
		document.getElementById('scheduleIframe').setAttribute('src',
				'myScheduleToday?pjno=${pjno}');
	}

	function resizeIframe(obj) {
		if (document.getElementById('scheduleIframe').getAttribute('src').includes("Gant")) {
			obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
		} else if (document.getElementById('scheduleIframe').getAttribute('src').includes("Calendar")) {
			obj.style.height = 50 + 'vh';
		} else {
			obj.style.height = 50 + 'vh';
			obj.style.width = 40 + 'vw';
		}

	}
	
</script>

<script type="text/javascript">
	var intervalOn = false;
	var blinkIntervalBoolean = false;
	var blinkInterval = null;
	var getName = null;

	function blinking() {
		document.getElementById('scheduleIframe').contentWindow.blinking();
	}
</script>

</head>
<body>
	<!-- 회원 로그인 시 상단 바-->
	<c:if test="${loginid!=null }">
		<div id="topnav">
			<div class="topnav">
				<div class="topnavdiv">
					<a href="./"><img src="resources/img/logo2-white.png"
						style="height: 50px; margin-top: -20px;"></a> <a href="logout">로그아웃</a>
					<!-- <a href="goplan">일정</a> -->
					<!-- <a href="goplanCalendar">일정_달력형</a>
					<a href="myScheduleToday">일정_개인형</a> -->
					<!-- <a href="goproject">프로젝트 기능 테스트</a> -->
					<!-- <a href="meetingForm">회의생성/참여</a> -->
				</div>
			</div>

		</div>
	</c:if>
	<!-- 회원 로그인시 상단 바 끝 -->


	<!-- today 버튼 -->
	<!-- <input type="button" value="today" onclick="javascript: blinking(this)"> -->
	<!-- value에 원하는 div name의 값을 넣어 Blink 기능을 실행 -->


	<br>


	<div align="center">
		<div style="width: 80%;">
			<!-- 메뉴 -->
			<div align="right">
				<!-- today 버튼 -->
				<!-- <button type="button" value="today" class="btn btn-default btn-lg"
					style="width: 10%; min-width: 130px; background-color: #5c948d"
					onclick="javascript: blinking()"></button> -->
				<button id="dropbtn" type="button" class="btn btn-default btn-lg"
						style="width: 4vw; min-width: 130px; height: 7%; color:coral; background-color: coral;"
						 onclick="javascript: blinking()">.<img src="resources/iconBulb.png" height="20vh">.</button>
				<!-- value에 원하는 div name의 값을 넣어 Blink 기능을 실행 -->
				<div class="dropdown">
					<button id="dropbtn" type="button" class="btn btn-default btn-lg"
						style="width: 12vw; min-width: 130px; height: 7%; background-color: #5c948d"
						onclick="goToGant()">간트차트</button>
					<div class="dropdown-content">
						<a href="Javascript: goToGant()">날짜순 (기본)</a> <a
							href="Javascript: goToGantByPriority()">중요도순</a> <a
							href="Javascript: goToGantById()">내 일정</a>
					</div>
				</div>

				<button type="button" class="btn btn-default btn-lg"
					style="width: 12vw; min-width: 130px; height: 7%; background-color: #5c948d"
					onclick="goToCalendar()">달력형</button>
				<button type="button" class="btn btn-default btn-lg"
					style="width: 12vw; min-width: 130px; height: 7%; background-color: #5c948d"
					onclick="goToPersonal()">투데이</button>
			</div>

			<!-- 부트스트랩 -->
			<div align="center" style="width: 100%">
				<section class='bb-slice' id='faq' style="width: 100%;">
				<div align="center" class='bb-container bb-container-variant-small'
					style="width: 75%">
					<h1 class='bb-section-title'>Schedule Managing</h1>

					<div align="center" class='bb-accordion' style="width: 100%;">
						<span class='bb-accordion-title'
							style="width: 100%; height: 100%; align: center; margin: 10px; padding: 10px; font-size: medium;">일정추가</span>
						<div class='bb-accordion-content'>
							<!-- 일정을 넣자! -->
							<div
								style="position: relative; padding: 0px; margin: 0px; text-align: left; font-size: medium;">
								<form action='javascript: addPlanGraph()' id="addPlanForm">
									<br>
									<p>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="일정이름" readonly="readonly"> <input
											id="planNameIns" type="text"
											style="font-size: medium; width: 48%;" autocomplete="off"/>
									</p>
									<p>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="시작 날짜" readonly="readonly"> <input
											id="pStartDateIns" type="datetime-local"
											style="font-size: medium; width: 48%;" />
									</p>
									<p>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="마감 날짜" readonly="readonly"> <input
											id="pEndDateIns" type="datetime-local"
											style="font-size: medium; width: 48%;" />
									</p>

									<script type="text/javascript" src="resources/bootstrap.min.js"></script>
									<script src="resources/moment-with-locales.js"></script>
									<script src="resources/bootstrap-datetimepicker.js"></script>
									<script>
								    	//$('.pStartDateIns').datetimepicker();
										//$('#pEndDateIns').datetimepicker();
									</script>

									<!--  -->
									<p>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="담당 팀원" readonly="readonly">
									
									<div id="planMemberSelectList" align="center"></div></p>
										<div>
										<c:if test="${memberList !=pj.id }">
											<c:forEach items="${memberList }" var="member">
												<script type="text/javascript">
												
												var input = '<a href="javascript:planMemberSelect(planmember${member.name })"><input type="button" class="menu"';
												input += 'style="border: none; background-color: #ffcb5e;';
												input += 'color: #fff; font-size: large; padding: 3px"';
												input += 'id="planmember${member.name }" name="planmember" value="${member.name }"';
												input += 'readonly="readonly" ';
												input += 'checked="unchecked"></a>';
												
												document.getElementById("planMemberSelectList").innerHTML += input + ' ';
												</script>
											</c:forEach>
										</c:if>
<script type="text/javascript">
	function planMemberSelect(ObjIns) {
		
		console.log(ObjIns);
		console.log(ObjIns.id);
		var planMember = document.getElementById(ObjIns.id);
		console.log(planMember.getAttribute("checked"));
		if (planMember.getAttribute("checked") == "checked") {
			planMember.setAttribute("checked","unchecked");
			planMember.style.backgroundColor = '#ffcb5e';
		} else {
			planMember.setAttribute("checked","checked");
			planMember.style.backgroundColor = '#d95b5b';			
		}  
		var planmemberArray = [];
		var planmemberByName = document.getElementsByName("planmember");

		for (var i = 0; i < planmemberByName.length; i++) {

			var checkedBoolean = planmemberByName.item(i).getAttribute("checked");
			var checkedValue = planmemberByName.item(i).value;
			
			console.log(checkedBoolean+'...'+checkedValue);
			
			if (checkedBoolean == "checked") {
				planmemberArray.push(planmemberByName.item(i).value);
				console.log('!!!!!!!!!!!!');
			}
			
		}

		console.log(planmemberArray);

	}
</script>

										<c:if test="${memberList ==pj.id }">
											<input type="text" class="menu"
												style="border: none; background-color: #d95b5b; color: #fff; font-size: medium; width: 48%;"
												value="팀원이 없습니다. 팀원을 초대하세요" readonly="readonly">
										</c:if>
									</div>

									<!-- 해당 팀원은 ArrayList로 변경 예정 -->
									<script src="resources/jscolor.js"></script>
									<p>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="색깔" readonly="readonly">   <input class="jscolor"
											id="colorIns" style="font-size: medium; width: 48%;" />
									</p>
									<!-- priority 추가함 -->
									<div>
										<input type="text" class="menu"
											style="border: none; background-color: #1f2944; color: #fff; font-size: medium; width: 48%;"
											value="중요도" readonly="readonly">   
										<input id="priority" autocomplete="off"
					type="number" name="priority" class="menu" style="text-align:center; border: none; background-color: #1f2944; color: #fff; height: 2.5em;" step="1" min="0" max="10">

									</div>
									<br>
									<div align="center">
										<button type="submit" class="btn btn-lg"
											style="width: 15%; background-color: #1f2944 font-size: medium;">확인</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				</section>
			</div>
		</div>

<div style="width:100%;" align="center">
		<iframe id="scheduleIframe" src="goplanCalendar?pjno=${pjno }"
			style="width: 80%; white-space: nowrap; overflow: hidden; margin: 0px; padding: 0px; border: 2px solid black"
			align="top" onload="resizeIframe(this)" /></iframe>

	</div>
	<!-- 메뉴/일정추가 포함 -->


	<script type="text/javascript">
		function addPlanGraph() {
			createPlan();
			scheduleUpdate();
		}
	</script>

</body>

<script src="resources/css/bb-043d654a.js"></script>
<script>
	(function(i, s, o, g, r, a, m) {
		i['GoogleAnalyticsObject'] = r;
		i[r] = i[r] || function() {
			(i[r].q = i[r].q || []).push(arguments)
		}, i[r].l = 1 * new Date();
		a = s.createElement(o), m = s.getElementsByTagName(o)[0];
		a.async = 1;
		a.src = g;
		m.parentNode.insertBefore(a, m)
	})(window, document, 'script',
			'https://www.google-analytics.com/analytics.js', 'ga');

	ga('create', 'UA-68685304-3', 'auto');
	ga('send', 'pageview');
</script>

</html>